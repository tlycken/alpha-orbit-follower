#include "aof/xml/xml-factory.hpp"
#include "xml-factory-tests.hpp"
#include "aof/physics/global-magnetic-field.hpp"
#include "aof/physics/flux-function-interpolation.hpp"
#include "aof/implementation-choices.hpp"

using namespace aof::physics;
using namespace aof::interpolation;
using namespace aof::xml;
using namespace aof::xml::tests;

// This test just here because test fixture parses xml.
// It would be stupid to fail all tests in this suite because
// the parser is broken, and not notice...
TEST_F(ValidGradShafranov_F, TestInitializesOK) { }

TEST_F(ValidGradShafranov_F, BuildsGSModelWithoutException) {
    ASSERT_TRUE(this->input.physicalDatas.size() > 0);

    fac.create<GradShafranovModel<Interp2D, Interp1D, 13>>(false);
}

TEST_F(ValidGradShafranov_F, BuildsSingleParticleInitialConditionWithoutException) {
    auto b = this->fac.create<GradShafranovModel<Interp2D, Interp1D, 13>>(false);
    this->fac.create<SingleParticleGCInitialCondition<state_t>>(b);
}

TEST_F(ValidGradShafranov_F, BuildsParticleSpeciesWithoutException) {
    auto b = this->fac.create<GradShafranovModel<Interp2D, FluxFunctionInterpolation<Interp1D, 13>, 13>>(false);
    auto sp = Util::find_named(this->input.physicalDatas[0].plasmas[0].particles, "Electrons");
    auto s = this->fac.create<particle_t>(sp, b);
    ASSERT_GT(s.n(5.1, 0.0, 1.1), 0.0);
}

TEST_F(ValidGradShafranov_F, BuildsPlasmaVolume) {
    auto v = this->fac.create<plasmavol_t>();
}

TEST_F(ValidGradShafranov_F, BuildsCloudInitialCondition) {
    auto b = this->fac.create<GradShafranovModel<Interp2D, FluxFunctionInterpolation<Interp1D, 13>, 13>>(false);
    auto ic = this->fac.create<ParticleCloudGCInitialCondition<state_t, plasma_t, globalfield_t, rng_t>>(b);
}

TEST_F(ValidGradShafranov_F, BuildsChosenIC) {
    auto b = this->fac.create<GradShafranovModel<Interp2D, FluxFunctionInterpolation<Interp1D, 13>, 13>>(false);
    auto ic = this->fac.create<initialcondition_t>(b);
}

TEST_F(ValidGradShafranov_F, BuildsDriverWithoutException) {
    auto d = this->fac.create<driver_t>();
}

TEST_F(ValidGradShafranov_F, BuildsSummaryOutput) {
    auto sb = this->fac.create<std::unique_ptr<IOutputBuilder>>(std::string("summary"), "");
    std::ostream &s = sb->get();
}

int pull_xml_factory_tests() {
    return 0;
}

