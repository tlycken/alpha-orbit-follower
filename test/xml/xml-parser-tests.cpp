#include "gtest/gtest.h"

#include <fstream>
#include <sstream>
#include <algorithm>
#include <iterator>

#include "aof/xml/xml-parser.hpp"
#include "xml-parser-tests.hpp"

using namespace std;
using namespace testing;
using namespace aof::xml;
using namespace aof::xml::tests;

TEST_F(XmlParserTests, StreamIsNotEmpty) {
    std::string s = this->xml_contents_stream.str();

    ASSERT_GT(s.length(), 0) << "The xml file was not read into memory correctly - expect other xml tests to fail...";
}

TEST_F(XmlParserTests, ParsesStreamWithoutError) {
    XmlParser parser;

    ASSERT_NO_THROW(parser.parse(this->xml_contents_stream));
}

TEST_F(XmlParserTests, SetsHasDocumentTrueAfterParsingStream) {
    XmlParser parser;

    ASSERT_FALSE(parser.hasDocument());
    parser.parse(this->xml_contents_stream);
    ASSERT_TRUE(parser.hasDocument());
}


TEST_F(XmlParserTests, ParsesFileWithoutError) {
    XmlParser parser;

    ASSERT_NO_THROW(parser.parse(this->fname));
}

TEST_F(XmlParserTests, GivenNonexistingFile_ThrowsError) {
    XmlParser parser;

    ASSERT_THROW(parser.parse(this->wrong_fname), no_file_error);
}

TEST_F(XmlParserTests, SetsHasDocumentTrueAfterParsingFile) {
    XmlParser parser;

    ASSERT_FALSE(parser.hasDocument());
    parser.parse(this->fname);
    ASSERT_TRUE(parser.hasDocument());
}

void verifyInput(AOFinput input) {
    ASSERT_EQ(1, input.physicalDatas.size());

    // Verify Grad-Shafranov representation
    representation gs = input.physicalDatas[0].magneticFields[0].representations[0];
    grid R_grid = gs.grids[0];
    ASSERT_EQ("R", R_grid.name);
    grid Z_grid = gs.grids[1];
    ASSERT_EQ("Z", Z_grid.name);
    grid psi_grid = gs.grids[2];
    ASSERT_EQ("psi", psi_grid.name);

    field psi_field = gs.fields[0];
    ASSERT_EQ(2, psi_field.dimensions);
    
    istringstream iss(psi_field.valueList[0].data);
    vector<string> valueData{istream_iterator<string>{iss}, istream_iterator<string>{}};
    ASSERT_EQ(195, valueData.size());

    // Verify Solovev representation
    representation sv = input.physicalDatas[0].magneticFields[0].representations[1];

    ASSERT_DOUBLE_EQ(sv.R0, 6.0);
    ASSERT_DOUBLE_EQ(sv.Z0, 0.0);
    ASSERT_DOUBLE_EQ(sv.psi0, 100.0);
    ASSERT_DOUBLE_EQ(sv.E, 2.0);
    ASSERT_DOUBLE_EQ(sv.tau, 0.0);    
}

TEST_F(XmlParserTests, ReturnsStructWithContentsAfterParsingStream) {
    XmlParser parser;

    parser.parse(this->xml_contents_stream);

    EXPECT_TRUE(parser.hasDocument());

    AOFinput input = parser.getInput();
    verifyInput(input);
}

TEST_F(XmlParserTests, ReturnsStructWithContentsAfterParsingFile) {
    XmlParser parser;
    parser.parse(this->fname);

    EXPECT_TRUE(parser.hasDocument());

    AOFinput input = parser.getInput();
    verifyInput(input);
}

TEST_F(XmlParserTests, GettingInputWithoutParsingThrowsException) {
    XmlParser parser;

    ASSERT_THROW(parser.getInput(), no_document_error);
}


int pull_xml_parser_tests() { return 0; }
