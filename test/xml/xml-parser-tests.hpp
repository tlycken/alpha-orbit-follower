#ifndef XML_PARSER_TESTS_HPP
#define XML_PARSER_TESTS_HPP

#include "gtest/gtest.h"
#include <sstream>
#include <fstream>
#include <iostream>

    namespace aof {
        namespace xml {
            namespace tests {

                class XmlParserTests : public ::testing::Test {
                public:

                    const std::string wrong_fname = "../testdata/grad-shafranov.xml";
                    const std::string fname = "data/grad-shafranov.xml";
                    std::ifstream xml_file;
                    std::stringstream xml_contents_stream;

                    void SetUp() {
                        // Read file to stream
                        xml_file.open(fname);
                        if (!xml_file) {
                            FAIL() << "Couldn't open xml file: " << fname << std::endl;
                        }
                        xml_contents_stream << xml_file.rdbuf();
                        xml_file.close();

                        // Re-open file to be used for test
                        xml_file.open(fname);
                    }

                    void TearDown() {
                        xml_file.close();
                    }

                };

            }
        }
    }

#endif
