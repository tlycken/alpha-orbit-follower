#ifndef XML_FACTORY_TESTS_HPP
#define XML_FACTORY_TESTS_HPP

#include "gtest/gtest.h"
#include "aof/xml/xml-parser.hpp"
#include "aof/xml/xml-factory.hpp"

    namespace aof {
        namespace xml {
            namespace tests {

                using namespace factories;

                class ValidGradShafranov_F : public ::testing::Test {
                public:
                    AOFinput input;
                    XmlFactory fac;

                    ValidGradShafranov_F() : fac("data/grad-shafranov.xml") {
                        XmlParser parser;
                        parser.parse("data/grad-shafranov.xml");
                        input = parser.getInput();
                    }
                };
            }
        }
    }

#endif
