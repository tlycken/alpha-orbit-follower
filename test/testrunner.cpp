#include "gtest/gtest.h"

int pull_interpolationtests();
static int pull_interptests_dummy = pull_interpolationtests();
int pull_local_magnetic_field_tests();
static int pull_local_magnetic_field_tests_dummy = pull_local_magnetic_field_tests();
int pull_grad_shafranov_model_tests();
static int pull_grad_shafranov_model_tests_dummy = pull_grad_shafranov_model_tests();
int pull_xml_parser_tests();
static int pull_xml_parser_tests_dummy = pull_xml_parser_tests();
int pull_xml_factory_tests();
static int pull_xml_factory_tests_dummy = pull_xml_factory_tests();
int pull_rhs_evaluator_tests();
static int pull_rhs_evaluator_tests_dummy = pull_rhs_evaluator_tests();
int pull_chamber_tests();
static int pull_chamber_tests_dummy = pull_chamber_tests();
int pull_plasma_tests();
static int pull_plasma_tests_dummy = pull_plasma_tests();

int main(int argc, char* argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}