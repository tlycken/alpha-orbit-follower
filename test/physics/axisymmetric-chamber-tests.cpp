#include "axisymmetric-chamber-tests.hpp"

#include "aof/algebra/vector-algebra.hpp"
#include "aof/physics/chamber.hpp"
#include "aof/physics/bounded-volume.hpp"
#include <cmath>
#include <algorithm>
#include <fstream>

#include "aof/xml/xml-factory.hpp"

using namespace aof;
using namespace aof::physics;
using namespace aof::xml::factories;

TEST(AxisymmetricChamber, IsInside) {
    AxisymmetricPolygonChamber c;

    // Adding four corners in order
    c.add_vertex(5.5, 0.0, -1.5);
    c.add_vertex(6.5, 0.0, -1.5);
    c.add_vertex(6.5, 0.0, 1.5);
    c.add_vertex(5.5, 0.0, 1.5);

    ASSERT_TRUE(c.isInside(6.0, 0.0, 1.0));
    ASSERT_TRUE(c.isInside(5.6, 1.0, -.3));
    ASSERT_FALSE(c.isInside(6.6, 2.0,  1.0));
    ASSERT_FALSE(c.isInside(5.0, 3.0,  1.7));
}

TEST(AxisymmetricChamber, SurfaceArea) {
    AxisymmetricPolygonChamber c;

    // Adding four corners in order
    c.add_vertex(5.5, 0.0, -1.5);
    c.add_vertex(6.5, 0.0, -1.5);
    c.add_vertex(6.5, 0.0, 1.5);
    c.add_vertex(5.5, 0.0, 1.5);

    EXPECT_NEAR(3.0, c.surfaceArea(), 1e-2);

    AxisymmetricPolygonChamber d;

    // Adding vertices on a circle with radius r
    double r = 1.5;
    int N = 9;
    for (int i = 0; i < N; ++i) {
        double a = i * 2 * 3.1415926 / N;
        d.add_vertex(r * cos(a), 0.0, r * sin(a));
    }

    EXPECT_NEAR(r * r * N * sin(2 * 3.1415926 / N) / 2., d.surfaceArea(), 1e-2);
}

TEST(AxisymmetricPolygonVolume, Volume) {

    // Adding vertices on a circle with radius r, at distance R from origo
    // This will correspond to a toroidal volume with major/minor radius R/r
    double r = 1.5, R = 3.5;
    double V = 155.44627;

    AxisymmetricPolygonChamber c;
    int N = 100;
    for (int i = 0; i < N; ++i) {
        double a = -i * 2 * 3.1415926 / float(N);
        c.add_vertex(R + r * cos(a), 0.0, 1.0 + r * sin(a));
    }
    EXPECT_NEAR(V, c.volume(), .5);
}

template<typename T>
bool IsPositive(T x) {
    return x > 0;
}


TEST(AxisymmetricPolygonVolume, VolumeFromXML) {
    XmlFactory f("data/grad-shafranov.xml");

    auto chamber = f.create<AxisymmetricPolygonChamber>();

    double cvolume = chamber.volume();

    auto plasma = f.create<PlasmaVolume>();

    double pvolume = plasma.volume();

    ASSERT_PRED1(IsPositive<double>, cvolume);
    ASSERT_PRED1(IsPositive<double>, pvolume);
}

int pull_chamber_tests() {
    return 0;
}