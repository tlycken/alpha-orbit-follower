#include "gmock/gmock.h"

#include "aof/physics/particle.hpp"
#include "aof/physics/plasma.hpp"

namespace aof {
    namespace physics {
        namespace mocks {

            class MockParticleSpecies : public aof::physics::IParticleSpecies {
            public:
                struct expectations {
                    double R, phi, Z, n, T;
                    particle type;

                    expectations(double R, double phi, double Z, double n, double T, particle type)
                        : R(R), phi(phi), Z(Z), n(n), T(T), type(type) { }

                    expectations(const expectations &other)
                        : R(other.R), phi(other.phi), Z(other.Z), n(other.n), T(other.T), type(other.type) { }
                };

                MockParticleSpecies() : expected(0, 0, 0, 0, 0, particle::alpha()) { }

                explicit MockParticleSpecies(const expectations &ex) : expected(ex) {
                    setupDefaultActions();
                }

                MockParticleSpecies(const MockParticleSpecies &other) : expected(other.expected) {
                    setupDefaultActions();
                }

                MOCK_CONST_METHOD3(n, double(double, double, double));
                MOCK_CONST_METHOD3(T, double(double, double, double));
                MOCK_CONST_METHOD0(properties, particle());

                void expect(double R, double phi, double Z, double n, double T, particle type) {
                    expected = expectations(R, phi, Z, n, T, type);
                    setupDefaultActions();
                }

                void setupDefaultActions() {
                    setupDefaultActions(expected);
                }

                void setupDefaultActions(expectations ex) {
                    ON_CALL(*this, n(ex.R, ex.phi, ex.Z)).WillByDefault(::testing::Return(ex.n));
                    ON_CALL(*this, T(ex.R, ex.phi, ex.Z)).WillByDefault(::testing::Return(ex.T));
                    ON_CALL(*this, properties()).WillByDefault(::testing::Return(ex.type));

                    EXPECT_CALL(*this, n(ex.R, ex.phi, ex.Z)).Times(::testing::AnyNumber());
                    EXPECT_CALL(*this, T(ex.R, ex.phi, ex.Z)).Times(::testing::AnyNumber());
                    EXPECT_CALL(*this, properties()).Times(::testing::AnyNumber());
                }

            private:
                expectations expected;
            };

            class MockRateCoefficient : public IRateCoefficient {
            public:
                struct expectations {
                    double T, k;
                    expectations(double T, double k) : T(T), k(k) { }
                    expectations(const expectations &other) {
                        T = other.T;
                        k = other.k;
                    }
                };

                MockRateCoefficient() : expected(0, 0) { }
                explicit MockRateCoefficient(const expectations &ex) : expected(ex) {
                    setupDefaultActions();
                }
                MockRateCoefficient(const MockRateCoefficient &other) : expected(other.expected) {
                    setupDefaultActions();
                }

                MOCK_CONST_METHOD1(rateCoeff, double(double));

                void expect(double T, double k) {
                    expected = expectations(T, k);
                    setupDefaultActions();
                }

                void setupDefaultActions() {
                    setupDefaultActions(expected);
                }

                void setupDefaultActions(expectations ex) {
                    ON_CALL(*this, rateCoeff(ex.T)).WillByDefault(::testing::Return(ex.k));
                    EXPECT_CALL(*this, rateCoeff(ex.T)).Times(::testing::AnyNumber());
                }

            private:
                expectations expected;
            };

        }
    }
}
