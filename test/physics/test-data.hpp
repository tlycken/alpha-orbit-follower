#ifndef TESTDATA_HPP
#define TESTDATA_HPP

#include "aof/physics/particle.hpp"

#include <array>

// These values calculated in MATLAB given B = F Dphi + Dphi X Dpsi/2pi

namespace aof {
    namespace physics {
        namespace tests {

            struct position_expectations {
                const double R;
                const double phi;
                const double Z;

                position_expectations(double R, double phi, double Z) :
                    R(R),
                    phi(phi),
                    Z(Z) { }
            };

            struct magnetic_field_expectations {
                const double psi;
                const double psi_R;
                const double psi_Z;
                const double psi_RR;
                const double psi_RZ;
                const double psi_ZZ;

                const double F;
                const double Fp;
                const double Fpp;

                const vector<3> B;
                const matrix<3, 3> nabla_outerprod_B;
                const vector<3> gradB;
                const vector<3> rotb;
                const vector<3> b;

                magnetic_field_expectations(
                    double psi,
                    double psi_R,
                    double psi_Z,
                    double psi_RR,
                    double psi_RZ,
                    double psi_ZZ,
                    double F,
                    double Fp,
                    double Fpp,

                    vector<3> B,
                    matrix<3, 3> nabla_outerprod_B,
                    vector<3> gradB,
                    vector<3> rotb,
                    vector<3> b) :

                    psi(psi),
                    psi_R(psi_R),
                    psi_Z(psi_Z),
                    psi_RR(psi_RR),
                    psi_RZ(psi_RZ),
                    psi_ZZ(psi_ZZ),

                    F(F),
                    Fp(Fp),
                    Fpp(Fpp),

                    B(B),
                    nabla_outerprod_B(nabla_outerprod_B),
                    gradB(gradB),
                    rotb(rotb),
                    b(b) { }
            };

            struct guiding_center_equations {
                const double vpar;
                const double mu;
                const double phase;

                std::array<double, 6> RHS;

                guiding_center_equations(double vpar, double mu, double phase, std::array<double, 6> RHS) :
                    vpar(vpar),
                    mu(mu),
                    phase(phase),

                    RHS(RHS) { }
            };

            struct default_expectations {
                static particle pcle() {
                    return particle::alpha();
                }

                static position_expectations position() {
                    return position_expectations(5.  /* R */,
                                                 0.3  /* phi */,
                                                 1.5  /* Z */);
                }

                static magnetic_field_expectations bfield(int COCOS) {

                    if (COCOS == 11) {
                        vector<3> B;
                        B << -0.00636619772367581, 0.7, 0.00954929658551372;
                        matrix<3, 3> nabla_outerprod_B;
                        nabla_outerprod_B << 0.000318309886183791, -0.092, 0.00190985931710274, -0.14, -0.00127323954473516, 0, -0.00795774715459477, 0.032, 0.00159154943091895;
                        vector<3> gradB;
                        gradB << -0.0919644811568327, 0, 0.0320897711994361;
                        vector<3> rotb;
                        rotb << 0.000122067911877827, -0.0154696357274598, 0.199904737910559;
                        vector<3> b;
                        b << -0.0090933460616996, 0.99986562143948, 0.0136400190925494;


                        return magnetic_field_expectations(
                                   2  /* psi */,
                                   0.3  /* psi_R */,
                                   0.2  /* psi_Z */,
                                   0.1  /* psi_RR */,
                                   0.05  /* psi_RZ */,
                                   0.25  /* psi_ZZ */,
                                   3.5  /* F */,
                                   0.8  /* Fp */,
                                   1.3  /* Fpp */,
                                   B,
                                   nabla_outerprod_B,
                                   gradB,
                                   rotb,
                                   b);
                    } else if (COCOS == 13) {
                        vector<3> B;
                        B << 0.00636619772367581, 0.7, -0.00954929658551372;
                        matrix<3, 3> nabla_outerprod_B;
                        nabla_outerprod_B << -0.000318309886183791, -0.092, -0.00190985931710274, -0.14, 0.00127323954473516, 0, 0.00795774715459477, 0.032, -0.00159154943091895;
                        vector<3> gradB;
                        gradB << -0.0919644811568327, 0, 0.0320897711994361;
                        vector<3> rotb;
                        rotb << 0.000122067911877827, 0.0154696357274598, 0.199904737910559;
                        vector<3> b;
                        b << 0.0090933460616996, 0.99986562143948, -0.0136400190925494;


                        return magnetic_field_expectations(
                                   2  /* psi */,
                                   0.3  /* psi_R */,
                                   0.2  /* psi_Z */,
                                   0.1  /* psi_RR */,
                                   0.05  /* psi_RZ */,
                                   0.25  /* psi_ZZ */,
                                   3.5  /* F */,
                                   0.8  /* Fp */,
                                   1.3  /* Fpp */,
                                   B,
                                   nabla_outerprod_B,
                                   gradB,
                                   rotb,
                                   b);
                    } else {
                        throw not_implemented("Only COCOS 11 and 13 are supported");
                    }
                };

                static guiding_center_equations gc_rhs() {
                    vector<6> RHS;
                    RHS << -73519.4423399582, 2247404.19034569, 989039.700637963, -38382308656.643, 0, 33761701.4398793;

                    return guiding_center_equations(11249669.9951599  /* vpar */,
                                                    2.00190967625791e-13  /* mu */,
                                                    0 /* phase */,

                    std::array<double, 6> {
                        -73519.4423399582, 2247404.19034569, 989039.700637963, -38382308656.643, 0, 33761701.4398793
                    } /* RHS */);
                }
            };
        }
    }
}

#endif
