#ifndef GLOBAL_MAGNETIC_FIELD_MOCKS_HPP
#define GLOBAL_MAGNETIC_FIELD_MOCKS_HPP

#include "aof/physics/magnetic-field.hpp"
#include "aof/physics/global-magnetic-field.hpp"
#include "gmock/gmock.h"


    namespace aof {
        namespace physics {
            namespace mocks {

                class MockGlobalMagneticField : public aof::physics::IGlobalMagneticField {
                public:
                    struct expectations {
                        double R, phi, Z;
                        matrix<3, 3> nabla_outerprod_B;
                        vector<3> B;
                        double psi, F;

                        expectations(double R, double phi, double Z, matrix<3, 3> nabla_outerprod_B, vector<3> B, double psi, double F) : R(R), phi(phi), Z(Z), nabla_outerprod_B(nabla_outerprod_B), B(B), psi(psi), F(F) { }

                        expectations(const expectations &other) {
                            R = other.R;
                            phi = other.phi;
                            Z = other.Z;
                            nabla_outerprod_B = other.nabla_outerprod_B;
                            B = other.B;
                            psi = other.psi;
                            F = other.F;
                        }
                    };

                    MockGlobalMagneticField() : expected(0, 0, 0, matrix<3, 3>(), vector<3>(), 0, 0) { }

                    explicit MockGlobalMagneticField(const expectations &ex) : expected(ex) {
                        setupDefaultActions();
                    }

                    MockGlobalMagneticField(const MockGlobalMagneticField &other) : expected(other.expected) {
                        setupDefaultActions();
                    }

                    MOCK_CONST_METHOD3(nabla_outerprod_B, matrix<3, 3>(double, double, double));
                    MOCK_CONST_METHOD3(B, vector<3>(double, double, double));
                    MOCK_CONST_METHOD3(psi, double(double, double, double));
                    MOCK_CONST_METHOD1(F, double(double));

                    void setupDefaultActions() {
                        setupDefaultActions(expected);
                    }

                    void setupDefaultActions(const expectations &ex) {
                        expected = ex;
                        ON_CALL(*this, B(ex.R, ex.phi, ex.Z)).WillByDefault(testing::Return(ex.B));
                        ON_CALL(*this, nabla_outerprod_B(ex.R, ex.phi, ex.Z)).WillByDefault(testing::Return(ex.nabla_outerprod_B));

                        ON_CALL(*this, psi(ex.R, ex.phi, ex.Z)).WillByDefault(testing::Return(ex.psi));
                        ON_CALL(*this, F(ex.psi)).WillByDefault(testing::Return(ex.F));

                        EXPECT_CALL(*this, B(ex.R, ex.phi, ex.Z)).Times(::testing::AnyNumber());
                        EXPECT_CALL(*this, nabla_outerprod_B(ex.R, ex.phi, ex.Z)).Times(::testing::AnyNumber());
                        EXPECT_CALL(*this, psi(ex.R, ex.phi, ex.Z)).Times(::testing::AnyNumber());
                        EXPECT_CALL(*this, F(ex.psi)).Times(::testing::AnyNumber());
                    }

                private:
                    expectations expected;
                };
            }
        }
    }

#endif
