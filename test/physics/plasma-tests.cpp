

#include "gtest/gtest.h"

#include "aof/physics/particle.hpp"
#include "aof/physics/plasma.hpp"
#include "plasma-mocks.hpp"

using namespace aof::physics;
using namespace aof::physics::mocks;

TEST(Plasma, CorrectReactionIntensity) {
    // Not true values, but ~correct OOM
    double T = 2e4; // eV
    double k = 1e-21; // m^3 / s
    double nD = 4e19;
    double nT = 4e19;
    double R = 6.1, phi = 1.3, Z = 0.5;

    auto rateCoeff = MockRateCoefficient();
    rateCoeff.expect(T, k);

    auto deuterium = MockParticleSpecies();
    deuterium.expect(R, phi, Z, nD, T, particle::deuterium());

    auto tritium = MockParticleSpecies();
    tritium.expect(R, phi, Z, nT, T, particle::tritium());

    Reaction<2, MockParticleSpecies, MockRateCoefficient> reaction(deuterium, tritium, rateCoeff);

    double expectedIntensity = nD * nT * k;

    ASSERT_FLOAT_EQ(expectedIntensity, reaction.intensity(R, phi, Z));
}

TEST(Plasma, CorrectRateCoefficient) {
    double T = 2e4; // eV

    // Parameters copied from input file
    double BG = 34.3827;
    double mc2 = 1124656;
    double C1 = 1.17302e-9;
    double C2 = 1.51361e-2;
    double C3 = 7.51886e-2;
    double C4 = 4.60643e-3;
    double C5 = 1.35000e-2;
    double C6 = -1.06750e-4;
    double C7 = 1.36600e-5;
    double Tmin = 2e2;
    double Tmax = 1e5;

    ParametrizedRateCoefficient k(BG, mc2, C1, C2, C3, C4, C5, C6, C7, Tmin, Tmax);

    // Calculated in Julia
    // Corresponds to displayed precision with value in paper
    double sigmav = 4.3302010759180204e-22;

    ASSERT_FLOAT_EQ(sigmav, k.rateCoeff(T));
}

int pull_plasma_tests() {
    return 0;
}
