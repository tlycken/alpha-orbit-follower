#include "gtest/gtest.h"
#include "local-magnetic-field-tests.hpp"
#include "global-magnetic-field-mocks.hpp"
#include "aof/algebra/vector-algebra.hpp"
#include "aof/physics/local-magnetic-field.hpp"

using namespace aof;
using namespace aof::physics;
using namespace aof::physics::tests;
using namespace aof::physics::mocks;
using namespace testing;

typedef Types<LocalMagneticField> LocalMagneticFieldImpls;
TYPED_TEST_CASE(LocalField_NormalConditions_F, LocalMagneticFieldImpls);
TYPED_TEST_CASE(LocalField_ZeroField_F, LocalMagneticFieldImpls);

TYPED_TEST(LocalField_NormalConditions_F, Instantiation) {
        TypeParam LFImpl(this->R, this->phi, this->Z, this->expected_B, this->expected_nabla_outerprod_B);
}

TYPED_TEST(LocalField_NormalConditions_F, EvaluateB) {
    TypeParam field(this->R, this->phi, this->Z, this->expected_B, this->expected_nabla_outerprod_B);
    vector<3> B = field.B();

    ASSERT_NEAR(this->expected_B[0], B[0], 1e-14);
    ASSERT_NEAR(this->expected_B[1], B[1], 1e-14);
    ASSERT_NEAR(this->expected_B[2], B[2], 1e-14);
}

TYPED_TEST(LocalField_NormalConditions_F, Evaluateb) {
    TypeParam field(this->R, this->phi, this->Z, this->expected_B, this->expected_nabla_outerprod_B);
    vector<3> b = field.b();

    ASSERT_NEAR(this->expected_b[0], b[0], 1e-14);
    ASSERT_NEAR(this->expected_b[1], b[1], 1e-14);
    ASSERT_NEAR(this->expected_b[2], b[2], 1e-14);
}

TYPED_TEST(LocalField_NormalConditions_F, EvaluateGradB) {
    TypeParam field(this->R, this->phi, this->Z, this->expected_B, this->expected_nabla_outerprod_B);
    vector<3> gradB = field.gradB();

    ASSERT_NEAR(this->expected_gradB[0], gradB[0], 1e-14);
    ASSERT_NEAR(this->expected_gradB[1], gradB[1], 1e-14);
    ASSERT_NEAR(this->expected_gradB[2], gradB[2], 1e-14);
}

TYPED_TEST(LocalField_NormalConditions_F, EvaluateRotb) {
    TypeParam field(this->R, this->phi, this->Z, this->expected_B, this->expected_nabla_outerprod_B);
    vector<3> rotb = field.rotb();

    ASSERT_NEAR(this->expected_rotb[0], rotb[0], 1e-14);
    ASSERT_NEAR(this->expected_rotb[1], rotb[1], 1e-14);
    ASSERT_NEAR(this->expected_rotb[2], rotb[2], 1e-14);
}

TYPED_TEST(LocalField_ZeroField_F, Instantiate) {
    ASSERT_THROW(TypeParam field(this->R, this->phi, this->Z, this->expected_B, this->expected_nabla_outerprod_B), zero_magnetic_field_exception);
}

int pull_local_magnetic_field_tests() {
    return 0;
}
