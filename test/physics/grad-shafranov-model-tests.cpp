#include "gtest/gtest.h"

#include "aof/numerics/interpolation.hpp"
#include "aof/physics/grad-shafranov-model.hpp"
#include "grad-shafranov-model-tests.hpp"

#include <iostream>

using namespace aof;
using namespace aof::interpolation::mocks;
using namespace aof::physics;
using namespace aof::physics::tests;
using namespace testing;

typedef GradShafranovModel<MockInterp2D, MockInterp1D, 11> COCOS11;
typedef GradShafranovModel<MockInterp2D, MockInterp1D, 13> COCOS13;
typedef Types<COCOS11, COCOS13> GradShafranovImpl;

TYPED_TEST_CASE(GradShafranovModel_F, GradShafranovImpl);

TYPED_TEST(GradShafranovModel_F, CalculateMagneticField) {
    vector<3> correct = this->expected_B;
    vector<3> result = this->sut.B(this->expected_R, this->expected_phi, this->expected_Z);

    for (int ii = 0; ii < 3; ++ii) {
        ASSERT_FLOAT_EQ(correct[ii], result[ii]) << " index ii = " << ii;
    }
}

TYPED_TEST(GradShafranovModel_F, CalculateOuterProduct) {
    matrix<3, 3> correct = this->expected_nabla_outerprod_B;
    matrix<3, 3> result = this->sut.nabla_outerprod_B(this->expected_R, this->expected_phi, this->expected_Z);

    for (int ii = 0; ii < 3; ++ii) {
        for (int jj = 0; jj < 3; ++jj) {
            ASSERT_FLOAT_EQ(correct(ii, jj), result(ii, jj))  << " index ii = " << ii << ", jj = " <<jj;
        }
    }
}

TYPED_TEST(GradShafranovModel_F, CalculatePsi) {
    ASSERT_EQ(this->expected_psi, this->sut.psi(this->expected_R, this->expected_phi, this->expected_Z));
}

int pull_grad_shafranov_model_tests() {
    return 0;
}
