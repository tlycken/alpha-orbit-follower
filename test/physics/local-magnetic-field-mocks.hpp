#ifndef LOCAL_MAGNETIC_FIELD_MOCKS_HPP
#define LOCAL_MAGNETIC_FIELD_MOCKS_HPP

#include "aof/physics/magnetic-field.hpp"
#include "gmock/gmock.h"

    namespace aof {
        namespace physics 
         {
            namespace mocks {
                class MockLocalMagneticField : public aof::physics::ILocalMagneticField {
                public:
                    MOCK_CONST_METHOD0(B, vector<3>());
                    MOCK_CONST_METHOD0(b, vector<3>());
                    MOCK_CONST_METHOD0(gradB, vector<3>());
                    MOCK_CONST_METHOD0(rotb, vector<3>());
                };
            }
         }
    }

#endif
