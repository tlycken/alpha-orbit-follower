#ifndef LOCAL_MAGNETIC_FIELD_TESTS_HPP
#define LOCAL_MAGNETIC_FIELD_TESTS_HPP

#include "gtest/gtest.h"
#include "aof/aof-commons.hpp"
#include "global-magnetic-field-mocks.hpp"
#include "test-data.hpp"

    namespace aof {
        namespace physics {
            namespace tests {

                template<typename T>
                class LocalField_NormalConditions_F : public ::testing::Test {
                protected:
                    double R = default_expectations::position().R;
                    double phi = default_expectations::position().phi;
                    double Z = default_expectations::position().Z;


                    double psi = default_expectations::bfield(11).psi;
                    double F = default_expectations::bfield(11).F;

                    vector<3> expected_B = default_expectations::bfield(11).B;
                    matrix<3, 3> expected_nabla_outerprod_B = default_expectations::bfield(11).nabla_outerprod_B;
                    vector<3> expected_gradB = default_expectations::bfield(11).gradB;
                    vector<3> expected_b = default_expectations::bfield(11).b;
                    vector<3> expected_rotb = default_expectations::bfield(11).rotb;
                };

                template<typename T>
                class LocalField_ZeroField_F : public ::testing::Test {
                protected:
                    const double R = default_expectations::position().R;
                    const double phi = default_expectations::position().phi;
                    const double Z = default_expectations::position().Z;

                    double psi = default_expectations::bfield(11).psi;
                    double F = default_expectations::bfield(11).F;

                    vector<3> expected_B = vector<3>(0, 0, 0);
                    matrix<3, 3> expected_nabla_outerprod_B = default_expectations::bfield(11).nabla_outerprod_B;
                };

            }
        }
    }

#endif
