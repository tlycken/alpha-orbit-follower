#ifndef RHS_EVALUATOR_TESTS_HPP
#define RHS_EVALUATOR_TESTS_HPP

#include "gtest/gtest.h"

#include "aof/physics/guiding-center-rhs.hpp"
#include "aof/physics/particle.hpp"

#include "global-magnetic-field-mocks.hpp"
#include "test-data.hpp"

    namespace aof {
        namespace physics {
            namespace tests {

                class GudingCenterRHS_F : public ::testing::Test {
                protected:
                    mocks::MockGlobalMagneticField globalField;

                    const double R = default_expectations::position().R;
                    const double phi = default_expectations::position().phi;
                    const double Z = default_expectations::position().Z;

                    const double vp = default_expectations::gc_rhs().vpar;
                    const double mu = default_expectations::gc_rhs().mu;
                    const double phase = default_expectations::gc_rhs().phase;

                    const particle p = default_expectations::pcle();

                    typedef GuidingCenterRHSEvaluator<mocks::MockGlobalMagneticField> rhs_t;

                public:
                    GudingCenterRHS_F() {
                        mocks::MockGlobalMagneticField::expectations ex(
                            default_expectations::position().R,
                            default_expectations::position().phi,
                            default_expectations::position().Z,
                            default_expectations::bfield(11).nabla_outerprod_B,
                            default_expectations::bfield(11).B,
                            default_expectations::bfield(11).psi,
                            default_expectations::bfield(11).F
                        );

                        globalField.setupDefaultActions(ex);
                    }
                };

            }
        }
    }

#endif
