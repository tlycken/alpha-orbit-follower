#ifndef GRAD_SHAFRANOV_MODEL_TESTS_HPP
#define GRAD_SHAFRANOV_MODEL_TESTS_HPP

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <iostream>

#include "aof/aof-commons.hpp"
#include "interpolation-mocks.hpp"
#include "test-data.hpp"

    namespace aof {
        namespace physics {
            namespace tests {

                using namespace aof::interpolation::mocks;

                template<typename T>
                class GradShafranovModel_F : public ::testing::Test {
                protected:
                    double expected_R = default_expectations::position().R;
                    double expected_phi = default_expectations::position().phi;
                    double expected_Z = default_expectations::position().Z;

                    double expected_psi = default_expectations::bfield(T::COCOS).psi;

                    double expected_psi_R = default_expectations::bfield(T::COCOS).psi_R;
                    double expected_psi_Z = default_expectations::bfield(T::COCOS).psi_Z;
                    double expected_psi_RR = default_expectations::bfield(T::COCOS).psi_RR;
                    double expected_psi_RZ = default_expectations::bfield(T::COCOS).psi_RZ;
                    double expected_psi_ZZ = default_expectations::bfield(T::COCOS).psi_ZZ;

                    double expected_F = default_expectations::bfield(T::COCOS).F;
                    double expected_Fp = default_expectations::bfield(T::COCOS).Fp;
                    double expected_Fpp = default_expectations::bfield(T::COCOS).Fpp;

                    vector<3> expected_B = default_expectations::bfield(T::COCOS).B;
                    matrix<3, 3> expected_nabla_outerprod_B = default_expectations::bfield(T::COCOS).nabla_outerprod_B;

                public:

                    const MockInterp1D::expectations Fex;
                    const MockInterp2D::expectations psiex;

                    MockInterp2D mockPsi;
                    MockInterp1D mockF;

                    T sut;

                    GradShafranovModel_F() :
                        Fex(expected_psi, expected_F, expected_Fp, expected_Fpp),
                        psiex(expected_R, expected_Z, expected_psi, expected_psi_R, expected_psi_Z, expected_psi_RR, expected_psi_RZ, expected_psi_ZZ),
                        mockPsi(psiex),
                        mockF(Fex),
                        sut(mockPsi, mockF) {
                    }
                };
            }
        }
    }

#endif
