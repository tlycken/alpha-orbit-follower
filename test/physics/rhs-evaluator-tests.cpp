#include "aof/physics/guiding-center-rhs.hpp"
#include "rhs-evaluator-tests.hpp"
#include "global-magnetic-field-mocks.hpp"

using namespace aof::physics;
using namespace aof::physics::tests;
using namespace aof::physics::mocks;
using namespace testing;

TEST_F(GudingCenterRHS_F, Instantiation) {
    ASSERT_NO_THROW(rhs_t RHS(this->globalField, particle::alpha()));
}

TEST_F(GudingCenterRHS_F, Evaluation) {
    rhs_t rhs(this->globalField, this->p);

    rhs_t::state_t y;
    y << this->R, this->phi, this->Z, this->vp, this-> mu, this->phase;

    auto RHS = rhs(0.0, y);

    for(int i = 0; i < 4; ++i) {
        ASSERT_FLOAT_EQ(1.0, default_expectations::gc_rhs().RHS[i] / RHS(i)) << "i = " << i;
    }
}

int pull_rhs_evaluator_tests() { return 0; }
