#include "gtest/gtest.h"
#include <math.h>

#include "aof/numerics/interpolation.hpp"
#include "aof/physics/flux-function-interpolation.hpp"
#include "interpolation-tests.hpp"

#define VERBOSE_OUTPUT false

using namespace aof::interpolation;
using namespace aof::interpolation::tests;

typedef ::testing::Types<aof::interpolation::Interp1D, aof::physics::FluxFunctionInterpolation<Interp1D, 13>> Interpolation1DImpls;
TYPED_TEST_CASE(Interp1D_F, Interpolation1DImpls);

TYPED_TEST(Interp1D_F, Initialization) {
    ASSERT_NO_THROW({
        TypeParam tst(this->N, this->x, this->f);
    });
}

TYPED_TEST(Interp1D_F, CopyConstructor) {
    TypeParam i1(this->N, this->x, this->f);
    ASSERT_NO_THROW(TypeParam i2 = i1);

    ASSERT_NEAR(sin(5.5 * this->dx), this->getInterpObjectPtr()->evaluateAt(5.5 * this->dx), 1e-3);
}

TYPED_TEST(Interp1D_F, Evaluation) {
    this->on_interpolated_grid([this](double x) -> void {
        if (!VERBOSE_OUTPUT && !this->HasFatalFailure()) {
            ASSERT_NEAR(sin(x), this->getInterpObjectPtr()->evaluateAt(x), 1e-3);
        }
    });
}

TYPED_TEST(Interp1D_F, FirstOrderDerivative) {
    this->on_interpolated_grid([this](double x) -> void {
        if (!VERBOSE_OUTPUT && !this->HasFatalFailure()) {
            ASSERT_NEAR(cos(x), this->getInterpObjectPtr()->evaluateAt(x, 1), 1e-3);
        }
    });
}

TYPED_TEST(Interp1D_F, SecondOrderDerivative) {
    this->on_interpolated_grid([this](double x) -> void {
        if (!VERBOSE_OUTPUT && !this->HasFatalFailure()) {
            ASSERT_NEAR(-sin(x), this->getInterpObjectPtr()->evaluateAt(x, 2), 2e-2);
        }
    });
}

typedef ::testing::Types<aof::interpolation::Interp2D> Interpolation2DImpls;
TYPED_TEST_CASE(Interp2D_F, Interpolation2DImpls);

TYPED_TEST(Interp2D_F, Initialization) {
    ASSERT_NO_THROW({
        TypeParam tst(this->M, this->N, this->xmin, this->dx, this->ymin, this->dy, this->f);
    });
    ASSERT_NO_THROW({
        TypeParam tst(this->M, this->N, this->x, this->y, this->f);
    });
}

TYPED_TEST(Interp2D_F, CopyConstructor) {
    TypeParam i1(this->M, this->N, this->xmin, this->dx, this->ymin, this->dy, this->f);
    ASSERT_NO_THROW(TypeParam i2 = i1);
    TypeParam i3 = i1;
    double x = this->xmin + 3.5 * this->dx;
    double y = this->ymin + 3.5 * this->dy;

    ASSERT_NEAR(this->test_function(x, y), i3.evaluateAt(x, y), 1e-3);
}

TYPED_TEST(Interp2D_F, Evaluation) {
    this->on_interpolated_grid([this](double x, double y) -> void {
        if (!VERBOSE_OUTPUT && !this->HasFatalFailure()) {
            ASSERT_NEAR(
                this->test_function(x, y),
                this->getInterpObjectPtr()->evaluateAt(x, y),
                1e-3
            );
        }
    });
}

TYPED_TEST(Interp2D_F, EvaluationAfterDeletingInputData) {

    double a = this->x[this->M / 3] + this->dx / 3;
    double b = this->y[this->N / 3] + this->dx / 3;

    TypeParam itp = *(this->getInterpObjectPtr());

    if (this->x != 0) {
        delete[] this->x;
        this->x = 0;
    }
    if (this->y != 0) {
        delete[] this->y;
        this->y = 0;
    }
    for (int ii = 0; ii < this->N; ++ii) {
        if (this->f[ii] != 0) {
            delete[] this->f[ii];
            this->f[ii] = 0;
        }
    }
    if (this->f != 0) {
        delete[] this->f;
        this-> f = 0;
    }

    ASSERT_NEAR(
        this->test_function(a, b),
        itp.evaluateAt(a, b),
        1e-3
    );

}

TYPED_TEST(Interp2D_F, FirstOrderDerivative) {
    this->on_interpolated_grid([this](double x, double y) -> void {
        if (!VERBOSE_OUTPUT && !this->HasFatalFailure()) {
            double tol = 1e-2;
            ASSERT_NEAR(
                this->test_function_x_deriv(x, y),
                this->getInterpObjectPtr()->evaluateAt(x, y, 1, 0),
                tol
            );
            ASSERT_NEAR(
                this->test_function_y_deriv(x, y),
                this->getInterpObjectPtr()->evaluateAt(x, y, 0, 1),
                tol
            );
        }
    });
}


TYPED_TEST(Interp2D_F, SecondOrderDerivative) {
    this->on_interpolated_grid([this](double x, double y) -> void {
        double tol = 2e-1;
        if (true && !this->HasFatalFailure()) {
            ASSERT_NEAR(
                this->test_function_xx_deriv(x, y),
                this->getInterpObjectPtr()->evaluateAt(x, y, 2, 0),
                tol
            );
            ASSERT_NEAR(
                this->test_function_xy_deriv(x, y),
                this->getInterpObjectPtr()->evaluateAt(x, y, 1, 1),
                tol
            );
            ASSERT_NEAR(
                this->test_function_yy_deriv(x, y),
                this->getInterpObjectPtr()->evaluateAt(x, y, 0, 2),
                tol
            );
        }
    });
}


// Required to run tests from external libraries
// See stackoverflow.com/questions/17912988 for details
int pull_interpolationtests() {
    return 0;
}
