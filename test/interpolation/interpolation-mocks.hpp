#ifndef INTERPOLATION_MOCKS_HPP
#define INTERPOLATION_MOCKS_HPP

#include "aof/numerics/interpolation.hpp"
#include "gmock/gmock.h"

    namespace aof {
        namespace interpolation {
            namespace mocks {

                class MockInterp1D : public aof::interpolation::IInterp1D {
                public:
                    struct expectations {
                        double x;
                        double f;
                        double fp;
                        double fpp;

                        expectations(double x, double f, double fp, double fpp) : x(x), f(f), fp(fp), fpp(fpp) { }

                        expectations(const expectations &other) {
                            x = other.x;
                            f = other.f;
                            fp = other.fp;
                            fpp = other.fpp;
                        }
                    };

                    MockInterp1D() : expected(0, 0, 0, 0) { };

                    explicit MockInterp1D(const expectations &ex) : expected(ex) {
                        setupDefaultActions();
                    };

                    MockInterp1D(const MockInterp1D &other) : expected(other.expected) {
                        setupDefaultActions();
                    };

                    MOCK_CONST_METHOD1(evaluateAt, double(double));
                    MOCK_CONST_METHOD2(evaluateAt, double(double, int));

                    void setupDefaultActions() {
                        setupDefaultActions(expected);
                    }

                    void setupDefaultActions(const expectations &ex) {
                        ON_CALL(*this, evaluateAt(ex.x))
                        .WillByDefault(::testing::Return(ex.f));
                        ON_CALL(*this, evaluateAt(ex.x, 0))
                        .WillByDefault(::testing::Return(ex.f));
                        ON_CALL(*this, evaluateAt(ex.x, 1))
                        .WillByDefault(::testing::Return(ex.fp));
                        ON_CALL(*this, evaluateAt(ex.x, 2))
                        .WillByDefault(::testing::Return(ex.fpp));

                        EXPECT_CALL(*this, evaluateAt(this->expected.x)).Times(::testing::AnyNumber());
                        EXPECT_CALL(*this, evaluateAt(this->expected.x, ::testing::Le(2))).Times(::testing::AnyNumber());
                    }
                private:
                    expectations expected;
                };

                class MockInterp2D : public aof::interpolation::IInterp2D {
                public:
                    struct expectations {
                        double x;
                        double y;
                        double F;
                        double Fx;
                        double Fy;
                        double Fxx;
                        double Fxy;
                        double Fyy;

                        expectations(double x, double y, double F, double Fx, double Fy, double Fxx, double Fxy, double Fyy) : x(x), y(y), F(F), Fx(Fx), Fy(Fy), Fxx(Fxx), Fxy(Fxy), Fyy(Fyy) { }

                        expectations(const expectations &other) : x(other.x), y(other.y), F(other.F), Fx(other.Fx), Fy(other.Fy), Fxx(other.Fxx), Fxy(other.Fxy), Fyy(other.Fyy) { }
                    };

                    MockInterp2D() : expected(0, 0, 0, 0, 0, 0, 0, 0) { };

                    explicit MockInterp2D(const expectations &ex) : expected(ex) {
                        setupDefaultActions();
                    }

                    MockInterp2D(const MockInterp2D &other) : expected(other.expected) {
                        setupDefaultActions();
                    };

                    MOCK_CONST_METHOD2(evaluateAt, double(double, double));
                    MOCK_CONST_METHOD4(evaluateAt, double(double, double, int, int));

                    void setupDefaultActions() {
                        ON_CALL(*this, evaluateAt(expected.x, expected.y)).WillByDefault(::testing::Return(expected.F));
                        ON_CALL(*this, evaluateAt(expected.x, expected.y, 0, 0)).WillByDefault(::testing::Return(expected.F));


                        ON_CALL(*this, evaluateAt(expected.x, expected.y, 1, 0))
                        .WillByDefault(::testing::Return(expected.Fx));
                        ON_CALL(*this, evaluateAt(expected.x, expected.y, 0, 1))
                        .WillByDefault(::testing::Return(expected.Fy));
                        ON_CALL(*this, evaluateAt(expected.x, expected.y, 2, 0))
                        .WillByDefault(::testing::Return(expected.Fxx));
                        ON_CALL(*this, evaluateAt(expected.x, expected.y, 1, 1))
                        .WillByDefault(::testing::Return(expected.Fxy));
                        ON_CALL(*this, evaluateAt(expected.x, expected.y, 0, 2))
                        .WillByDefault(::testing::Return(expected.Fyy));

                        EXPECT_CALL(*this, evaluateAt(this->expected.x, this->expected.y)).Times(::testing::AnyNumber());
                        EXPECT_CALL(*this, evaluateAt(this->expected.x, this->expected.y, ::testing::Le(2), ::testing::Le(2))).Times(::testing::AnyNumber());
                    }

                    void setupDefaultActions(const expectations &ex) {
                        this->expected = ex;
                        setupDefaultActions();
                    }
                private:
                    expectations expected;
                };
            }
        }
    }

#endif