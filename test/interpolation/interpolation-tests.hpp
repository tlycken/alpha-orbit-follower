#ifndef INTERPOLATION_TESTS_HPP
#define INTERPOLATION_TESTS_HPP

#include "gtest/gtest.h"
#include "aof/numerics/interpolation.hpp"
#include <cmath>

namespace aof {
    namespace interpolation {
        namespace tests {

            template <typename T>
            class Interp1D_F : public ::testing::Test {
            protected:
                int N;
                double xmin, dx;
                double* x;
                double* f;

                T* _interp1d = 0;

            public:
                Interp1D_F() {
                    N = 15;
                    xmin = 0;
                    dx = M_PI / (N - 1);
                    x = new double[N];
                    f = new double[N];

                    for (int ii = 0; ii < N; ++ii) {
                        x[ii] = xmin + ii * dx;
                        f[ii] = sin(x[ii]);
                    }
                }

                T* getInterpObjectPtr() {
                    if (_interp1d == 0) {
                        _interp1d = new T(N, x, f);
                    }
                    return _interp1d;
                }

                ~Interp1D_F() {
                    if (_interp1d != 0) {
                        delete _interp1d;
                    }
                    if (x != 0) {
                        delete[] x;
                    }
                    if (f != 0) {
                        delete[] f;
                    }
                }

                void on_interpolated_grid(std::function<void(double)> f) {
                    double dxp = dx / 10;
                    double xmax = xmin + (N - 1) * dx;

                    for (double xp = xmin + 2 * dx; xp < xmax - 2 * dx; xp += dxp) {
                        f(xp);
                    }
                }
            };

            template<typename T>
            class Interp2D_F : public ::testing::Test {
            protected:
                int M, N;
                double xmin, dx, ymin, dy;
                double* x;
                double* y;
                double** f;

                T* _interp2d = 0;

            public:
                Interp2D_F() {
                    M = 40;
                    xmin = 0;
                    dx = M_PI / (M - 1);
                    x = new double[M];

                    N = 65;
                    ymin = 0;
                    dy = M_PI / (N - 1);
                    y = new double[N];

                    f = new double*[N];

                    for (int ii = 0; ii < N; ++ii) {
                        y[ii] = ymin + ii * dy;
                        f[ii] = new double[M];
                        for (int jj = 0; jj < M; ++jj) {
                            x[jj] = xmin + jj * dx;
                            f[ii][jj] = test_function(x[jj], y[ii]);
                        }
                    }
                }

                ~Interp2D_F() {
                    if (x != 0) {
                        delete[] x;
                        x = 0;
                    }
                    if (y != 0) {
                        delete[] y;
                        y = 0;
                    }
                    if (f != 0) {
                        for (int ii = 0; ii < N; ++ii) {
                            delete[] f[ii];
                            f[ii] = 0;
                        }
                        delete[] f;
                        f = 0;
                    }

                    if (_interp2d != 0) {
                        delete _interp2d;
                        _interp2d = 0;
                    }
                }

                T* getInterpObjectPtr() {
                    if (_interp2d == 0) {
                        _interp2d = new T(M, N, x, y, f);
                    }
                    return _interp2d;
                }

                double test_function(double xp, double yp) {
                    // test function: f(x,y) = x^2 + y^2 + x*y
                    return cos(xp) * sin(yp) + cos(xp) + sin(yp);
                }

                double test_function_x_deriv(double xp, double yp) {
                    return -sin(xp) * sin(yp) - sin(xp);
                }

                double test_function_y_deriv(double xp, double yp) {
                    return cos(xp) * cos(yp) + cos(yp);
                }

                double test_function_xx_deriv(double xp, double yp) {
                    return -cos(xp) * sin(yp) - cos(xp);
                }

                double test_function_xy_deriv(double xp, double yp) {
                    return -sin(xp) * cos(yp);
                }

                double test_function_yy_deriv(double xp, double yp) {
                    return - cos(xp) * sin(yp) - sin(yp);
                }

                void on_interpolated_grid(std::function<void(double, double)> f) {
                    double dxp = dx;
                    double dyp = dy;

                    double xmax = xmin + (M - 1) * dx;
                    double ymax = ymin + (N - 1) * dy;

                    for (double xp = xmin + 2.3 * dx; xp < xmax - 2 * dx; xp += dxp) {
                        for (double yp = ymin + 2.3 * dy; yp < ymax - 2 * dy; yp += dyp) {
                            f(xp, yp);
                        }
                    }
                }
            };
        }
    }
}

#endif
