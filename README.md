#Alpha-particle orbit follower

This project is part of a masters' thesis at KTH by Tomas Lycken.

### Toolchain

The tools required to build this project are listed below:

* [CMake](http://cmake.org/)
* A build system supported by CMake, e.g. [GNU Make](http://www.gnu.org/software/make/)
* A C++ compiler suite, e.g. [GCC](http://gcc.gnu.org/)

### Building and testing

Before building the main project, you must [build the binary dependencies that are bundled with the code](https://bitbucket.org/tlycken/alpha-orbit-follower/wiki/Building_and_testing).

When that's done, you build the project by running, from the project root:

    $> mkdir build && cd build && cmake .. && make

i.e.

   1. Create a new directory in the project root, call it `build`
   2. Navigate to the new directory
   3. Run `cmake ..` to configure the build system
   4. Run `make` to build the project

If you want to run the tests, you can run `make install` from within `build`. That will move two binaries into `build/bin`, so after you've `make install`ed, you can run `bin/test-runner` from within `build`, to run the unit-test suite.

### Running the program

In order to run the program, a few extra things need to be done.

   1. Provide an input file. The input should be in xml format, corresponding to the example in `test/data/grad-shafranov.xml`; data from an actual ITER scenario can be used by navigating to `input` and calling the MATLAB script `AOFXmlFromGEQDSK` (start with `help AOFXmlFromGEQDSK` in the MATLAB prompt to see an example of its call signature).

   2. If the input file is called `input/iter.xml` you can run `cmake ..` from `build` to copy the input file into the build directory. (You can of course do that manually, too.)

   3. The program does not create a folder structure for the output files - you'll need to provide that yourself. In the default case, creating `output` and `output/diagnostics` within `build` is sufficient.

   4. Run the program by executing

       bin/alpha-orbit-follower iter.xml

    (given that you have the configuration described above). When the program finishes, you'll have all the output files in `build/output`, ready for postprocessing.