using HDF5, JLD, DataFrames
using PostProcess, AOFoutput, Utilities, PsiTubeVolumes

function processall(root,machine)
    println("Processing $machine in folder $root...")
    sall = AOFoutput.getsummary(root)

    addPsi!(sall, root, machine)

    sloops = AOFoutput.getloops(root)
    sloss = AOFoutput.getwallhits(root)

    Nbins = 512
    psimin,psimax = 0.,2.
    psibins,initialhist = histogram(Nbins,psimin,psimax,zip(sloops[:InitialPsi], sloops[:Weight]))

    psibins,finalhist = walktraces!(sloops, zeros(Nbins), psimin, psimax, root, machine)

    # initialpowerdensity = powerdensity(initialhist, vols)
    # finalpowerdensity = powerdensity(finalhist, vols)
    # deltapowerdensity = finalpowerdensity .- initialpowerdensity

    # initcumpower = cumsum(power(initialhist))
    # finalcumpower = cumsum(power(finalhist))

    # psibins, allhist = histogram(0.,1.,zip(sall[:InitialPsi], sall[:Weight]), Nbins)
    # psibins, losthist = histogram(0.,1.,zip(sloss[:InitialPsi], sloss[:Weight]), Nbins)
    myjldpath=abspath(joinpath(root,"..","processed.jld"))
    jldopen(myjldpath, "w") do f
        @write f sall
        @write f sloops
        @write f sloss
        @write f Nbins
        @write f psibins
        @write f initialhist
        @write f finalhist
    end

    println("Processed data saved to $myjldpath")
                                # initialpowerdensity,
                                # finalpowerdensity,
                                # deltapowerdensity,
                                # initcumpower,
                                # finalcumpower,
                                # allhist,
                                # losthist

    # writetable("$root/walkedtraces.csv", sloops)
end

function loadprocessed(root)
    jldopen("$root/processed.jld") do f
        read(f, "sall"),
        read(f, "sloops"),
        read(f, "sloss"),
        read(f, "Nbins"),
        read(f, "psibins"),
        read(f, "initialhist"),
        read(f, "finalhist")
        # read(f, "initialpowerdensity"),
        # read(f, "finalpowerdensity"),
        # read(f, "deltapowerdensity"),
        # read(f, "initcumpower"),
        # read(f, "finalcumpower"),
        # read(f, "allhist"),
        # read(f, "losthist")
    end
    # sloops = readtable("$root/walkedtraces.csv")

    # [:Nbins=>Nbins,:psibins=>psibins,:initialhist=>initialhist,:finalhist=>finalhist,:volumes=>vols,:allhist=>allhist,:losthist=>losthist,:loopsummary=>sloops]
end

power(w) = 3.5*1.6022e-19/1e-9 * w

powerdensity(h,vs) = power(h)./vs

