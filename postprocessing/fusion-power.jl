module FusionPower

using Grid, LibExpat, PyPlot

using PostProcess, AOFinput, AOFoutput

DT = (34.3827^2,
    1_124_656,
    1.17302e-9,
    1.51361e-2,
    7.51886e-2,
    4.60643e-3,
    1.35000e-2,
    -1.06750e-4,
    1.36600e-5,
    0.2e3,
    100e3)

He3D = (68.7508^2,
    1_124_572,
    5.51036e-10,
    6.41918e-3,
    -2.02896e-3,
    -1.91080e-5,
    1.35776e-4,
    0.,
    0.,
    0.5e3,
    190e3)

DD = (31.3970,
    937_814,
    5.65718e-12,
    3.41267e-3,
    1.99167e-3,
    0.,
    1.05060e-5,
    0.,
    0.,
    0.2e3,
    100e3)

function BorschAndHale(T_eV, params)
    if T_eV == 0
        return 0.
    end
    BG2, mc2, C1, C2, C3, C4, C5, C6, C7, Tmin, Tmax = params

    # Convert eV to keV
    Ti = T_eV * 1e-3

    theta = Ti ./ (1 .- Ti .* (C2 .+ Ti .* (C4 .+ Ti .* C6)) ./ (1 .+ Ti .* (C3 .+ Ti .* (C5 .+ Ti .* C7))))
    ksi = (BG2 ./ (4 * theta)).^(1/3)
    sigma_v_cm3 = C1 * theta .* sqrt(ksi ./ (mc2 .* Ti.^3)) .* exp(-3*ksi)

    # Convert cm^3 to m^3
    sigma_v_cm3 * 1e-6
end
BorschAndHale_DT(T_eV) = BorschAndHale(T_eV, DT)

function fusionpower(root)

# Read input for plasma shape

doc = AOFinput.parsefile(joinpath(root,"..", "jet.xml"))

Rs = AOFinput.uniformgrid(AOFinput.namedgrid(AOFinput.gradshafranov(doc), "R"))
Zs = AOFinput.uniformgrid(AOFinput.namedgrid(AOFinput.gradshafranov(doc), "Z"))'
NR, NZ = length(Rs), length(Zs)

psi_input = AOFinput.fieldvalues(AOFinput.namedfield(AOFinput.gradshafranov(doc), "psi"), NR, NZ)
psimag, psibry = AOFinput.psilim(doc)

Rlcfs, Zlcfs = AOFinput.lastclosed(doc)


# Read output for plasma shape

fielddump = AOFoutput.getfields(root)
psi_output = Field2D(fielddump, :R, :Z, :psi)
plasmadump = AOFoutput.getplasmas(root)
psiitp_out = PostProcess.interp2D(psi_output)

# Temperature profile

# T0 = 25e3
# Tedge = 3e3
# nD = nT = 1e20/2
EnergyAlpha = 3.5e6 * 1.6022e-19
EnergyNeutron = 14.1e6 * 1.6022e-19
# NT = 200

# psirng = range(psimag, (psibry-psimag)/(NT-1), NT)
# psilinnorm = ([psirng].-psimag) ./ (psibry-psimag)
# TeV = T0 .+ (Tedge - T0) * psilinnorm

psirng = AOFinput.uniformgridrange(AOFinput.namedgrid(doc[xpath"//plasma/species"][3],"psi"))
TeV = AOFinput.fieldvalues(AOFinput.namedfield(doc[xpath"//plasma/species"][3],"temperature"), length(psirng))
nD = nT = AOFinput.fieldvalues(AOFinput.namedfield(doc[xpath"//plasma/species"][3],"density"), length(psirng))[1]

Titp = CoordInterpGrid(psirng, TeV, BCperiodic, InterpLinear)

# Identify 2D gridcells inside plasma

sign_dpsi = sign(psibry-psimag)
psi_size_cond = psi_input*sign_dpsi .< psibry*sign_dpsi

minR, maxR = extrema(Rlcfs)
inside_R_box  = minR .< Rs .< maxR

minZ, maxZ = extrema(Zlcfs)
inside_Z_box  = minZ .< Zs .< maxZ

II = psi_size_cond & (inside_R_box * inside_Z_box)

# Interpolate temperature + calculate <sigma v> on 2D grid

TeV_II = map(psi -> Titp[psi], psi_input) .* II
sigma_v_in = map(T -> BorschAndHale_DT(T), TeV_II)
sigma_v_output_itp = interp2D(plasmadump, :R, :Z, :Rate)
nD_itp = interp2D(plasmadump, :R, :Z, :DeuteriumDensity)
nT_itp = interp2D(plasmadump, :R, :Z, :TritiumDensity)

sigma_v_out = map((r,z) -> sigma_v_output_itp[r,z] / (nD_itp[r,z] * nD_itp[r,z]),
                  Rs.*ones(size(Zs)), Zs.*ones(size(Rs)))

for sigma_v in {sigma_v_in, sigma_v_out}
    if sigma_v == sigma_v_in
        println("From input:")
    elseif sigma_v == sigma_v_out
        println("From output:")
    end
    # Volume integrated particle source rates and power

    dR = Rs[2]-Rs[1]
    dZ = Zs[1,2]-Zs[1,1]
    dV = 2*pi*dR*dZ * Rs
    Nfusion = sum(sum(dV .* sigma_v .* nD .* nT))
    Pneutron = Nfusion * EnergyNeutron
    Palpha   = Nfusion * EnergyAlpha
    Pfusion  = Pneutron + Palpha

    # Display results

    @printf("Fusion rate:  = %f 10^20/ns\n", Nfusion/1e20)
    @printf("Neutron power = %4.2f MW\n", Pneutron*1e-6)
    @printf("Alpha power   = %4.2f MW\n", Palpha*1e-6)
    @printf("Fusion power  = %4.2f MW\n", Pfusion*1e-6)

end



end

end #module
