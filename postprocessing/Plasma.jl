module Plasma

using Grid

import Diagnostics: readplasma, readfields, interp2D
export sigmav, ionT

#plasmadata = readplasma("output/")

const params = (
        34.3827,
        1_124_656,
        (1.17302e-9,
         1.51361e-2,
         7.51886e-2,
         4.60643e-3,
         1.35000e-2,
         -1.06750e-4,
         1.36600e-5),
        0.2, 
        100)

function sigmav(T::Real)
    TkeV = 1e-3T
    BG::Float32, mc2::Float32, C::NTuple{7,Float32}, Tmin::Float32, Tmax::Float32 = params
    
    if !(Tmin < TkeV < Tmax)
        return 0
    end

    theta = TkeV / (1 - 
            TkeV * (C[2] + TkeV * (C[4] + TkeV * C[6])) / 
            (1 + TkeV * (C[3] + TkeV * (C[5] + TkeV * C[7])))
        )
    ksi = cbrt(BG^2 / (4theta))
    C[1] * theta * sqrt(ksi/(mc2*TkeV^3)) * exp(-3ksi) * 1e6
end

#tempitp = interp2D(plasmadata, :R, :Z, :DeuteriumTemperature)

function ionT(R::Real, Z::Real)
    tempitp[R,Z]
end

end

