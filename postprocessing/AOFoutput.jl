module AOFoutput

export 
    getdoc,
    getfields,
    getplasmas,
    getsummary,
    gettrace,
    getpsiitp

import AOFinput: psilim, chamber
using PostProcess
using DataFrames, Grid, GZip

dataframes = {String=>DataFrame}
itps = {(DataFrame,Symbol)=>InterpGrid}

function getframe(path; save=true)
    if !haskey(dataframes,path)
        #println("Reading $path...")
        if isfile(path)
            df = readtable(path)
        elseif isfile("$path.gz")
            fh = GZip.open("$path.gz")
            df = readtable(fh)
            close(fh)
        else
            throw(ErrorException("Neither of $path and $path.gz exist!"))
        end

        if save
           dataframes[path] = df
        end
        return df
    end
    dataframes[path]
end

function getitp2(df,symbol)
    if !haskey(itps,(df,symbol))
        #println("Interpolating $(symbol)...")
        itps[(df,symbol)] = interp2D(df, :R, :Z, :psi)
    end
    itps[(df,symbol)]
end


getfields(root) = getframe(joinpath(root, "diagnostics/field-dump.txt"))
getplasmas(root) = getframe(joinpath(root, "diagnostics/plasma-dump.txt"))
function getsummary(root)
    s = getframe(joinpath(root, "summary.txt"))
    s[:Reason] = map(s -> symbol(s), s[:Reason])
    pool!(s, :Reason)
    s
end
getsummary(root, endreason) = getsummary(root)[getsummary(root)[:Reason].==endreason,:]
getloops(root) = getsummary(root,:loop)
getwallhits(root) = getsummary(root,:wall)
function gettracerow(root,idx::Integer)
    s = getsummary(root)
    s[first(find(s[:Index].==idx)),:]
end

function gettrace(root,machine,idx::Integer; save=false)
    tr = getframe(joinpath(root, "traces/trace-$idx.txt"); save=save)
    #@show idx,gettracerow(root,idx)[1,:Reason], gettracerow(root,idx)[1,:Reason] == :wall
    # if gettracerow(root,idx)[1,:Reason] == :wall
        return tr
    # end
    # throwaway_warmup(tr,root,machine)
end

function throwaway_warmup(tr,root,machine)
    signchanges = sign(tr[2:end,:VParallelDot].*tr[1:end-1,:VParallelDot]) .< 0
    insides = PostProcess.insidelcfs(root,machine,tr[2:end,:R],tr[2:end,:Z])
    firstidx = first(find(signchanges & insides))+1
    tr[firstidx:end,:]
end

gettrace(root,machine,idxs; save=false) = map(i->gettrace(root,machine,i; save=save), idxs)

getpsiitp(root) = getitp2(getfields(root), :psi)

clear() = (empty!(dataframes); nothing)

end #module
