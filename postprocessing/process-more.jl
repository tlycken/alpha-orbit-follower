using AOFinput, AOFoutput
using HDF5, JLD, ProgressMeter, DataFrames, DataArrays

function processmore(root,machine)
    sloops,sloss,Nbins,psibins,initialhist,finalhist,vols =     jldopen("$root/processed.jld") do f
        read(f, "sloops"),
        read(f, "sloss"),
        read(f, "Nbins"),
        read(f, "psibins"),
        read(f, "initialhist"),
        read(f, "finalhist"),
        read(f, "vols")
    end

    Nparticles = size(sloops,1)

    classifications = PooledDataArray(Symbol,Nparticles)
    p = Progress(Nparticles, .1, "Classifying confined traces in $root...", 50)

    for i in 1:Nparticles
        next!(p)
        idx = sloops[i,:Index]
        trace = AOFoutput.gettrace(root,idx;save=false)
        vpmin,vpmax = extrema(trace[:VParallel])
        classifications[i] = sign(vpmin*vpmax) > 0 ? :passing : :trapped
    end

    sloops[:Classification] = classifications

    jldopen("$root/processed.jld", "w") do f
        @write f sloops
        @write f sloss
        @write f Nbins
        @write f psibins
        @write f initialhist
        @write f finalhist
        @write f vols
    end

    nothing
end