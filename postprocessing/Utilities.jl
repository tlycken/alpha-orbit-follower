module Utilities

export histogram, histidx

function histidx(xmin, xmax, Nbins::Integer, x)
    if x <= xmin
        return 1 
    elseif x >= xmax
        return Nbins
    else
        return iceil((x-xmin)/((xmax-xmin)/Nbins))
    end
end

function histbins(xmin, xmax, Nbins)
    range(xmin, binwidth(xmin,xmax,Nbins), Nbins+1)[1:Nbins]
end

binwidth(xmin,xmax,Nbins)=(xmax-xmin)/Nbins

function histogram(N::Integer, xmin, xmax, data)
    bins = zeros(Float64, N)
    histogram!(bins,xmin,xmax,data)
end

function histogram!(bins, xmin, xmax, data)
    N = length(bins)

    for (x,f) in data
        bins[histidx(xmin,xmax,N,x)] += f
    end
    histbins(xmin,xmax,N), bins
end

function reducehistogram(h)
    @assert length(h) % 2 == 0
    (h[1:2:end-1] .+ h[2:2:end])./2
end

function reducehistogram(h, Nreducts::Integer)
    h = copy(h)
    for i = 1:Nreducts
        h = copy(reducehistogram(h))
    end
    h
end

end
