module MakePlots

using HDF5, JLD, PyPlot, DataFrames, LibExpat

using AOFinput, AOFoutput, Utilities
import AOFinput.chamber
import Base.display

export display

type DataSet
    sloops::DataFrame
    sloss::DataFrame
    Nbins::Integer
    psibins::Vector{Float64}
    initialhist::Vector{Float64}
    finalhist::Vector{Float64}
    vols::Vector{Float64}
    input::ETree

    root::String
    machine::String
end

display(d::DataSet) = display(nothing)

machines = [Symbol=>DataSet]

# default_theme = Theme(minor_label_font="Palatino", major_label_font="Palatino",panel_fill=color("white"))

function loaddata(rt, mchn)
    if (mchn=="jet")
        machines[:JET] = DataSet((jldopen(joinpath(rt, "processed.jld")) do f
                read(f, "sloops"),
                read(f, "sloss"),
                read(f, "Nbins"),
                read(f, "psibins"),
                read(f, "initialhist"),
                read(f, "finalhist"),
                read(f, "vols")
            end)..., AOFinput.getdoc(joinpath(rt, "$mchn.xml")), rt, mchn)
    elseif mchn=="iter"
        machines[:ITER] = DataSet((jldopen(joinpath(rt, "processed.jld")) do f
                read(f, "sloops"),
                read(f, "sloss"),
                read(f, "Nbins"),
                read(f, "psibins"),
                read(f, "initialhist"),
                read(f, "finalhist"),
                read(f, "vols")
            end)..., AOFinput.getdoc(joinpath(rt, "$mchn.xml")), rt, mchn)
    else
        throw(ErrorException("Invalid machine name: $mchn (must be \"jet\" or \"iter\""))
    end
    nothing
end

if isdefined(:Gadfly)

    function chamber()
        Rwall, Zwall = chamber(input)
        Rlc, Zlc = lastclosed(input)
        magaxR, magaxZ = magaxis(input)

        # plot(
            layer(x=[Rwall,Rwall[1]], y=[Zwall,Zwall[1]], Geom.point, 
                Geom.line(preserve_order=true),
                Theme(default_color=color("grey"))), 
            layer(x=[Rlc,Rlc[1]], y=[Zlc,Zlc[1]], 
                Geom.line(preserve_order=true), 
                Theme(default_color=color("red"),line_width=0.1mm)),
            # layer(x=[magaxR magaxR], y=[magaxZ magaxZ],
            #     Geom.point, 
            #     Theme(default_color=color("red"),line_width=0.1mm)),
            default_theme,
            Guide.xlabel("R [m]"),
            Guide.ylabel("Z [m]")
            
        # )
    end

    function orbits()
        # set_default_plot_size(4inch,8inch)
        elements = {};

        idxs = sloops[sloops[:Classification].==:trapped,:Index]
        push!(elements, tracelayer(idxs[1], color("blue")))

        idxs = sloops[sloops[:Classification].==:passing,:Index]
        push!(elements, tracelayer(idxs[1], color("green")))

        idxs = sloss[:Index]
        push!(elements, tracelayer(idxs[1], color("red")))

        append!(elements, [chamber()...])

        plot(elements..., Scale.x_continuous(minvalue=1,maxvalue=4.5))
    end

    
    function deltapowerdensity(k::Integer)
        psis = Utilities.reducehistogram(psibins,k)
        deltapower = Utilities.reducehistogram(powerdensity(finalhist.-initialhist, vols),k)

        # plot(x=1:length(deltapower),y=deltapower,Geom.line(preserve_order=true))

        plot(x=psis[2:end],y=deltapower[2:end],Geom.point,
             layer(Geom.line,Theme(default_color=color("red"))),
             layer(x=psis,y=zeros(size(psis)), Geom.line, Theme(default_color=color("gray"),line_width=0.1mm)),
             Guide.xlabel("psi"),
             Guide.ylabel("Power density difference [MW/m^3]"),
             Theme(panel_fill=color("white")))
    end

    function powerdensities(k::Integer)
        psis = Utilities.reducehistogram(psibins,k)
        initpower = Utilities.reducehistogram(powerdensity(initialhist,vols),k)
        finalpower = Utilities.reducehistogram(powerdensity(finalhist,vols),k)

        plot(
            layer(x=psis[2:end], y=initpower[2:end], Theme(default_color=color("green")), Geom.line),
            layer(x=psis[2:end], y=finalpower[2:end], Theme(default_color=color("red")), Geom.line)
        )
    end

    function makeplots(plotsfolder)
        orbs = orbits()
        draw(PDF(joinpath(plotsfolder, "orbit-classification.pdf"),4inch,8inch), orbs)
    end


end

if isdefined(:PyPlot)

    PyPlot.rc("text", usetex=true)
    PyPlot.rc("font", family="mathpazo")

    function chamber(machine::DataSet)
        Rwall,Zwall = AOFinput.chamber(machine.input)
        Rlc,Zlc = lastclosed(machine.input)
        magaxR, magaxZ = magaxis(machine.input)

        f = plot([Rwall,Rwall[1]],[Zwall,Zwall[1]],"grey"; linewidth=1)
        axis(:equal)
        plot([Rlc,Rlc[1]],[Zlc,Zlc[1]],"r--"; linewidth=.5, figure=f)
        psiitp = AOFoutput.getpsiitp(joinpath(machine.root,"output"))
        Rgrid = linspace(extrema(Rwall)...,100)
        Zgrid = linspace(extrema(Zwall)...,100)
        cs = contour(Rgrid,Zgrid,[psiitp[r,z] for z in Zgrid, r in Rgrid], colors="lightgrey", figure=f, linewidth=.1)
        # clabel(cs, inline=1, fmt="%2.0f")
        xlim(extrema(Rwall)...)
        xlabel(L"$R$ [m]")
        ylabel(L"$Z$ [m]")
    end

    orbits(machine::Symbol) = orbits(machines[machine])
    function orbits(machine::DataSet)
        p = chamber(machine)

        idxs = machine.sloops[machine.sloops[:Classification].==:passing,:Index]
        trace = gettrace(joinpath(machine.root,"output"),idxs[2]; compressed=true)
        plot([trace[:R],trace[1,:R]], [trace[:Z],trace[1,:Z]],"g";linewidth=1.4, figure=p, label="Passing")

        idxs = machine.sloops[machine.sloops[:Classification].==:trapped,:Index]
        trace = gettrace(joinpath(machine.root,"output"),idxs[1]; compressed=true)
        plot([trace[:R],trace[1,:R]], [trace[:Z],trace[1,:Z]],"b";linewidth=1.4, figure=p, label="Trapped")

        idxs = machine.sloss[:Index]
        trace = gettrace(joinpath(machine.root,"output"),idxs[9]; compressed=true)
        plot(trace[:R], trace[:Z],"r";linewidth=1.4, figure=p, label="Lost")

        legend()
        gcf()
    end

    function promptlossfraction()
        psimin,psimax = 0.,2.
        Nbins = 32

        jetloops,jetlosses =  machines[:JET].sloops, machines[:JET].sloss
        jetpsis = [jetlosses[:InitialPsi],jetloops[:InitialPsi]]
        jetweights = [jetlosses[:Weight],jetloops[:Weight]]


        psibins, jeth0 = Utilities.histogram(Nbins,psimin,psimax,zip(jetpsis,jetweights))
        psibins, jethloss = Utilities.histogram(Nbins,psimin,psimax,zip(jetlosses[:InitialPsi],jetlosses[:Weight]))
        jeth = jethloss./jeth0

        iterloops,iterlosses = machines[:ITER].sloops, machines[:ITER].sloss
        iterpsis = [iterlosses[:InitialPsi],iterloops[:InitialPsi]]
        iterweights = [iterlosses[:Weight],iterloops[:Weight]]

        # plot(jetpsis,power(jetweights),",",label="JET")
        # plot(iterpsis,power(iterweights),",",label="ITER")
        # legend(loc=:best)

        psibins, iterh0 = Utilities.histogram(Nbins,psimin,psimax,zip(iterpsis,iterweights))
        psibins, iterhloss = Utilities.histogram(Nbins,psimin,psimax,zip(iterlosses[:InitialPsi],iterlosses[:Weight]))
        iterh = power(iterhloss)./power(iterh0)

        bw = Utilities.binwidth(psimin,psimax,Nbins)

        f = figure()
        bar(psibins, iterh, width=.4bw,color="r",label="ITER",figure=f)
        bar(psibins.+0.4bw, jeth, width=.4bw,color="g",label="JET", figure=f)
        xlim([.5,1])
        xlabel(L"$\psi$ (normalized)")
        ylabel("Loss fraction")
        legend(loc=:best)
        gcf()
    end

    function promptlostpower()
        psimin,psimax = 0.,2.
        Nbins = 32

        jetlosses =  machines[:JET].sloss
        jetpsis = jetlosses[:InitialPsi]
        jetperm = sortperm(jetpsis)
        jetweights = jetlosses[jetperm,:Weight]

        f = plot(jetpsis[jetperm], cumsum(power(jetweights)),color="g",label="JET")

        iterlosses =  machines[:ITER].sloss
        iterpsis = iterlosses[:InitialPsi]
        iterperm = sortperm(iterpsis)
        iterweights = iterlosses[iterperm,:Weight]

        plot(iterpsis[iterperm], cumsum(power(iterweights)),color="r",label="ITER",figure=f)
        xlabel(L"$\psi$ (normalized)")
        ylabel("Power loss (cumulative) [MW]")
        legend(loc=:best)
        xlim(0.4,1)
        gcf()
    end        

    function getpowerdensities()
        iterloops = machines[:ITER].sloops
        iterpsi0s, iterpsibars, iterweights = iterloops[:InitialPsi], iterloops[:PsiBar], iterloops[:Weight]

        psimin,psimax = 0.,2.
        Nbins = 512
        psibins, iterh0 = Utilities.histogram(Nbins,psimin,psimax,zip(iterpsi0s,iterweights))
        psibins, iterh1 = Utilities.histogram(Nbins,psimin,psimax,zip(iterpsibars,iterweights))

        psibins = Utilities.histbins(psimin,psimax,16)
        iterh0red = Utilities.reducehistogram(iterh0,5)
        iterh1red = Utilities.reducehistogram(iterh1,5)
        itervols = machines[:ITER].vols
        itervolsred = mapreduce(i->itervols[i:32:end], .+, 1:8)
        iterP0 = powerdensity(iterh0red,itervolsred)
        iterP1 = powerdensity(iterh1red,itervolsred)

        jetloops = machines[:JET].sloops
        jetpsi0s, jetpsibars, jetweights = jetloops[:InitialPsi], jetloops[:PsiBar], jetloops[:Weight]

        psimin,psimax = 0.,2.
        Nbins = 512
        psibins, jeth0 = Utilities.histogram(Nbins,psimin,psimax,zip(jetpsi0s,jetweights))
        psibins, jeth1 = Utilities.histogram(Nbins,psimin,psimax,zip(jetpsibars,jetweights))

        psibins = Utilities.histbins(psimin,psimax,16)
        jeth0red = Utilities.reducehistogram(jeth0,5)
        jeth1red = Utilities.reducehistogram(jeth1,5)
        jetvols = machines[:JET].vols
        jetvolsred = mapreduce(i->jetvols[i:32:end], .+, 1:8)
        jetP0 = powerdensity(jeth0red,jetvolsred)
        jetP1 = powerdensity(jeth1red,jetvolsred)

        psibins, iterP0, iterP1, jetP0, jetP1
    end

    function powerdensities()
        psibins, iterP0, iterP1, jetP0, jetP1 = getpowerdensities()

        plot(psibins,iterP0,"r--",label="ITER, initial")
        plot(psibins,iterP1,"r",label="ITER, final")
        xlim(0,1)
        legend()
        xlabel(L"$\psi$ (normalized)")
        ylabel(L"Power density $P$ [MW/m\textsuperscript{3}]")
        f1 = gcf()
        figure()
        plot(psibins,jetP0,"g--",label="JET, initial")
        plot(psibins,jetP1,"g",label="JET, final")

        xlim(0,1)
        legend()
        xlabel(L"$\psi$ (normalized)")
        ylabel(L"Power density $P$ [MW/m\textsuperscript{3}]")
        f1, gcf()
    end

    function deltapowerdensities()
        psibins, iterP0, iterP1, jetP0, jetP1 = getpowerdensities()

        plot(psibins[2:end],zeros(psibins[2:end]),"--",color="grey")
        plot(psibins[2:end], iterP1[2:end].-iterP0[2:end],"r",label="ITER")
        xlabel(L"$\psi$ (normalized)")
        ylabel(L"$\Delta P$")
        legend()
        xlim(0,1)
        f1 = gcf()

        figure()
        plot(psibins,zeros(psibins),"--",color="grey")
        plot(psibins, jetP1.-jetP0,"g",label="JET")
        legend()
        ylabel(L"$\Delta P$")
        xlabel(L"$\psi$ (normalized)")
        xlim(0,1)
        f1, gcf()
    end
end

function makeplots(plotsfolder,doorbits=false)
    if !haskey(machines,:JET) || !haskey(machines,:ITER)
        throw(ErrorException("Load both jet and iter before running this method"))
    end
    if doorbits
        close("all")
        open(joinpath(plotsfolder, "orbits.pdf"),"w") do f
            writemime(f, "application/pdf", orbits(machines[:JET]))
        end
        println("Wrote orbits.pdf")
    end
    close("all")
    open(joinpath(plotsfolder, "promptlosses.pdf"), "w") do f
        writemime(f, "application/pdf", promptlossfraction())
    end
    println("Wrote promptlosses.pdf")
    close("all")
    open(joinpath(plotsfolder, "promptlostpower.pdf"), "w") do f
        writemime(f, "application/pdf", promptlostpower())
    end
    println("Wrote promptlostpower.pdf")
    close("all")
    Piter,Pjet = powerdensities()
    open(joinpath(plotsfolder, "powerdensityiter.pdf"), "w") do f
        writemime(f, "application/pdf", Piter)
    end
    println("Wrote powerdensityiter.pdf")
    open(joinpath(plotsfolder, "powerdensityjet.pdf"), "w") do f
        writemime(f, "application/pdf", Pjet)
    end
    println("Wrote powerdensityjet.pdf")
    close("all")
    dPiter,dPjet = deltapowerdensities()
    open(joinpath(plotsfolder, "deltapowerdensityiter.pdf"), "w") do f
        writemime(f, "application/pdf", dPiter)
    end
    println("Wrote deltapowerdensityiter.pdf")
    open(joinpath(plotsfolder, "deltapowerdensityjet.pdf"), "w") do f
        writemime(f, "application/pdf", dPjet)
    end
    println("Wrote deltapowerdensityjet.pdf")
    close("all")
end

function lossfraction(machine::DataSet)
    sum(machine.sloss[:Weight]) / (sum(machine.sloss[:Weight]) + sum(machine.sloops[:Weight]))
end

function avgorbwidth(machine::DataSet)
    ps = machine.sloops
    sqrt(sum((ps[:PsiSquaredBar].-ps[:PsiBar].^2).*ps[:Weight]) / sum(ps[:Weight]))
end

# function tracelayer(idx::Int,c)
#     trace = gettrace(joinpath(root,"output"), idx)
#     R,Z = trace[:R], trace[:Z]
#     layer(x=R, y=Z, Geom.line(preserve_order=true), Theme(line_width=0.5mm,default_color=c))
# end
# function addtracetoplot(p::Plot, idx::Int, c)
#     push!(p.layers, tracelayer(idx,c))
#     p
# end
# addtracetoplot(p::Plot, idx::Int) = addtracetoplot(p,idx,color("blue"))

power(w) = 3.5*1.6022e-19/1e-9 * w

powerdensity(h,vs) = power(h)./vs

end