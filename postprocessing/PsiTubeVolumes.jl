module PsiTubeVolumes

using Contour
using Volume

function psivols(fname::String)
    f = matopen(fname)
    S = read(f, "S")
    close(f)

    Ns = int(vec(S["N"]))
    Rs = vec(map(float, S["x"]))
    Zs = vec(map(float, S["y"]))
    psis = float(vec(S["f"]))

    loopend = length(psis)
    i = 0

    while i < loopend
        i += 1
        psi = psis[i]
        idxs = findin(psis, psi)
        for j = 2:length(idxs)
            if Ns[idxs[j]] > Ns[idxs[1]]
                deleteat!(psis, idxs[1])
                deleteat!(Ns, idxs[1])
                deleteat!(Rs, idxs[1])
                deleteat!(Zs, idxs[1])
                i -= 1
                loopend -= 1
            else
                deleteat!(psis, idxs[j])
                deleteat!(Ns, idxs[j])
                deleteat!(Rs, idxs[j])
                deleteat!(Zs, idxs[j])
                loopend -= 1
            end
        end
    end

    @assert length(psis) == length(Ns) == length(Rs) == length(Zs)

    volumes = Array(Float64,length(psis)+1)
    volumes[1] = 0

    for i = 2:length(volumes)
        volumes[i] = Volume.volume(Rs[i-1], Zs[i-1])
    end
    volumes[2] = volumes[4]/3
    volumes[3] = 2*volumes[4]/3
    tubes = (volumes[2:end].-volumes[1:end-1])
end

function psivols(psifield, psibins)
    allcontours = contour(psifield.R, psifield.Z, psifield.F', psibins)

    longestcontours = map(cs -> cs[indmax(map(c -> length(c.x), cs))])
end

end