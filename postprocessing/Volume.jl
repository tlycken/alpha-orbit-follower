module Volume

using AOFinput

function volume(Rs, Zs)
    v = 0.
    N = length(Rs)
    @assert N == length(Zs)

    for i = 1:N
        h = Zs[mod1(i+1,N)] - Zs[i]
        r = Rs[i]
        R = Rs[mod1(i+1,N)]
        v += h*(R^2 + r^2 + R*r)
    end

    abs(v) * pi/3
end

# doc = parsefile("iter.xml")

# c = chamber(doc)
# RWall, ZWall = c[:RWall], c[:ZWall]
# @assert RWall[1] != RWall[end]
# @assert ZWall[1] != ZWall[end]



# v = volume(RWall, ZWall)

# println("Chamber volume: $v")

# s = lastclosed(doc)

# RLastClosed, ZLastClosed = s[:RLastClosed], s[:ZLastClosed]
# v = volume(RLastClosed, ZLastClosed)
# @assert RLastClosed[1] != RLastClosed[end]
# @assert ZLastClosed[1] != ZLastClosed[end]

# println("Plasma volume: $v")

end
