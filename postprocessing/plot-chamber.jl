include("AOFinput.jl")

function getchamber(doc)
    getcyclicRZ(AOFinput.chamber(doc)...)
end

function getlastclosed(doc)
    getcyclicRZ(AOFinput.lastclosed(doc)...)
end

function getcyclicRZ(R, Z)
    push!(R, R[1])
    push!(Z, Z[1])
    R, Z
end

function getmagaxis(doc)
    magaxR, magaxZ = AOFinput.magaxis(doc)
end


function plotchamber{T<:pyplot}(::Type{T}, doc)
    Rwall, Zwall = getchamber(doc)
    Rlc, Zlc = getlastclosed(doc)
    Rx, Zx = getmagaxis(doc)

    f = PyPlot.plot(Rwall, Zwall, label="Wall")[1]
    PyPlot.plot(Rlc, Zlc, label="Last closed flux surface", figure=f)
    PyPlot.plot(Rx, Zx, label="Magentic axis", figure=f)
    PyPlot.axis(:scaled)
    # PyPlot.xticks(4:1.5:8.5)
    PyPlot.xlim([.95,1.05] .* [extrema(Rwall)...])
    PyPlot.ylim(1.05 * [extrema(Zwall)...])
    f
end

function plotchamber{T<:gadfly}(::Type{T}, doc)
    Rwall, Zwall = getchamber(doc)
    Rlc, Zlc = getlastclosed(doc)
    magaxR, magaxZ = getmagaxis(doc)

    @show magaxR, magaxZ

    Gadfly.plot(
    Gadfly.layer(x=Rwall, y=Zwall, Gadfly.Geom.point, 
        Gadfly.Geom.line(preserve_order=true)), 
    Gadfly.layer(x=Rlc, y=Zlc, 
        Gadfly.Geom.line(preserve_order=true), 
        Gadfly.Theme(default_color=color("red"))),
    Gadfly.layer(x=[magaxR magaxR], y=[magaxZ magaxZ],
        Gadfly.Geom.point, 
        Gadfly.Theme(default_color=color("red"))),
    Gadfly.Theme(minor_label_font="Palatino", major_label_font="Palatino"),
    Gadfly.Guide.xlabel("R [m]"),
    Gadfly.Guide.ylabel("Z [m]")
    )
end
