module WallDistance

import AOFinput: chamber, getdoc
import AOFoutput: getsummary, gettrace
import Base: string, length
using Utilities
using PyPlot

immutable Point{T<:Real}
    R::T
    Z::T
end

immutable Line{T<:Real}
    edge1::Point{T}
    edge2::Point{T}
end

string{T<:Real}(p::Point{T}) = "($(p.R),$(p.Z))"
length{T<:Real}(l::Line{T}) = sqrt((l.edge2.R-l.edge1.R)^2 + (l.edge2.Z-l.edge1.Z)^2)

function intersection{T<:Real}(line1::Line{T}, line2::Line{T})
    x1, x2 = line1.edge1, line1.edge2
    x3, x4 = line2.edge1, line1.edge2

    R = ( (x1.R*x2.Z - x1.Z*x2.R) * (x3.R-x4.R) - (x1.R-x2.R) * (x3.R*x4.Z - x3.Z*x4.R) ) /
          ( (x1.R-x2.R)*(x3.Z-x4.Z) - (x1.Z-x2.Z)*(x3.R-x4.R) )
    Z = ( (x1.R*x2.Z - x1.Z*x2.R) * (x3.Z-x4.Z) - (x1.Z-x2.Z) * (x3.R*x4.Z - x3.Z*x4.R) ) /
          ( (x1.R-x2.R)*(x3.Z-x4.Z) - (x1.Z-x2.Z)*(x3.R-x4.R) )
    Point(R,Z)
end

function pointonline{T<:Real}(line::Line{T}, R::Point{T})
    P,Q = line.edge1, line.edge2
    abs((Q.R - P.R) * (R.Z - P.Z) - (Q.Z - P.Z) * (R.R - P.R)) < cbrt(eps())
end

function intersectsthis{T<:Real}(isect::Point{T}, line::Line{T})
    prev, next = line.edge1, line.edge2
    ( (prev.R < isect.R < next.R || prev.R > isect.R > next.R)
        &&
      (prev.Z < isect.Z < next.Z || prev.Z > isect.Z > next.Z) )
end

function wallhist(root, Nbins, qf)
    s = getsummary(root)
    wallhits = s[s[:Reason] .== "wall",:]


    wallvxs = map((r,z) -> Point(r,z), chamber(getdoc(joinpath(root, "iter.xml")))...)
    N = length(wallvxs)
    walledges = [Line(wallvxs[i],wallvxs[mod1(i+1,N)]) for i in 1:N]
    
    hitcoords = map(
                (r,z,rp,zp,ri,zi,w) -> (Point(r,z), Point(rp,zp), Point(ri,zi), w),
                wallhits[:FinalR].data,
                wallhits[:FinalZ].data,
                wallhits[:SecondToFinalR].data,
                wallhits[:SecondToFinalZ].data,
                wallhits[:InitialR].data,
                wallhits[:InitialZ].data,
                qf(wallhits,root),
            )
    walledgelengths = map(length, walledges)
    cumwalllength = cumsum(walledgelengths)

    distribution = Array((Float64,Float64),0)

    for hit in hitcoords
        final, subfinal, initial, w = hit
        l = 0.

        for i = 1:N
            
            isect = intersection(Line(subfinal, final), walledges[i])

            if intersectsthis(isect, walledges[i])
                l = (i > 1 ? cumwalllength[i-1] : 0) + sqrt((isect.R - wallvxs[i].R)^2 + (isect.Z-wallvxs[i].Z)^2)
                break
            end
        end

        push!(distribution, (l, w))
    end

    binedges, barheights = Utilities.histogram(0, cumwalllength[end], distribution, Nbins)
end

end #module
