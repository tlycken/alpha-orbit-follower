lowdiff(x) = diff(x[1:end-1])
highdiff(x) = diff(x[2:end])
timeweight(t) = (lowdiff(t)+highdiff(t)) ./ (2* (t[end-1] - t[2]))

scaletoindex(R, minR, maxR, N) = ifloor(N * (R.-minR) ./ (maxR .- minR)) .+ 1

readtrace(fname) = readtable(fname;header=false, names=[:t, :R, :phi, :Z, :vparallel, :mu, :phase, :Rdot, :phidot, :Zdot, :vparalleldot, :mudot, :phasedot, :weight], allowcomments=true)

function processtrace!(A, fname)
    trace = readtrace(fname)

    totalweight = timeweight(trace[:t]) .* trace[:weight][2:end-1]

    R = trace[:R][2:end-1]
    Z = trace[:Z][2:end-1]

    minR = 3.9
    maxR = 8.1
    minZ = -2.1
    maxZ = 2.1

    N = size(A,1)
    N == size(A,2) || error("A must be quadratic")

    Rindex = scaletoindex(R, minR, maxR, N)
    Zindex = scaletoindex(Z, minZ, maxZ, N)

    for i = 1:length(Rindex)
        A[Zindex[end-i+1],Rindex[i]] += totalweight[i]
    end
    A
end

function processtrace(fname, N)
    A = zeros(N,N)
    processtrace!(A,fname)
    A
end

tracelayer(trace) = Gadfly.layer(x=trace[:R].data, y=trace[:Z].data, Gadfly.Geom.line(preserve_order=true))