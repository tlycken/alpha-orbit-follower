module Plotting

using Gadfly, DataFrames, PyPlot
using AOFinput, Utilities

export plotchamber, plottrace, addtracetoplot!, chamberwithtraces

import Base: push!

abstract PlottingBackend

immutable gadfly <: PlottingBackend end
immutable pyplot <: PlottingBackend end

#include("Postprocess.jl")

include("plot-chamber.jl")
include("plot-trace.jl")

function chamberwithtraces{T<:PlottingBackend}(pt::Type{T}, root)
    chamberwithtraces(pt, root, (1:length(readdir("$root/output/traces"))) .- 1)
end

chamberwithtraces{T<:PlottingBackend}(pt::Type{T}, root, idxs) = chamberwithtraces(pt, root, "iter", idxs)
chamberwithtraces{T<:PlottingBackend}(pt::Type{T}, root, machine, idx::Number) = chamberwithtraces(pt, root, machine, [idx])
function chamberwithtraces{T<:PlottingBackend}(pt::Type{T}, root, machine, idxs)
   p = plotchamber(pt, AOFinput.getdoc(abspath(joinpath(root, "..", "$machine.xml"))))
   PyPlot.axis(:scaled)

    for idx in idxs #filter(fn->ismatch(traces, fn), readdir("output-traces/traces"))
        addtracetoplot!(p, root, machine, idx) #/traces/trace-$idx.txt") #output-traces/traces/$f")
    end
    p
end 

function push!(p::Gadfly.Plot, l::Gadfly.Layer)
    push!(p.layers, l)
end

function plothist{T<:pyplot}(pt::Type{T}, h,lbl)
    if (length(h) > 100)
        plot(Utilities.histbins(0,1,length(h)), h, label=lbl)
    else
        bar(Utilities.histbins(0,1,length(h)), h, width=.5*Utilities.binwidth(0,1,length(h)), label=lbl)
    end
end

function psicontours(::Type{pyplot}, root, machine; figure=nothing)
    doc = AOFinput.getdoc(joinpath(root, "$machine.xml"))
    gs = AOFinput.gradshafranov(doc)
    Rgrid = AOFinput.uniformgridrange(AOFinput.namedgrid(gs, "R"))
    Zgrid = AOFinput.uniformgridrange(AOFinput.namedgrid(gs, "Z"))
    psivals = AOFinput.fieldvalues(AOFinput.namedfield(gs, "psi"), length(Rgrid), length(Zgrid))

    psimin,psimax = AOFinput.psilim(doc)

    p = PyPlot.contour(Rgrid, Zgrid, (psivals'.-psimin)./(psimax-psimin), 0:.1:1.5; figure=figure, linestyles="dashed", linewidths=.5)
    PyPlot.colorbar()
    p
end

function ICspread{T<:PlottingBackend}(pt::Type{T}, doc, ics)
    subplot(121)
    p = plotchamber(pt, doc)
    PyPlot.axis(:scaled)

    plot(ics[:R], ics[:Z], ",")

    subplot(122)
    plot(ics[:Psi], ics[:Weight]*size(ics,1),",")
end

end #module 

    
    