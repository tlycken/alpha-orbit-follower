module InvestigatePsi

using Diagnostics, PyPlot, Grid

fielddata = readfields()

uniqr, uniqz = sort(unique(fielddata[:R])), sort(unique(fielddata[:Z]))
Nr, Nz = length(uniqr), length(uniqz)

rmin,rmax = extrema(uniqr)
zmin,zmax = extrema(uniqz)

dr, dz = uniqr[2]-uniqr[1], uniqz[2]-uniqz[1]

rs = range(rmin, dr, Nr)
zs = range(zmin, dz, Nz)

psis = reshape(fielddata[:psi].data, Nz, Nr)'

PyPlot.figure()
PyPlot.contour([rs], [zs], psis')
PyPlot.colorbar()

psiitp = CoordInterpGrid((rs,zs), psis, BCnan, InterpLinear)


rrange = 4.1:.05:8.0
zrange = -4:.05:4
psivals = Array(Float64, length(rrange), length(zrange))

for (ir,r) in enumerate(rrange)
    for (iz,z) in enumerate(zrange)
        psivals[ir,iz] = psiitp[r,z]
    end
end

display(psivals)

PyPlot.figure()
PyPlot.contour([rrange], [zrange], psivals')
PyPlot.colorbar()

end #module