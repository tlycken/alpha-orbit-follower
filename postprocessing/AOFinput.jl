module AOFinput

using LibExpat
using DataFrames
using Grid

export
    parsefile,
    chamber,
    lastclosed,
    magaxis,
    plotchamber,
    psilim,
    getdoc

docs = {String=>ETree}

function getdoc(path)
    if !haskey(docs,path)
        docs[path] = parsefile(path)
    end
    docs[path]
end

function parsefile(fname)
    xp_parse(readall(fname))
end

function chamber(doc::ETree)
    vertices = doc[xpath="//chamber/vertex"]
    floatattrs(vertices, "R"), floatattrs(vertices, "Z")
end

function divertor(doc::ETree, machine)
    Rwall,Zwall = chamber(doc)
    if machine==:iter
        return (
            (Rwall[1:13],Rwall[14:20],Rwall[21:27],Rwall[28:38]),
            (Zwall[1:13],Zwall[14:20],Zwall[21:27],Zwall[28:38],Zwall[39:75],Zwall[76:122],Zwall[123:137]))
    elseif machine==:jet
        throw(ErrorException("Not implemented yet!"))
    else
        throw(ErrorException("Invalid machine"))
    end
end

function outboard(doc::ETree, machine)
    Rwall,Zwall = chamber(doc)
    if machine==:iter
        return((Rwall[39:75],Rwall[76:122]), (Zwall[39:75],Zwall[76:122]))
    elseif machine==:jet
        throw(ErrorException("Not implemented yet!"))
    else
        throw(ErrorException("Invalid machine"))
    end
end


function inboard(doc::ETree, machine)
    Rwall,Zwall = chamber(doc)
    if machine==:iter
        return Rwall[123:137], Zwall[123:137]
    elseif machine==:jet
        throw(ErrorException("Not implemented yet!"))
    else
        throw(ErrorException("Invalid machine"))
    end
end

function lastclosed(doc::ETree)
    separatrices = doc[xpath"//magneticField/topology/separatrix"]

    separatrices = filter(s -> s.attr["lastClosed"]=="true", separatrices)
    separatrix = first(separatrices)

    vertices = separatrix[xpath"vertex"]

    floatattrs(vertices, "R"), floatattrs(vertices, "Z")
end

function magaxis(doc::ETree)
    opoints = doc[xpath"//magneticField/topology/Opoint"]

    axis = first(filter(op -> op.attr["mainAxis"]=="true", opoints))

    float(axis.attr["R"]), float(axis.attr["Z"])
end

function floatattrs(nodes, name)
    map(v -> float(v.attr[name]), nodes)
end

namedgrid(parent::ETree, name) = first(filter(g->g.attr["name"]==name, parent[xpath"grid"]))

namedfield(parent::ETree, name) = first(filter(f->f.attr["name"]==name, parent[xpath"field"]))


function uniformgrid(grid::ETree)
    grid = first(grid[xpath"uniform"])
    mn = float(grid.attr["minValue"])
    mx = float(grid.attr["maxValue"])
    N = int(grid.attr["gridPoints"])
    linspace(mn, mx, N)
end

function uniformgridrange(grid::ETree)
    grid = first(grid[xpath"uniform"])
    mn = float(grid.attr["minValue"])
    mx = float(grid.attr["maxValue"])
    N = int(grid.attr["gridPoints"])
    range(mn, (mx-mn)/N, N)
end

function gradshafranov(doc::ETree) 
    first(filter(
        r -> r.attr["name"] == "GradShafranov",
        doc[xpath"//magneticField/representation"]
    ))
end

function fieldvalues(field::ETree, N::Integer)
    float(split(strip(find(first(field[xpath"values"]), "#string"))))
end

function fieldvalues(field::ETree, M::Integer, N::Integer)
    vallist = fieldvalues(field, M*N)
    reshape(vallist, M, N)
end

function psi(doc::ETree) 
    gs = gradshafranov(doc)
    Rgrid = uniformgrid(namedgrid(gs, "R"))
    Zgrid = uniformgrid(namedgrid(gs, "Z"))

    psifield = namedfield(gs, "psi")
    reshape(
        float(split(strip(
            find(first(psifield[xpath"values"]), "#string")
        ))), 
        length(Rgrid), 
        length(Zgrid)
    )
end

function psilim(doc::ETree)
    gs = gradshafranov(doc)

    psigrid = uniformgrid(namedgrid(gs, "psi"))
    minimum(psigrid), maximum(psigrid)
end

function deuterium(doc::ETree)
    species = doc[xpath"//species"]
    species[first(Base.find(s -> s.attr["name"]=="Deuterium",species))]
end
function tritium(doc::ETree)
    species = doc[xpath"//species"]
    species[first(Base.find(s -> s.attr["name"]=="Deuterium",species))]
end

function number_density(species::ETree)
    psi = uniformgridrange(namedgrid(species, "psi"))
    n = fieldvalues(namedfield(species, "density"), length(psi))
    psi,n
end

function temperature(species::ETree)
    psi = uniformgridrange(namedgrid(species, "psi"))
    T = fieldvalues(namedfield(species, "temperature"), length(psi))
    psi,T
end


end #module
