module PostProcess

using DataFrames, Grid, ProgressMeter

using AOFoutput, AOFinput, Utilities

export
    Field2D,
    interp2D,
    addPsi!,
    walktraces!


immutable Field2D{T<:Real}
    R::Vector{T}
    Z::Vector{T}
    F::Matrix{T}
end

function Field2D(df::DataFrame, x::Symbol, y::Symbol, f::Symbol)
    NR = length(unique(df[x]))
    NZ = length(unique(df[y]))
    @assert NR * NZ == length(df[f].data)

    Field2D(unique(df[x].data), unique(df[y].data), reshape(df[f].data, NZ, NR)')
end

function interp2D{T<:Real}(F::Field2D{T})
    xmin,xmax = extrema(F.R)
    ymin,ymax = extrema(F.Z)
    Nx, Ny = length(F.R), length(F.Z)

    xs = range(xmin, (xmax-xmin)/(Nx-1), Nx)
    ys = range(ymin, (ymax-ymin)/(Ny-1), Ny)

    CoordInterpGrid((xs,ys), F.F, BCperiodic, InterpLinear)
end  

interp2D(df::DataFrame, x::Symbol, y::Symbol, f::Symbol) = interp2D(Field2D(df, x, y, f))


function addPsi!(s::DataFrame, root::String, machine::String)
    if :InitialPsi in names(s)
        return s
    end
    s[:InitialPsi] = psi(s[:InitialR], s[:InitialZ], root, machine)
    s
end

function psispread(df::DataFrame)
    initial = sum(df[:InitialPsi].^2 .* df[:Weight]) / sum(df[:Weight])
    final = sum(df[:FinalPsiSquared] .* df[:Weight]) / sum(df[:Weight])
    initial, final, final/initial
end

function psi(R,Z,root,machine)
   psiitp = getpsiitp(root)
   psiaxis, psiboundary = psilim(getdoc(joinpath(root, "..", "$machine.xml")))
   map((r,z) -> (psiitp[r,z]-psiaxis) / (psiboundary-psiaxis), R, Z)
end
midpoints(X) = X[1:end-1] .+ diff(X)./2

function addWarmUp!(df::DataFrame,root,machine)
    for i = 2:size(df,1)
        if sign(df[i,:VParallelDot]*df[i-1,:VParallelDot]) < 0 &&
            PostProcess.insidelcfs(root,machine,df[i,:R],df[i,:Z])
            firstidx = i
            break
        end
    end
    df[:WarmUp] = [[true for i=1:firstidx-1], [false for i=firstidx:size(df,1)]]
    df
end

warmup(trace) = trace[find(trace[:WarmUp]),:]
postwarmup(trace) = trace[find(!trace[:WarmUp]),:]

function walktrace(root,machine,idx)
    trace = gettrace(root,machine,idx)

    addWarmUp!(trace,root,machine)

    tpw = postwarmup(trace)

    r0,z0 = tpw[1,:R], tpw[1,:Z]

    dt = diff(tpw[:Time])
    p = psi(midpoints(tpw[:R]), midpoints(tpw[:Z]), root, machine)
    T = tpw[end,:Time]-tpw[1,:Time]
    vpmin,vpmax = extrema(tpw[:VParallel])
    class = sign(vpmin*vpmax) > 0 ? :passing : :trapped
    dt,T,p,class,r0,z0
end

function walktraces!(s::DataFrame, h::Vector, psimin, psimax, root::String, machine::String)
    Nbins = length(h)
    Nparticles = size(s,1)

    tracepsis = Array(Float64,Nparticles)
    tracepsisquareds = Array(Float64,Nparticles)
    classifications = PooledDataArray(Symbol,Nparticles)
    postwarmupR = Array(Float64,Nparticles)
    postwarmupZ = Array(Float64,Nparticles)

    p = Progress(Nparticles, 2, "Walking traces in $root...", 50)
    for i in 1:Nparticles
        next!(p)
        idx = s[i,:Index]
        w = s[i,:Weight]
        dts,T,ps,class,r0,z0 = walktrace(root,machine,idx)
        postwarmupR[i] = r0
        postwarmupZ[i] = z0
        # all(ps.>psimin) || throw(ErrorException("Invalid psi values for index $idx")))
        isapprox(sum(dts), T) || throw(ErrorException("$(sum(dts)) == sum(dts) != T == $T"))

        classifications[i] = class
        tracepsis[i] = sum(ps.*dts ./ T)
        tracepsisquareds[i] = sum(ps.^2 .* dts./T)
        dtwis = dts .* w ./ T
        Utilities.histogram!(h,psimin,psimax,zip(ps.data, dtwis))
        isapprox(sum(dtwis), w) || throw(ErrorException("$(sum(dtwis)) == sum(dtwis) != w == $w"))
        @assert all(h .!= Inf)
    end

    s[:PsiBar] = tracepsis
    s[:PsiSquaredBar] = tracepsisquareds
    s[:Classification] = classifications
    s[:PostWarmupR] = postwarmupR
    s[:PostWarmupZ] = postwarmupZ
        
    Utilities.histbins(0.,2.,Nbins), h
end

function windingnumber(Rs,Zs,r,z)
    wn = 0
    N = length(Rs)
    @assert length(Zs) == N
    @inbounds for i=1:N
        if Zs[mod1(i+1,N)] > z && Zs[i] < z &&
            r < Rs[i] + (z - Zs[i]) * (Rs[mod1(i+1,N)] - Rs[i]) / (Zs[mod1(i+1,N)] - Zs[i])
            wn += 1
        elseif Zs[mod1(i+1,N)] < z && Zs[i] > z &&
            r < Rs[i] + (z - Zs[i]) * (Rs[mod1(i+1,N)] - Rs[i]) / (Zs[mod1(i+1,N)] - Zs[i])
            wn -= 1
        end
    end
    wn
end

inside(Rs,Zs,r::Number,z::Number) = windingnumber(Rs,Zs,r,z) != 0
inside(Rs,Zs,R,Z) = map((r,z)->inside(Rs,Zs,r,z),R,Z)

function insidechamber(root, machine, R, Z)
    Rs, Zs = AOFinput.chamber(AOFinput.getdoc(joinpath(root, "..", "$machine.xml")))
    inside(Rs,Zs,R,Z)
end

function insidelcfs(root,machine,R,Z)
    Rs,Zs = AOFinput.lastclosed(AOFinput.getdoc(joinpath(root,"..", "$machine.xml")))
    inside(Rs,Zs,R,Z)
end

# function plotpsispread(df::DataFrame)
#     wallhits = psispread(df[df[:Reason].==:wall,:], :Wallhits)
#     loops = psispread(df[df[:Reason].==:loop,:], :Loops)

#     spread = append!(psispread(df, :All), append!(wallhits, loops))

#     spread = spread[spread[:Xlabel].!="Wallhits (final)",:]
#     spread = spread[spread[:Xlabel].!="All (final)",:]

#     draw(PNG("psispread.png", 6inch, 3inch), plot(spread, x=:Xlabel, y=:Psi, ymin=:Mins, ymax=:Maxs, Geom.point, Geom.errorbar, Scale.x_discrete(levels=["All (initial)", "Wallhits (initial)", "Loops (initial)", "Loops (final)"]), Guide.xlabel(""), Scale.y_continuous(minvalue=0,maxvalue=1)))
# end

end #module