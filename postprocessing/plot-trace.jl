using Gadfly, DataFrames, PyPlot, PyCall
using AOFoutput, PostProcess

function gettracecoords(root, machine, idx::Integer; removewarmup = true)
    trace = AOFoutput.gettrace(root,machine,idx)
    if removewarmup
        PostProcess.addWarmUp!(trace,root,machine)
        trace = PostProcess.postwarmup(trace)
    end
    trace[:R].data, trace[:Z].data
end

function plottrace{T<:gadfly}(::Type{T}, root, machine, idx::Integer)
    R, Z = gettracecoords(root, machine, idx)
    layer(x=R, y=Z, Geom.line(preserve_order=true), Theme(line_width=0.1mm))
end

function addtracetoplot!(p::Gadfly.Plot, root, machine, idx::Integer)
    push!(p.layers, plottrace(gadfly, root, idx))
    p
end

function addtracetoplot!(p::PyObject, R, Z)
    plot(R, Z, "c--", figure=p)
    plot(R[1], Z[1], "gx", figure=p)
    plot(R[end], Z[end], "rx", figure=p)
end

function addtracetoplot!(p::PyObject, root, machine, idx::Integer)
    R,Z = gettracecoords(root,machine,idx)
    if length(R) == 0
        summarydata = getsummary(root)
        R,Z = summarydata[summarydata[:Index].==idx,:InitialR], summarydata[summarydata[:Index].==idx,:InitialZ]
    end
    addtracetoplot!(p, R, Z)
end
