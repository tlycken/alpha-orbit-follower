module LossPerPsi

include("Diagnostics.jl")
include("AOFinput.jl")
include("Postprocess.jl")

import Diagnostics: readfields
import Postprocess: readsummary
import AOFinput: psilim, parsefile
using DataFrames

export lossperpsi

function normalizepsis(psis, root)
    psilims = AOFinput.psilim(AOFinput.parsefile("$root/iter.xml"))
    if psilims[1] != 0
        psis .-= psilims[1]
        psilims .-= psilims[1]
    end

    psis./psilims[2]
end

function lossperpsi(root)
    psiitp = Diagnostics.interp2D(Diagnostics.readfields(root), :R, :Z, :psi)
    summarydata = readsummary("$root/summary.txt")
    wallhits = summarydata[summarydata[:Reason] .== "wall", :]

    initialPsis = map((r,z) ->psiitp[r,z], wallhits[:InitialR], wallhits[:InitialZ])
    
    idxs = sortperm(initialPsis)
    losses = cumsum(wallhits[idxs,:Weight])

    DataFrame(psi = initialPsis[idxs], weightloss = losses)
end

end #module
