function windingnumber(vertices, point)
    wn = 0
    N = length(vertices)
    @inbounds for i=1:N
        if (
            vertices[mod1(i+1,N)][2] > point[2] && vertices[i][2] < point[2]
            && vertices[i][1] > point[1])
            wn += 1
        elseif (
            vertices[mod1(i+1,N)][2] < point[2] && vertices[i][2] > point[2]
            && vertices[mod(i+1,N)][1] > point[1])
            wn -= 1
        end
    end
    wn
end

inside(vertices, point) = windingnumber(vertices,point) != 0

function area(boundingbox, h, vertices)
    N = 0
    hits = 0
    xs = [boundingbox[1]:h:boundingbox[2]]
    ys = [boundingbox[3]:h:boundingbox[4]]
    for x = xs, y = ys
        N += 1
        inside(vertices, [x,y]) && (hits += 1)
    end

    (boundingbox[2]-boundingbox[1])*(boundingbox[4]-boundingbox[3])*hits/N
end

function area(vertices) 
    N = length(vertices)
    A = 0.0
    for i = 1:N
        @inbounds A += vertices[i][1]*vertices[mod1(i+1,N)][2] - vertices[i][2]*vertices[mod1(i+1,N)][1]
    end
    A/2
end

function volume(v)
    N = length(v)
    V = 0.0
    for i = 1:N
        @inbounds V += (v[i][1]+v[mod1(i+1,N)][1])^2 * (v[mod1(i+1,N)][2]-v[i][2])
    end
    V *= pi/4.
end

function circle(R0,r,N)
    vertices = Vector{typeof(R0)}[]

    for i = 0:(N-1)
        t = i * 2pi / N
        push!(vertices, [R0 + r*cos(t), r*sin(t)])
    end

    return vertices
end

R, r = 2., 1.5
C = circle(R,r,9)

CBB = (R-r,R+r,-r,r)

area(CBB, 0.01, C), 1.5^2 * 10 * sin(2pi/10) / 10
volume(C), pi*r^2 * 2pi*R