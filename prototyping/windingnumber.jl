
function plotvertices(vertices, c)
    p = plot(x = vertices[:,1], y = vertices[:,2], Geom.point, Theme(default_color=color(c)))

    draw(PNG("circle.png", 4inch, 3inch), p)
    println("circle.png done.")
end

function initpolygon(R0,r)
    vertices = Vector{typeof(R0)}[]

    for i = 0:100
        push!(vertices, [R0 + r*cos(2pi/100 * i), r*sin(2pi/100 * i)])
    end

    return vertices
end

function windingnumber(vertices, point)
    wn = 0
    N = length(vertices)
    for i=1:N
        if (
            vertices[mod1(i+1,N)][2] > point[2] && vertices[i][2] < point[2]
            && vertices[i][1] > point[1])
            wn += 1
        elseif (
            vertices[mod1(i+1,N)][2] < point[2] && vertices[i][2] > point[2]
            && vertices[mod(i+1,N)][1] > point[1])
            wn -= 1
        end
    end
    wn
end

inside(vertices, point) = windingnumber(vertices,point) != 0

function checkpoint!(hits, misses, vertices, point)
    if inside(vertices,point)
        push!(hits,point)
    else
        push!(misses, point)
    end
end

function checkgrid!(h,m,v,R0,r) 
    for R = R0-1.5r:0.1:R0+1.5r, Z = -1.5r:0.1:1.5r
        checkpoint!(h,m,v,[R,Z])
    end
end    
    
function doit()
    R0, r = 5.0, 1.5

    v = initpolygon(R0,r)
    h = Vector{Float64}[]
    m = Vector{Float64}[]

    checkgrid!(h,m,v,R0,r)

    empty!(h)
    empty!(m)

    @time checkgrid!(h,m,v,R0,r)


    println("init phase done. entering loop...")

    println("loop done")

    hits = getlayer(h,"green")
    misses = getlayer(m, "red")
    vxs = getlayer(v,"yellow")

    p = plot(hits, misses)

    run(`rm -f hitormiss.png`)
    draw(PNG("hitormiss.png", 4inch, 3inch), p)
    println("hitormiss.png done")
end

function getlayer(v,c)
    xs, ys = Float64[p[1] for p in v], Float64[p[2] for p in v]
    layer(
        x = xs,
        y = ys,
        Geom.point,
        Theme(default_color=color(c))
    )
end

