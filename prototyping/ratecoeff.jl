BG=34.3827
mc2=1124656
C1=1.17302e-9
C2=1.151361e-2
C3=7.51886e-2
C4=4.60643e-3
C5=1.35000e-2
C6=-1.06750e-4
C7=1.36600e-5

function k(T)
    if all(0.2 .<= T .<= 100.)
        top = T .* (C2 .+ T .* (C4 .+ T .* C6))
        bottom = 1 .+ T .* (C3 .+ T .* (C5 .+ T .* C7))
        theta = T ./ (1 .- top ./ bottom)
        ksi = cbrt(BG.*BG ./ 4theta)
        r = C1 .* theta .* sqrt(ksi./(mc2.*T.^3)) .* exp(-3ksi)
        return r
    end
    return 0
end

using Gadfly

Ts = 0.2:3.8:100

k(Ts)
