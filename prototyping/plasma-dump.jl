#using DataFrames, Winston

plasma = readtable("plasma-dump.txt")

DT = reshape(plasma[:TritiumTemperature], length(unique(plasma[:Z])), length(unique(plasma[:R]))).data;

imagesc(DT)