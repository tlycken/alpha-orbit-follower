function N=HardcodedNumericalParams(Np)
%
% function N=HardcodedNumericalParams()
%
% Set of hard-coded numerical parameters for AOF
%

% Particle cloud initial condition
N.IC = struct();
if (nargin < 1)
    Np = 100;
end
N.IC.Nparticles = Np;   % Particle count
N.IC.E0 = 3.5e6;        % Electronvolt
N.IC.Type = 'alpha';    % Simulate alpha particles
N.IC.InitTime = 1e-9;   % Time during which particle cloud is initialized


% Final condition
N.FC = struct();
N.FC.Time = 1e-3;       % Seconds
N.FC.Laps = 1;          % laps

N.Step = 0.025;          % Maximum step lenght in the poloidal plane for the initial step.

end
