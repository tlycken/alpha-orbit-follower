function verifyAOFpsiMapping(AOF, PLASMA)
close all
Rs=linspace(AOF.grid2d.Rlim(1), AOF.grid2d.Rlim(2), AOF.grid2d.NR);
Zs=linspace(AOF.grid2d.Zlim(1), AOF.grid2d.Zlim(2), AOF.grid2d.NZ);
psi = AOF.grid2d.psi;
imagesc(Rs, Zs, psi); set(gca, 'YDir', 'normal')
colorbar
title('psi(R,Z)')

figure
psig = linspace(AOF.grid1d.psi_min, AOF.grid1d.psi_max, AOF.grid1d.Npsi);
Fvals = AOF.grid1d.F;
imagesc(Rs, Zs, interp1(psig, Fvals, psi, 'spline', 'extrap')); set(gca, 'YDir', 'normal')
colorbar
title('F(psi(R,Z))')

figure
plot(psig, Fvals)
title('F(psi)')

figure


end