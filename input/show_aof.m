Rg = linspace(AOF.grid2d.Rlim(1), AOF.grid2d.Rlim(2), AOF.grid2d.NR);
Zg = linspace(AOF.grid2d.Zlim(1), AOF.grid2d.Zlim(2), AOF.grid2d.NZ);
psig = linspace(AOF.grid1d.psi_min, AOF.grid1d.psi_max, AOF.grid1d.Npsi);

psi = AOF.grid2d.psi;
F = AOF.grid1d.F;

close all

subplot(1,2,1)
plot(psig, F)
ylim([0 10*ceil(max(F(:)/10))])
xlim([min(psi(:)) max(psi(:))])

subplot(1,2,2)
contour(Rg, Zg, psi, 25)
colorbar

