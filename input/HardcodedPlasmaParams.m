function P=HardcodedPlasmaParams(Npsi,AOF)
%
% function P=HardcodedPlasmaParams()
%
% Set of hardcoded plasma parameters...for AOF
%

%-----------------------------
% START INPUT
%-----------------------------

% Electron density
P.ne0=0.9e20;
P.needge=0.4e20;

% Hydrogen concentration
P.X_H=0.01;

% D/T concentrations
P.X_D=(1-P.X_H)/2;
P.X_T=1-P.X_H-P.X_D;

% Temperature on axis and at the edge
P.T0=25e3;
P.Tedge=3e3;

P.species.amn = [5.4858e-4 , 1 , 2 , 3];
P.species.zn  = [       -1 , 1 , 1 , 1];
P.species.zion= [       -1 , 1 , 1 , 1];
P.species.name= {'Electrons','Hydrogen','Deuterium','Tritium'};
P.species.symbol={'e','H','D','T'};
P.species.concentration=[1.0, P.X_H, P.X_D, P.X_T];

P.Reaction = struct();
P.Reaction.Reactant1 = 'Deuterium';
P.Reaction.Reactant2 = 'Tritium';
P.Reaction.Product = 'alpha';
P.Reaction.BG = 34.3827;
P.Reaction.mc2 = 1124656;
P.Reaction.C1 = 1.17302e-9;
P.Reaction.C2 = 1.51361e-2;
P.Reaction.C3 = 7.51886e-2;
P.Reaction.C4 = 4.60643e-3;
P.Reaction.C5 = 1.35000e-2;
P.Reaction.C6 = -1.06750e-4;
P.Reaction.C7 = 1.36600e-5;
P.Reaction.Tmin = 0.2e3;
P.Reaction.Tmax = 100e3;


%-----------------------------
% END OF INPUT
%-----------------------------

psin=linspace(1,0,Npsi);
P.Temperature = P.Tedge + (P.T0-P.Tedge) * psin;
P.DensityElectrons = P.needge + (P.ne0-P.needge) * psin;

