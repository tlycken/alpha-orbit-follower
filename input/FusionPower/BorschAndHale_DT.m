function sigma_v = BorschAndHale_DT(T_eV)

% M=  [
% 0.2  1.254e-26
% 1.0  6.857e-21
% 3.0  1.867e-18
% 5.0  1.366e-17
% 8.0  6.222e-17
% 10.0 1.136e-16
% 12.0 1.747e-16
% 15.0 2.740e-16
% 20.0 4.330e-16
% 30.0 6.681e-16
% ];

% sigma_v = 1e-6 * exp( interp1( M(:,1)*1e3 , log(M(:,2)) , T_eV ,'spline','extrap') );

BG2 = 34.3827^2;
mc2 = 1124656;
C1 = 1.17302e-9;
C2 = 1.51361e-2;
C3 = 7.51886e-2;
C4 = 4.60643e-3;
C5 = 1.35000e-2;
C6 = -1.06750e-4;
C7 = 1.36600e-5;
Tmin = 0.2e3;
Tmax = 100e3;

% Convert eV to keV
Ti = T_eV * 1e-3;

theta = Ti ./ (1 - Ti .* (C2 + Ti .* (C4 + Ti .* C6)) ./ (1 + Ti .* (C3 + Ti .* (C5 + Ti .* C7))));
ksi = (BG2 ./ (4 * theta)).^(1/3);
sigma_v_cm3 = C1 * theta .* sqrt(ksi ./ (mc2 .* Ti.^3)) .* exp(-3*ksi);

% Convert cm^3 to m^3
sigma_v = sigma_v_cm3 * 1e-6;

end