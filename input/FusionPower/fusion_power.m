
P.T0=25e3;
P.Tedge=3e3;
nD=1e20/2;
nT=1e20/2;
EnergyAlpha=3.5e6 * 1.6022e-19;
EnergyNeutron=14.1e6 * 1.6022e-19;

N1d=200;

%--------------------------------------------------
% Reading GEQDSKfor plasma shape
%--------------------------------------------------

addpath('../GEQDSK/matlab/');
fn_GEQDSK='../GEQDSK/examples/iter_Scen2_2007_equil_out_snap_20_65x136.eqdsk';
[S,ier]=read_geqdsk(fn_GEQDSK);
[Rmat,Zmat]=meshgrid(S.rgrid,S.zgrid);

%%% VERY ODD...WRONG SING FOR PSIRZ!!
psirz = -S.psirz;

%--------------------------------------------------
% Temperature profile
%--------------------------------------------------

psi=linspace( S.ssimag , S.ssibry , N1d);
psin=(psi-psi(1))/(psi(end)-psi(1));
TeV = P.T0 + (P.Tedge-P.T0) * psin;

%--------------------------------------------------
% Identify the 2D grid cells that are inside 
% the plasma, II
%--------------------------------------------------

sign_dpsi = sign(S.ssibry - S.ssimag);
% psi_axis < psi < psi_boundary
cond1=(psirz*sign_dpsi < S.ssibry*sign_dpsi);   
% R and Z inside bounding box of chamber
cond2=(Rmat < max(S.rbbbs));                    
cond3=(Rmat > min(S.rbbbs));
cond4=(Zmat < max(S.zbbbs));
cond5=(Zmat > min(S.zbbbs));
% all conditions must apply
II=find(cond1 & cond2 & cond3 & cond4 & cond5);

%--------------------------------------------------
% Interpolate the temperature + calculate sigma_v
% on the 2D grid
%--------------------------------------------------

TeV_II=interp1( psi , TeV , psirz(II)*sign_dpsi,'linear','extrap' );
sigma_v=zeros(size(psirz));
sigma_v(II) = BorschAndHale_DT(TeV_II);

%--------------------------------------------------
% Volume integrated particle source rates and power 
%--------------------------------------------------

dR=diff(Rmat(1,1:2));
dZ=diff(Zmat(1:2,1));
dV=2*pi*Rmat*dR*dZ;
Nfusion=sum(sum(dV .* sigma_v .* nD .* nT));
Pneutron = Nfusion * EnergyNeutron;
Palpha   = Nfusion * EnergyAlpha;
Pfusion  = Pneutron + Palpha;

%--------------------------------------------------
% Display results
%--------------------------------------------------

disp(['Neutron power = ',num2str(Pneutron*1e-9, '%4.2f GW')])
disp(['Alpha   power = ',num2str(Palpha*1e-9, '%4.2f GW')])
disp(['Fusion  power = ',num2str(Pfusion*1e-9, '%4.2f GW')])

clf
hold on
plot(S.rbbbs,S.zbbbs,'-','linewidth',2,'color',0.7*[1,1,1])
plot(S.xlim,S.ylim,'k.','markersize',4)
contour(Rmat(1,:),Zmat(:,1),sigma_v.*nD.*nT);
colorbar
axis equal
text(6.6,-3.2,sprintf('P_a=%4.2fGW',Palpha*1e-9))
text(6.6,-3.7,sprintf('P_n=%4.2fGW',Pneutron*1e-9))
text(6.6,-4.2,sprintf('P_{tot}=%4.2fGW',Pfusion*1e-9))
title('Particle source density rate in (R,Z)')

%plot(psirz(II),TeV_II,'.')
%plot(sigma_v);
