function []=plotAOF(EQ)

R2d=linspace(EQ.grid2d.Rlim(1),EQ.grid2d.Rlim(2),EQ.grid2d.NR);
Z2d=linspace(EQ.grid2d.Zlim(1),EQ.grid2d.Zlim(2),EQ.grid2d.NZ);

psi_mid=interp2(R2d,Z2d,EQ.grid2d.psi, linspace(EQ.grid2d.Rlim(1),EQ.grid2d.Rlim(2),3) , [0,0,0]);
if ( psi_mid(2) > mean(psi_mid) )
  psi_lcfs=EQ.grid1d.psi_max;
else
  psi_lcfs=EQ.grid1d.psi_min;
end
psi_lcfs=EQ.grid1d.psi_max;

figure(1)
clf
contour(R2d,Z2d,EQ.grid2d.psi,100);
hold on
plot( EQ.Rwall , EQ.Zwall , 'k','linewidth',2);
contour(R2d,Z2d,EQ.grid2d.psi,[1,1]*psi_lcfs, 'r-','linewidth',2);
plot( EQ.Rlcfs , EQ.Zlcfs , 'g','linewidth',2);

plot( EQ.MagAxis.R*[1,1] , EQ.MagAxis.Z*[1,1] , 'kx','linewidth',2,'markersize',8);


axis equal