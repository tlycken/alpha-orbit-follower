function [EQout,PLASMAout]=transform_from_ITER_to_JET(EQ,PLASMA,kargs)

%------------------------------------------------------------
% Input parsing
%------------------------------------------------------------
if nargin<3
  kargs=struct();
end
if isfield(kargs,'R_plasmaRangeJET')
  R_plasmaRangeJET=kargs.R_plasmaRangeJET;
else
  R_plasmaRangeJET=[2.16 , 3.84];
end

if isfield(kargs,'Z_plasmaRangeJET')
  Z_plasmaRangeJET=kargs.Z_plasmaRangeJET;
else
  Z_plasmaRangeJET=[-1.45 , 1.95];
end

if isfield(kargs,'psi_plasmaWidthJET')
  psi_plasmaWidthJET = kargs.psi_plasmaWidthJET;
else
  % Reference: JET shot 85307 at 51s
  Iplasma_Ref=2.0;
  psi_plasmaWidthJET_Ref= 0.1 * (2.0 * pi);
  % Request:
  Iplasma=4.0;
  psi_plasmaWidthJET = psi_plasmaWidthJET_Ref * Iplasma / Iplasma_Ref;
end

if isfield(kargs,'B0_JET')
  B0_JET=kargs.B0_JET;
else
  B0_JET = 3.7;
end

if isfield(kargs,'density_JET')
  density_JET=kargs.ne_JET;
else
  density_JET = 6e19;
end

if isfield(kargs,'temperature_JET')
  temperature_JET=kargs.temperature_JET;
else
  temperature_JET = 6e19;
end

%------------------------------------------------------------
% Initialise output
%------------------------------------------------------------
EQout=EQ;
PLASMAout=PLASMA;

%------------------------------------------------------------
% Modify wall
%------------------------------------------------------------
DAT=load('JET_wall.dat');
EQout.Rwall=DAT(:,1);
EQout.Zwall=DAT(:,2);

%------------------------------------------------------------
% Grids from input
%------------------------------------------------------------
R2d=linspace(EQ.grid2d.Rlim(1),EQ.grid2d.Rlim(2),EQ.grid2d.NR);
Z2d=linspace(EQ.grid2d.Zlim(1),EQ.grid2d.Zlim(2),EQ.grid2d.NZ);
% psi_mid=interp2(R2d,Z2d,EQ.grid2d.psi, [4.3,6,8] , [0,0,0]);
% if ( psi_mid(2) > mean(psi_mid) )
%   psi_lcfs=EQ.grid1d.psi_max;
% else
%   psi_lcfs=EQ.grid1d.psi_min;
% end
psi_lcfs=EQ.grid1d.psi_max;
CNTR=contourStruct(R2d,Z2d,EQ.grid2d.psi,psi_lcfs*[1,1]);
EQout.C=CNTR;

indContour=0;
lenContour=0;
for j=1:length(CNTR)
  if CNTR(j).N > lenContour
    indContour = j;
    lenContour = CNTR(j).N;
  end
end
Rlcfs=CNTR(indContour).x;
Zlcfs=CNTR(indContour).y;

%------------------------------------------------------------
% Transform 2d grid to fit inside JET wall
%------------------------------------------------------------
R_plasmaRangeITER=[min(Rlcfs) , max(Rlcfs)];
Z_plasmaRangeITER=[min(Zlcfs) , max(Zlcfs)];
EQout.grid2d.Rlim = linearRemap( EQ.grid2d.Rlim, R_plasmaRangeITER, R_plasmaRangeJET);
EQout.grid2d.Zlim = linearRemap( EQ.grid2d.Zlim, Z_plasmaRangeITER, Z_plasmaRangeJET);

R2dJET = linearRemap( R2d, R_plasmaRangeITER, R_plasmaRangeJET);
Z2dJET = linearRemap( Z2d, Z_plasmaRangeITER, Z_plasmaRangeJET);

EQout.MagAxis.R = linearRemap( EQout.MagAxis.R, R_plasmaRangeITER, R_plasmaRangeJET);
EQout.MagAxis.Z = linearRemap( EQout.MagAxis.Z, Z_plasmaRangeITER, Z_plasmaRangeJET);

EQout.Rlcfs = linearRemap( Rlcfs, R_plasmaRangeITER, R_plasmaRangeJET);
EQout.Zlcfs = linearRemap( Zlcfs, Z_plasmaRangeITER, Z_plasmaRangeJET);

%------------------------------------------------------------
% Extend plasma range in R to cover area inside wall
%------------------------------------------------------------
R2dJET_tmp=[3*R2dJET(1) - R2dJET(2) - R2dJET(end:-1:1) , R2dJET];
R2dJET_new=R2dJET_tmp( R2dJET_tmp > min(EQout.Rwall) - 0.1);
[Rm,Zm]=meshgrid([R2dJET_new], Z2dJET);
RJETm  =[ 1.5                , R2dJET];
psiJETm=[ 60+4*(2*EQ.grid2d.psi(:,1)-EQ.grid2d.psi(:,2)) , EQ.grid2d.psi];
EQout.grid2d.psi=interp2(RJETm, Z2dJET, psiJETm, Rm,Zm,'spline');
EQout.grid2d.Rlim=[min(R2dJET_new) , max(R2dJET_new)];
EQout.grid2d.NR=length(R2dJET_new);

%------------------------------------------------------------
% Normalize to a realistic plasma current in JET
%------------------------------------------------------------
psi_plasmaWidthITER  = EQout.grid1d.psi_max - EQout.grid1d.psi_min;
EQout.grid2d.psi     = EQout.grid2d.psi     * psi_plasmaWidthJET / psi_plasmaWidthITER;
EQout.grid1d.psi_min = EQout.grid1d.psi_min * psi_plasmaWidthJET / psi_plasmaWidthITER;
EQout.grid1d.psi_max = EQout.grid1d.psi_max * psi_plasmaWidthJET / psi_plasmaWidthITER;

%------------------------------------------------------------
% Normalize F for JET
%------------------------------------------------------------
EQout.grid1d.F = EQ.grid1d.F * B0_JET * EQout.MagAxis.R / min(EQ.grid1d.F);

%------------------------------------------------------------
% Normalize the density and temperature for JET 
%------------------------------------------------------------
PLASMAout.Temperature = PLASMA.Temperature * temperature_JET / max(PLASMAout.Temperature);
PLASMAout.DensityElectrons = PLASMAout.DensityElectrons * density_JET / max(PLASMAout.DensityElectrons);

function xOut=linearRemap(x,xLimIn,xLimOut)
  
  xOut = ( x - xLimIn(1) ) * diff(xLimOut) / diff(xLimIn) + xLimOut(1);
  return


