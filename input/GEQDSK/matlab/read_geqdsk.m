function [S,ier]=read_geqdsk(fneqdsk,HeaderType)

%
% [S,ier]=read_geqdsk(fneqdsk,HeaderType)
%
% (Rewrite of fortran routine /home/flush/flush/fortran/readEquilibrium/read_geqdsk.f)
%
% Reads in the contents of an GEQDSK file
% Based on the EFIT read routine
% Different formats of the first line can be read by 
% choosing "HeaderType \in {1,2}".
%
% Input:
%fneqdsk: unit number of GEQDSK file
% keqdsk: =1 for data in ASCII format, otherwise binary format
%
% Output (only some of the information):
%     mw: maximum number of grid points in R direction (width) =
%         maximum number of flux surfaces
%     mh: maximum number of grid points in Z direction (height)
%     nw: number of grid points in R direction (width) = number of
%         flux surfaces
%     nh: number of grid points in Z direction (height)
%  rgrid: radial coordinates of grid [metres]
%  zgrid: vertical coordinates of grid [metres]
%  psirz: psi on (R,Z) grid
%     r0: radius of vacumm toroidal magnetic field (bcentr) [metres]
% bcentr: vacuum toroidal magnetic field [Tesla]
% cpasma: plasma current [Amperes]
%  psiax: psi on axis
% ssibry: psi on boundary
%      q: q profile
%      f: F profile
%   dpdp: dP/dPsi
%   dfdp: dF/dPsi
%
% psiPol: 1D profile, poloidal flux function
% psiTor: 1D profile, toroidal flux function
% rhoPol: 1D profile, normalised square root of the poloidal flux function, 0 <= rhoPol <= 1
% rhoTor: 1D profile, normalised square root of the toroidal flux function, 0 <= rhoTor <= 1
%
% More info can be found on: https://fusion.gat.com/theory/Efitgeqdsk and various other websites
%
%  E.g.
%   fneqdsk='/home/tjohn/ITER/MachineDesign/Equilibrium/Scen2/equil_out_snap_22_65x136.eqdsk';HeaderType=1;[S,ier]=read_geqdsk(fneqdsk,HeaderType);
%   fneqdsk='equil_out_snap_22_65x136.eqdsk';HeaderType=1;[S,ier]=read_geqdsk(fneqdsk,HeaderType);
%   fneqdsk='../GEQDSK/Scen4_bn2.57_513x513.txt';HeaderType=2;[S,ier]=read_geqdsk(fneqdsk,HeaderType);
%

S=struct();
ier=0;

if nargin<2
  HeaderType=1;
end

fid=fopen(fneqdsk);
if fid==-1
  ier=1;
  disp('ERROR in read_geqdsk. Failed to open:')
  disp(['   ',fneqdsk])
  disp('ABORT')
  return
end

%-----------------
% OLD: READ HEADER
if HeaderType==1
  str=fgetl(fid);
  idum={};
  for j=1:6
    idum={idum{:},str(1:8)};
    str=str(9:end);
  end
  eval(['m_dummy=',str(1:3),';']);
  eval(['mw=',str(4:6),';']);
  eval(['mh=',str(7:10),';']);
end

%-----------------
% NEW: READ HEADER
if HeaderType==2
  str=deblank(fgetl(fid));
  flag=0;
  while flag==0
    if ( str(end)=='0' |str(end)=='1' |str(end)=='2' |str(end)=='3' |...
         str(end)=='4' |str(end)=='5' |str(end)=='6' |str(end)=='7' |...
         str(end)=='8' |str(end)=='9' )
      flag=1;
    else
      str=str(1:end-1);
      if length(str)<9
        disp('Error! Fail to find array dimensions in first line of EQDSK file.');
        return
      end
    end
  end
  
  %Iblank==find(str==' ');
  %str=deblank(str(1:Iblank(end)));
  
  if length(str)<9
    disp('Error! Fail to find array dimensions in first line of EQDSK file.');
    return
  end
  
  Ns=length(str);
  eval(['m_dummy=',str(max(1,max(1,Ns-11)):Ns-8),';']);
  eval(['mw=',str(Ns-7:Ns-4),';']);
  eval(['mh=',str(Ns-3:Ns),';']);
end
%-----------------

nw=mw;
nh=mh;

S.mw=mw;
S.mh=mh;

S.description_mw='Number of horizontal (R) grid points';
S.description_mh='Number of vertical (Z) grid points';

M=fscanf(fid,'%g',[5]);
S.xdim  = M(1);
S.zdim  = M(2);
S.rzero = M(3);
S.rmid  = M(4);
S.zmid  = M(5);
S.rmid=S.rmid+0.5*S.xdim;

S.rgrid=zeros(1,mw);
S.zgrid=zeros(1,mh);
for i=1:mw
  S.rgrid(i)=S.rmid+(real(i-1)/real(mw-1)-0.5)*S.xdim;
end
for i=1:mh
  S.zgrid(i)=S.zmid+(real(i-1)/real(mh-1)-0.5)*S.zdim;
end
S.description_rgrid='2D grid: radial coordinates of the grid [metres]';
S.description_zgrid='2D grid: vertical coordinates of grid [metres]';

M=fscanf(fid,'%g',[5]);
S.rmaxis = M(1);
S.zmaxis = M(2);
S.ssimag = M(3);
S.ssibry = M(4);
S.bcentr = M(5);
S.description_rmaxis='radius of the magnetic axis [metres]';
S.description_zmaxis='vertical coordinate of the magnetic axis [metres]';
S.description_ssimag='psi on magnetic axis (Weber/rad)';
S.description_ssibry='psi on boundary (Weber/rad)';
S.description_bcentr='vacuum toroidal magnetic field at the magnetic axis [Tesla]';

M=fscanf(fid,'%g',[5]);
S.cpasma = M(1);
S.ssimag = M(2);
  xdum   = M(3);
S.rmaxis = M(4);
  xdum   = M(5);
S.description_cpasma='plasma current [Amperes]';

M=fscanf(fid,'%g',[5]);
S.zmaxis = M(1);
  xdum   = M(2);
S.ssibry = M(3);
  xdum   = M(4);
  xdum   = M(5);
S.fpol=fscanf(fid,'%g',[mw]);
S.pres=fscanf(fid,'%g',[mw]);
S.ffprim=fscanf(fid,'%g',[mw]);
S.pprime=fscanf(fid,'%g',[mw]);
S.description_fpol='1D profile: Poloidal current function, F=R*B_tor (R=major radius, B_tor=toroidal magnetic field component)';
S.description_pres='1D profile: pressure, p';
S.description_ffprim='1D profile: F * dF/dpsi';
S.description_pprime='1D profile: dp/dpsi';

S.psirz=fscanf(fid,'%g',[mw,mh])';
S.description_psirz='2D data: psi on the (R,Z) grid (Weber/rad)';

S.qpsidat=fscanf(fid,'%g',[mw]);
S.psiN=linspace(0,1,mw)';

M=fscanf(fid,'%d',[2]);
S.nbbbs  = M(1);
S.limitr = M(2);
S.description_nbbbs='Number of boundary points';
S.description_limitr='Number of limiter points';

M=fscanf(fid,'%g',[2,S.nbbbs])';
S.rbbbs=M(:,1);
S.zbbbs=M(:,2);
S.description_rbbbs='R at boundary points (meter)';
S.description_zbbbs='Z at boundary points (meter)';

M=fscanf(fid,'%g',[2,S.limitr])';
S.xlim=M(:,1);
S.ylim=M(:,2);


S.Nunknown=fscanf(fid,'%d',[1]);
S.Xunknown=fscanf(fid,'%f',[5,S.Nunknown]);

fclose(fid);

% Poloidal/Toroidal flux/rho:
S.psiPol=S.ssimag + (S.ssibry - S.ssimag)*S.psiN;

X=S.psiPol;
Y=S.qpsidat;
S.psiTor=[0];
for j=2:mw
  tmp=quad( @(x)interp1(X,Y,x),X(j-1),X(j));
  S.psiTor=[S.psiTor ; S.psiTor(end) + tmp];
end
S.rhoPol=sqrt((S.psiPol-S.psiPol(1)) / (S.psiPol(end)-S.psiPol(1)));
S.rhoTor=sqrt((S.psiTor-S.psiTor(1)) / (S.psiTor(end)-S.psiTor(1)));

S.description_psiPol='1D profile: poloidal flux function';
S.description_psiTor='1D profile: toroidal flux function';
S.description_rhoPol='1D profile: normalised square root of the poloidal flux function, 0 <= rhoPol <= 1';
S.description_rhoTor='1D profile: normalised square root of the toroidal flux function, 0 <= rhoTor <= 1';
