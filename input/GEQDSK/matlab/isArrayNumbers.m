function [out]=isArrayNumbers(strIn);

str = deblank(strIn);
str = deblank(str(end:-1:1));
str = str(end:-1:1);

Iblank = find(str==' ');

if length(Iblank)==0
  
  [out]=isNumber(str);
  
else
  
  [out1]=isArrayNumbers(str(1:Iblank(1)));
  [out2]=isArrayNumbers(str(Iblank(1):end));
  
  out = out1 & out2;
  
end

  
