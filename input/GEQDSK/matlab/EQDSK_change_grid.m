

fn1='/u/tjohn/ITER/MachineDesign/Equilibrium/Scen2/equil_out_snap_22_65x136.eqdsk';
fn2='equil_out_snap_22_129x129.eqdsk';

NR=129;
NZ=129;

%InterpolationMethod='spline';
InterpolationMethod='cubic';
InterpolationMethod2='spline';

fid1=fopen(fn1,'r');
fid2=fopen(fn2,'w');

%-----------------------------------------
% Header line:

str=fgetl(fid1);
strStart='';
for j=1:6
  strStart=[strStart,str(1:8)];
  str=str(9:end);
end
eval(['m_dummy=',str(1:3),';']);
eval(['mw=',str(4:6),';']);
eval(['mh=',str(7:10),';']);
nw=mw;
nh=mh;

fprintf(fid2,' %s     %s %13s %2d %3d %3d %s\n','ITER Scen2 equil22',date,'',m_dummy,NR,NZ,' EQDSK');

%fprintf(fid2,' %46s %2d %3d %3d \n', [fn2,' ',date],m_dummy,NR,NZ);


r1=linspace(0,1,mw);
z1=linspace(0,1,mh);

r2=linspace(0,1,NR);
z2=linspace(0,1,NZ);

[r2m,z2m]=meshgrid(r2,z2);

%-----------------------------------------
% Scalars

for j=1:4
  M = fscanf(fid1,'%g',5);
  % str=fgetl(fid1);
  fprintf(fid2,'%15.7E ', M);
  fprintf(fid2,'\n');
end

%-----------------------------------------
% Fields

fpol1 = fscanf(fid1,'%g',[mw]);
fpol2 = interp1(r1,fpol1,r2 , InterpolationMethod );

pres1 = fscanf(fid1,'%g',[mw]);
pres2 = interp1(r1,pres1,r2 , InterpolationMethod );

ffprim1 = fscanf(fid1,'%g',[mw]);
ffprim2 = interp1(r1,ffprim1,r2 , InterpolationMethod );

pprime1 = fscanf(fid1,'%g',[mw]);
pprime2 = interp1(r1,pprime1,r2 , InterpolationMethod );

psirz1 = fscanf(fid1,'%g',[mw,mh])';

psirz2 =(... %interp2(r1(1:1:end), z1(1:1:end),psirz1(1:1:end,1:1:end), r2m, z2m , InterpolationMethod ) + ...
         interp2(r1(1   :1:end       ), z1([  1:2:end-1,end]),psirz1([  1:2:end-1,end],[  1:1:end      ]), r2m, z2m , InterpolationMethod ) + ...
         interp2(r1([1  :2:end-1,end]), z1(   1:1:end       ),psirz1([  1:1:end      ],[  1:2:end-1,end]), r2m, z2m , InterpolationMethod ) + ...
         interp2(r1([1  :2:end-1,end]), z1([  1:2:end-1,end]),psirz1([  1:2:end-1,end],[  1:2:end-1,end]), r2m, z2m , InterpolationMethod ) + ...
         interp2(r1(1   :1:end       ), z1([1,2:2:end-1,end]),psirz1([1,2:2:end-1,end],[  1:1:end      ]), r2m, z2m , InterpolationMethod ) + ...
         interp2(r1([1,2:2:end-1,end]), z1(   1:1:end       ),psirz1([  1:1:end      ],[1,2:2:end-1,end]), r2m, z2m , InterpolationMethod ) + ...
         interp2(r1([1,2:2:end-1,end]), z1([1,2:2:end-1,end]),psirz1([1,2:2:end-1,end],[1,2:2:end-1,end]), r2m, z2m , InterpolationMethod ) )/6;

%psirz2 = interp2(r1,z1,psirz1,r2m,z2m , InterpolationMethod );

qpsidat1 = fscanf(fid1,'%g',[mw]);
qpsidat2 = interp1(r1,qpsidat1,r2 , InterpolationMethod );


fprintf(fid2, '%15.7E %15.7E %15.7E %15.7E %15.7E\n', fpol2);
fprintf(fid2, '\n');
fprintf(fid2, '%15.7E %15.7E %15.7E %15.7E %15.7E\n', pres2);
fprintf(fid2, '\n');
fprintf(fid2, '%15.7E %15.7E %15.7E %15.7E %15.7E\n', ffprim2);
fprintf(fid2, '\n');
fprintf(fid2, '%15.7E %15.7E %15.7E %15.7E %15.7E\n', pprime2);
fprintf(fid2, '\n');

fprintf(fid2, '%15.7E %15.7E %15.7E %15.7E %15.7E\n', psirz2');
fprintf(fid2, '\n');

%for j=1:NZ
%  fprintf(fid2, '%15.7E %15.7E %15.7E %15.7E %15.7E\n', psirz2(j,:));
%  fprintf(fid2, '\n');
%end
fprintf(fid2, '%15.7E %15.7E %15.7E %15.7E %15.7E\n', qpsidat2);
fprintf(fid2, '\n');


%-----------------------------------------
% Boundaries

M=fscanf(fid1,'%d',[2]);
S.nbbbs  = M(1);
S.limitr = M(2);
fprintf(fid2, ' %3d %3d\n', S.nbbbs, S.limitr);


M=fscanf(fid1,'%g',[1,2*S.nbbbs]);
fprintf(fid2, '%15.7E %15.7E %15.7E %15.7E %15.7E\n', M);
fprintf(fid2, '\n');


M=fscanf(fid1,'%g',[1,2*S.limitr]);
fprintf(fid2, '%15.7E %15.7E %15.7E %15.7E %15.7E\n', M);
fprintf(fid2, '\n');

%-----------------------------------------

fclose(fid1);
fclose(fid2);

