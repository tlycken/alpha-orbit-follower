function [out]=isNumber(strIn);

str = deblank(strIn);

if length(str)==0
  out=0;
  return
end

out=1;
while length(str)>0
  if ( not( str(end)=='0' |str(end)=='1' |str(end)=='2' |str(end)=='3' |...
            str(end)=='4' |str(end)=='5' |str(end)=='6' |str(end)=='7' |...
            str(end)=='8' |str(end)=='9' ) )
    if length(deblank(str)) > 0
      out=0;
    end
    return
  end
  str=str(1:end-1);
end
return

