function O = OutputParams(EQ, diagnostic)
%
% function O=OutputParams(diagnostic)
%
% Set of hard-coded parameters for the output of AOF
%
%     diagnostic == true emits elements for diagnostic data dumps, while diagnostic == false doesn't
%

O.Diagnostics = struct();

rootfolder = 'output';

O.Main = struct('filePattern', [rootfolder '/traces/trace.txt']);
O.Summary = struct('fileName', [rootfolder '/summary.txt']);

Rmin = min(EQ.Rwall);
Rmax = max(EQ.Rwall);
dR = (Rmax-Rmin)/100;
Zmin = min(EQ.Zwall);
Zmax = max(EQ.Zwall);
dZ = (Zmax-Zmin)/100;

if (diagnostic)
    fields = struct();
    fields.name = 'grad-shafranov-fields';
    fields.fileName = [rootfolder '/diagnostics/field-dump.txt'];
    fields.Rmin = Rmin;
    fields.Rmax = Rmax;
    fields.dR = dR;
    fields.Zmin = Zmin;
    fields.Zmax = Zmax;
    fields.dZ = dZ;

    plasma = struct();
    plasma.name = 'thermal-plasma';
    plasma.fileName = [rootfolder '/diagnostics/plasma-dump.txt'];
    plasma.Rmin = Rmin;
    plasma.Rmax = Rmax;
    plasma.dR = dR;
    plasma.Zmin = Zmin;
    plasma.Zmax = Zmax;
    plasma.dZ = dZ;

    psi = struct();
    psi.name = 'psi';
    psi.fileName = [rootfolder '/diagnostics/psi-dump.txt'];
    psi.Rmin = Rmin;
    psi.Rmax = Rmax;
    psi.dR = dR;
    psi.Zmin = Zmin;
    psi.Zmax = Zmax;
    psi.dZ = dZ;

    F = struct();
    F.name = 'F';
    F.fileName = [rootfolder '/diagnostics/F-dump.txt'];
    F.psimin = EQ.grid1d.psi_min;
    F.psimax = EQ.grid1d.psi_max;
    F.dpsi = (F.psimax-F.psimin) / 100;

    O.Diagnostics.TwoD = [ fields, plasma, psi ];
    O.Diagnostics.OneD = [ F ];

    IC = struct();
    IC.name = 'IC';
    IC.fileName = [rootfolder '/diagnostics/IC-dump.txt'];

    O.Diagnostics.Custom = [ IC ];
end

end