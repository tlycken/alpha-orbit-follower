function CUR=current(R,Z,psi,Rlcfs,Zlcfs)
%
% function CUR=current(R,Z,psi,Rlcfs,Zlcfs)
%
% Relation between psi and the plasma current can be obtained from Helander & Sigmar, eq. (7.9) and (7.10) on pages 121 and 122 respectively:
%   J = R * div( grad(psi)/R^2 ) / mu_0
%
% Here R,Z,psi are all matrixes. (R,Z) for a rectangular, equidistant coordinate system, where diff(R(:,i))=0 and diff(Z(i,:))=0 for all i.
%
% Example:
%  load('JETequilibria/JETequilibrium_pulse42976_at_53.2s.mat');
%  CUR=current(EQ.R,EQ.Z,EQ.psi,EQ.Rsep,EQ.Zsep); contour(EQ.R,EQ.Z,CUR.J_inside); CUR.I_inside,CUR.I
%
psi = psi ./ (2*pi);

mu0 = 4e-7*pi;

Rv=R(1,:);
Zv=Z(:,1);
[dpsi_dr,dpsi_dz] = gradient(psi,Rv,Zv);

% Construct the two terms in "div( grad(psi)/R^2 )"
[term1,dum] = gradient( dpsi_dr ./ R.^2 ,Rv,Zv);
[dum,term2] = gradient( dpsi_dz ./ R.^2 ,Rv,Zv);

CUR.J =  (term1 + term2) .* R / mu0;

psi_lcfs = mean(interp2(Rv,Zv,psi,Rlcfs,Zlcfs));
CUR.J_inside = CUR.J;
CUR.J_inside( (psi > psi_lcfs) ) = 0;
CUR.J_inside( (Z < min(Zlcfs)) ) = 0;
CUR.J_inside( (Z > max(Zlcfs)) ) = 0;

dA = mean(diff(Rv(:))) * mean(diff(Zv(:)));

CUR.I = dA * sum( CUR.J(:) );
CUR.I_inside = dA * sum( CUR.J_inside(:) );
