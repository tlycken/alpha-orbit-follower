function [PLASMA]=ReadJetPlasmaParams(EQ, dn0, fn_Ti, fn_ne, fn_Te, Z_lidr, Z_cxfs, doPlot)
%
% function [PLASMA]=ReadJetPlasmaParams(EQ, Z_lidr, Z_cxfs, doPlot)
%
% NOTE: You need 
%
% 42976_at_53.2s_Profiles/cxfs_ti_53.2s.txt
% 42976_at_53.2s_Profiles/lidr_ne_52.3s.txt
% 42976_at_53.2s_Profiles/lidr_te_52.3s.txt
%
% Example:
% Npsi1d=80;
% filename='JETequilibria/JETequilibrium_pulse42976_at_53.2s.mat';
% [EQ_AOF]=mapJetEQtoAOF(filename,Npsi1d);
% PLASMA=ReadJetPlasmaParams(EQ_AOF);
%

if nargin<2
  dn0='42976_at_53.2s_Profiles';
end
if nargin<3
  fn_Ti='cxfs_ti_53.2s.txt';
  
end
if nargin<4
  fn_ne='lidr_ne_52.3s.txt';
end
if nargin<5
  fn_Te='lidr_te_52.3s.txt';
end
if nargin<6
  Z_lidr = 0.2;
end
if nargin<7
  Z_cxfs = 0;
end
if nargin<8
  doPlot = false;
end

%-------------------------------------
% Hardcoded: plasma composition
%-------------------------------------

% Hydrogen concentration
PLASMA.X_H=0.01;

% D/T concentrations
PLASMA.X_D=(1-PLASMA.X_H)/2;
PLASMA.X_T=1-PLASMA.X_H-PLASMA.X_D;

PLASMA.species.amn = [5.4858e-4 , 1 , 2 , 3];
PLASMA.species.zn  = [       -1 , 1 , 1 , 1];
PLASMA.species.zion= [       -1 , 1 , 1 , 1];
PLASMA.species.name= {'Electrons','Hydrogen','Deuterium','Tritium'};
PLASMA.species.symbol={'e','H','D','T'};
PLASMA.species.concentration=[1.0, PLASMA.X_H, PLASMA.X_D, PLASMA.X_T];

PLASMA.Reaction = struct();
PLASMA.Reaction.Reactant1 = 'Deuterium';
PLASMA.Reaction.Reactant2 = 'Tritium';
PLASMA.Reaction.Product = 'alpha';
PLASMA.Reaction.BG = 34.3827;
PLASMA.Reaction.mc2 = 1124656;
PLASMA.Reaction.C1 = 1.17302e-9;
PLASMA.Reaction.C2 = 1.51361e-2;
PLASMA.Reaction.C3 = 7.51886e-2;
PLASMA.Reaction.C4 = 4.60643e-3;
PLASMA.Reaction.C5 = 1.35000e-2;
PLASMA.Reaction.C6 = -1.06750e-4;
PLASMA.Reaction.C7 = 1.36600e-5;
PLASMA.Reaction.Tmin = 0.2e3;
PLASMA.Reaction.Tmax = 100e3;

%-------------------------------------
% Read raw experimental data
%-------------------------------------

PLASMA.raw_Ti=readJetProfileFormat([dn0,'/',fn_Ti]);
PLASMA.raw_Te=readJetProfileFormat([dn0,'/',fn_Te]);
PLASMA.raw_ne=readJetProfileFormat([dn0,'/',fn_ne]);

%-----------------------------------------
% Smooth the density profile !!! UGLY !!!
%-----------------------------------------

PLASMA.raw_ne.y_smooth = PLASMA.raw_ne.y;
II=find(PLASMA.raw_ne.y > 3.5e19);
PLASMA.raw_ne.y_smooth(II) = mean(PLASMA.raw_ne.y(II)) * ones(size(II));

%------------------------------------------
% Construct measure-points in psi(R,Z)
%------------------------------------------

R2d=linspace(EQ.grid2d.Rlim(1),EQ.grid2d.Rlim(2),EQ.grid2d.NR);
Z2d=linspace(EQ.grid2d.Zlim(1),EQ.grid2d.Zlim(2),EQ.grid2d.NZ);

x1=PLASMA.raw_ne.x;
y1=Z_lidr + zeros(size(x1));
PLASMA.raw_ne_mapped_2d.psi = interp2(R2d,Z2d,EQ.grid2d.psi, x1 , y1, 'spline');
x2=PLASMA.raw_Te.x;
y2=Z_lidr + zeros(size(x2));
PLASMA.raw_Te_mapped_2d.psi = interp2(R2d,Z2d,EQ.grid2d.psi, x2 , y2);

x3=PLASMA.raw_Ti.x;
y3=Z_cxfs + zeros(size(x3));
PLASMA.raw_Ti_mapped_2d.psi = interp2(R2d,Z2d,EQ.grid2d.psi, x3 , y3);


if (doPlot)
  figure(1)
  clf;
  contour(R2d,Z2d,EQ.grid2d.psi,80)
  colorbar
  hold on
  plot(EQ.Rlcfs,EQ.Zlcfs,'k--')
  plot(EQ.Rwall,EQ.Zwall,'k')
  plot(x1 , y1 , 'rx')
  %plot(x2 , y2 , 'go') % Same as (x1,y2)
  plot(x3 , y3 , 'kd')
end

%------------------------------------------
% Interpolate onto grid1d
%------------------------------------------
psi1d=linspace(EQ.grid1d.psi_min, EQ.grid1d.psi_max, EQ.grid1d.Npsi);

psi_raw = PLASMA.raw_Ti_mapped_2d.psi;
dpsi = diff(psi_raw);
II0 = find(sign( dpsi(1:end-1) .* dpsi(2:end) ) == -1) + 1;
II_outboard = II0:length(psi_raw);
PLASMA.Temperature      = interp1(psi_raw(II_outboard) , PLASMA.raw_Ti.y(II_outboard) , psi1d,'linear','extrap');

psi_raw = PLASMA.raw_ne_mapped_2d.psi;
dpsi = diff(psi_raw);
II0 = find(sign( dpsi(1:end-1) .* dpsi(2:end) ) == -1) + 1;
II_outboard = II0:length(psi_raw);
PLASMA.DensityElectrons = interp1(psi_raw(II_outboard) , PLASMA.raw_ne.y_smooth(II_outboard) , psi1d,'linear','extrap');


if (doPlot)
  figure(2)
  clf
  subplot(121); hold on
  %plot(PLASMA.raw_Ti_mapped_2d.psi , PLASMA.raw_Ti.y,'bx-')
  %plot( psi1d , PLASMA.Temperature,'r.-')
  plot( sqrt(psi1d-psi1d(1)) , PLASMA.Temperature,'r.-')
  
  subplot(122); hold on
  plot(PLASMA.raw_ne_mapped_2d.psi , PLASMA.raw_ne.y,'b.-')
  plot(PLASMA.raw_ne_mapped_2d.psi(II_outboard) , PLASMA.raw_ne.y(II_outboard),'bx')
  plot( psi1d , PLASMA.DensityElectrons,'r.-')
  plot( psi1d , PLASMA.DensityElectrons,'r.-')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function D=readJetProfileFormat(fn)

fid=fopen(fn,'r');
D.header.signal=fgetl(fid);
D.header.type=fgetl(fid);
M=fscanf(fid,'%f',[1,inf]);
D.x=M(1:2:end);
D.y=M(2:2:end);
fclose(fid);
end

end
