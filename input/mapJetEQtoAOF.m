function [AOF,EQ]=mapJetEQtoAOF(filename, Npsi, factor_rescale_current, factor_rescale_B0, flip_sign_psi1d, COCOS_psi)
%
% function [AOF]=mapJetEQtoAOF(filename)
%
% Ex:
% filename='JETequilibria/JETequilibrium_pulse42976_at_53.2s.mat';[AOF]=mapJetEQtoAOF(filename,80);

if nargin < 6
  COCOS_psi=2*pi;
end

load(filename);

%------------------------------------------------
% 2D data
%------------------------------------------------
% To avoid spurious oscillations, take only every d-th grid-point
d = 6;

AOF.grid2d.Rlim=[ min(EQ.R(:)) , max(EQ.R(:)) ];
AOF.grid2d.Zlim=[ min(EQ.Z(:)) , max(EQ.Z(:)) ];
AOF.grid2d.NR=size(EQ.R(1:d:end,1:d:end),2);
AOF.grid2d.NZ=size(EQ.R(1:d:end,1:d:end),1);
AOF.grid2d.psi=EQ.psi(1:d:end,1:d:end) * COCOS_psi;

%------------------------------------------------
% 1D data
%------------------------------------------------
AOF.grid1d.psi_min = interp2(EQ.R(1:d:end,1:d:end),EQ.Z(1:d:end,1:d:end),EQ.psi(1:d:end,1:d:end),EQ.R0,EQ.Z0) * COCOS_psi;
AOF.grid1d.psi_max = mean(interp2(EQ.R(1:d:end,1:d:end),EQ.Z(1:d:end,1:d:end),EQ.psi(1:d:end,1:d:end),EQ.Rsep,EQ.Zsep)) * COCOS_psi;
AOF.grid1d.Npsi = Npsi;
AOF.grid1d.psi = linspace(AOF.grid1d.psi_min , AOF.grid1d.psi_max , Npsi);

R_equitorial_plane = EQ.R(1,1:d:end);
Z_equitorial_plane = EQ.Z0-zeros(1,AOF.grid2d.NR);

psi_equitorial_plane = interp2(EQ.R(1:d:end,1:d:end),EQ.Z(1:d:end,1:d:end), EQ.psi(1:d:end,1:d:end)      ,R_equitorial_plane, Z_equitorial_plane) * COCOS_psi;
F_equitorial_plane   = interp2(EQ.R(1:d:end,1:d:end),EQ.Z(1:d:end,1:d:end), EQ.BT(1:d:end,1:d:end).*EQ.R(1:d:end,1:d:end) ,R_equitorial_plane, Z_equitorial_plane);

I_outboard=( R_equitorial_plane > EQ.R0 );
psi_outboard    = psi_equitorial_plane(I_outboard);
psi_outboard(1) = AOF.grid1d.psi_min;
F_outboard     = F_equitorial_plane(I_outboard);
AOF.grid1d.F = interp1( psi_outboard , F_outboard , AOF.grid1d.psi);

%------------------------------------------------
% Wall data
%------------------------------------------------
AOF.Rwall = EQ.Rw(2:end);
AOF.Zwall = EQ.Zw(2:end);

%------------------------------------------------
% The Last Closed Flux Surface
%------------------------------------------------
AOF.Rlcfs = EQ.Rsep(2:end);
AOF.Zlcfs = EQ.Zsep(2:end);

%------------------------------------------------
% Magnetic axis
%------------------------------------------------
AOF.MagAxis.R=EQ.R0;
AOF.MagAxis.Z=EQ.Z0;


%------------------------------------------------
% Rescale the magnetic field
%------------------------------------------------
if nargin >= 3
  AOF.grid1d.psi_min = AOF.grid1d.psi_min * factor_rescale_current;
  AOF.grid1d.psi_max = AOF.grid1d.psi_max * factor_rescale_current;
  AOF.grid1d.psi = AOF.grid1d.psi * factor_rescale_current;
  AOF.grid2d.psi = AOF.grid2d.psi * factor_rescale_current;
endif

if nargin >= 4
  AOF.grid1d.F = AOF.grid1d.F * factor_rescale_B0;
end

if nargin >= 5
  psi_max=AOF.grid1d.psi_min;
  psi_min=AOF.grid1d.psi_max;
  AOF.grid1d.psi_min = - psi_max;
  AOF.grid1d.psi_max = - psi_min;
end

end