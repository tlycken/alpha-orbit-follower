function write_wml(EQ,PLASMA,NUMERICS,OUTPUT,filenameXMLoutput)
%
% Write the input EQ to an xml-file readable by the AOF code.
% Part of the masters thesis project of Tomas Lycken, spring 2014.
%

if nargin<2
  filenameXMLoutput='AOFparameters.xml';
end
[status,UserName]=unix('whoami');

%-----------------------------
% Auxiliary functions
%-----------------------------

function write_uniform_grid(fid, name, minValue, maxValue, gridPoints, indentation)
  fprintf(fid, '%s<grid name="%s">\n', indentation, name);
  fprintf(fid, '%s  <uniform minValue="%f" maxValue="%f" gridPoints="%d" />\n', indentation, minValue, maxValue, gridPoints);
  fprintf(fid, '%s</grid>\n', indentation);
end

function write_1d_field(fid, name, coord, values, indentation)
  fprintf(fid, '%s<field name="%s" dimensions="1">\n', indentation, name);
  fprintf(fid, '%s  <coordinate name="%s" dimension="1" />\n', indentation, coord);
  fprintf(fid, '%s  <values>\n%s  ', indentation, indentation);
  fprintf(fid, ' %7.6e', values);
  fprintf(fid, '\n%s  </values>\n%s</field>\n', indentation, indentation);
end

function write_polygon(fid, Rlist, Zlist, indentation)
  for j=1:length(Rlist)
    fprintf(fid,'%s<vertex R="%f" Z="%f"/>\n', indentation, Rlist(j) , Zlist(j));
  end
end

%-----------------------------
% Write xml-file
%-----------------------------

fid=fopen(filenameXMLoutput,'w');

% TinyBind chokes on this =(
%fprintf(fid,'<?xml version="1.0" encoding="utf-8"?>\n');
fprintf(fid,'<AOFinput \n');
fprintf(fid,'    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n');
fprintf(fid,'    xsi:noNamespaceSchemaLocation="AOFinput.xsd">\n');

% METADATA

fprintf(fid,'  <meta author="%s" date="%s">\n',deblank(UserName),datestr(now,'yyyy-mm-dd'));
fprintf(fid,'    <comment>\n');
fprintf(fid,'    Input for AOF (masters thesis project of Tomas Lycken, spring 2014)\n');
fprintf(fid,'    - Equilibrium parameters taken from %s\n',EQ.inputReference);
fprintf(fid,'    - Plasma parameters taken from %s\n',PLASMA.inputReference);
fprintf(fid,'    - Numerical parameters taken from %s\n',NUMERICS.inputReference);
fprintf(fid,'    - Output specification taken from %s\n',OUTPUT.inputReference);
fprintf(fid,'    </comment>\n');
fprintf(fid,'  </meta>\n\n');

fprintf(fid,'  <physicalData>\n');

% MAGNETIC FIELD

fprintf(fid,'    <magneticField>\n');
fprintf(fid,'      <representation name="GradShafranov">\n');
write_uniform_grid(fid, 'R', EQ.grid2d.Rlim(1), EQ.grid2d.Rlim(2), EQ.grid2d.NR, '        ');
write_uniform_grid(fid, 'Z', EQ.grid2d.Zlim(1), EQ.grid2d.Zlim(2), EQ.grid2d.NZ, '        ');
write_uniform_grid(fid, 'psi', EQ.grid1d.psi_min, EQ.grid1d.psi_max, EQ.grid1d.Npsi, '        ');
fprintf(fid,'        <field name="psi" dimensions="2">\n');
fprintf(fid,'          <coordinate name="R" dimension="1"/>\n');
fprintf(fid,'          <coordinate name="Z" dimension="2"/>\n');
fprintf(fid,'          <values>\n              ');
fprintf(fid,'%4.3e ',EQ.grid2d.psi');
fprintf(fid,'\n          </values>\n');
fprintf(fid,'        </field>\n');
write_1d_field(fid, 'F', 'psi', EQ.grid1d.F, '        ');
fprintf(fid,'      </representation>\n');
fprintf(fid,'      <topology>\n');
fprintf(fid,'        <Opoint R="%f" Z="%f" mainAxis="true" />\n', EQ.MagAxis.R, EQ.MagAxis.Z);
fprintf(fid,'        <separatrix type="polygon" lastClosed="true">\n');
write_polygon(fid, EQ.Rlcfs, EQ.Zlcfs, '          ');
fprintf(fid,'        </separatrix>\n');
fprintf(fid,'      </topology>\n');
fprintf(fid,'    </magneticField>\n');

% THERMAL PLASMA

fprintf(fid,'    <plasma>\n');
for jspec=1:length(PLASMA.species.amn)
  fprintf(fid,'      <species amn="%f" zn="%f" zion="%f" name="%s" symbol="%s">\n', ...
    PLASMA.species.amn(jspec), PLASMA.species.zn(jspec), PLASMA.species.zion(jspec), PLASMA.species.name{jspec}, PLASMA.species.symbol{jspec});

  write_uniform_grid(fid, 'psi', EQ.grid1d.psi_min, EQ.grid1d.psi_max, EQ.grid1d.Npsi, '        ');
  write_1d_field(fid, 'temperature', 'psi', PLASMA.Temperature, '        ');
  write_1d_field(fid, 'density', 'psi', PLASMA.DensityElectrons * PLASMA.species.concentration(jspec), '        ');
  fprintf(fid,'      </species>\n');
end
fprintf(fid, '      <reaction>\n');
fprintf(fid, '        <reactant name="%s" />\n', PLASMA.Reaction.Reactant1);
fprintf(fid, '        <reactant name="%s" />\n', PLASMA.Reaction.Reactant2);
fprintf(fid, '        <product name="%s" />\n', PLASMA.Reaction.Product);
fprintf(fid, '        <rateCoefficient>\n');
fprintf(fid, '          <parametrization ');
fprintf(fid, 'BG="%e" mc2="%e" C1="%e" C2="%e" C3="%e" C4="%e" C5="%e" C6="%e" C7="%e" Tmin="%e" Tmax="%e" />\n', ...
  PLASMA.Reaction.BG, ...
PLASMA.Reaction.mc2, ...
PLASMA.Reaction.C1, ...
PLASMA.Reaction.C2, ...
PLASMA.Reaction.C3, ...
PLASMA.Reaction.C4, ...
PLASMA.Reaction.C5, ...
PLASMA.Reaction.C6, ...
PLASMA.Reaction.C7, ...
PLASMA.Reaction.Tmin, ...
PLASMA.Reaction.Tmax);
fprintf(fid, '        </rateCoefficient>\n');
fprintf(fid, '     </reaction>\n');
fprintf(fid,'    </plasma>\n');
fprintf(fid,'\n');

% CHAMBER WALLS

fprintf(fid,'    <chamber name="polygon">\n');
write_polygon(fid, EQ.Rwall, EQ.Zwall, '      ');
fprintf(fid,'    </chamber>\n');

fprintf(fid,'  </physicalData>\n');

% NUMERICAL INPUT PARAMETERS

fprintf(fid,'\n');
fprintf(fid,'  <numericalData>\n');
fprintf(fid,'    <initialCondition name="cloud" Nparticles="%d" E0="%g" particleType="%s" time="%g" />\n', ...
  NUMERICS.IC.Nparticles, NUMERICS.IC.E0, NUMERICS.IC.Type, NUMERICS.IC.InitTime);
fprintf(fid,'    <finalCondition time="%f" laps="%d" />\n', NUMERICS.FC.Time, NUMERICS.FC.Laps);
fprintf(fid,'    <integratorParameters stepLength="%f" />\n', NUMERICS.Step);
fprintf(fid,'  </numericalData>\n');

% OUTPUT SPECIFICATIONS

fprintf(fid,'\n');
fprintf(fid,'  <output>\n');
if isfield(OUTPUT, 'Main')
  if isfield(OUTPUT.Main, 'fileName')
    fprintf(fid,'    <main fileName="%s" />\n', OUTPUT.Main.fileName);
  elseif isfield(OUTPUT.Main, 'filePattern')
    fprintf(fid,'    <main filePattern="%s" />\n', OUTPUT.Main.filePattern);
  else
    err = MException('write_xml:InvalidMainOutput', 'Must specify either fileName or filePattern on main output');
    throw(err);
  end
end

fprintf(fid,'    <summary fileName="%s" />\n', OUTPUT.Summary.fileName);

for i=1:length(OUTPUT.Diagnostics.TwoD)
  d = OUTPUT.Diagnostics.TwoD(i);
  fprintf(fid, '    <diagnostic name="%s" fileName="%s" Rmin="%f" Rmax="%f" dR="%f" Zmin="%f" Zmax="%f" dZ="%f" />\n', d.name, d.fileName, d.Rmin, d.Rmax, d.dR, d.Zmin, d.Zmax, d.dZ);
end
for i = 1:length(OUTPUT.Diagnostics.OneD)
  d = OUTPUT.Diagnostics.OneD(i);
  fprintf(fid, '    <diagnostic name="%s" fileName="%s" psimin="%f" psimax="%f" dpsi="%f" />\n', d.name, d.fileName, d.psimin, d.psimax, d.dpsi);
end
for i = 1:length(OUTPUT.Diagnostics.Custom)
  d = OUTPUT.Diagnostics.Custom(i);
  fprintf(fid, '    <diagnostic name="%s" fileName="%s" />\n', d.name, d.fileName);
end
fprintf(fid,'  </output>\n');


fprintf(fid,'</AOFinput>\n');
fprintf(fid,'\n');

fclose(fid);

fprintf('Done! Wrote to %s\n', filenameXMLoutput);

end