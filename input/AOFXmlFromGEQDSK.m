function [GEQ,EQ_AOF,PLASMA_AOF,OUTPUT_AOF,NUMERICS_AOF,S]=AOFXmlFromGEQDSK(machine,Nparticles,filenameInput,filenamePlasmaParams,HeaderType)
%
% function [GEQ,EQ_AOF,PLASMA_AOF]=AOFXmlFromGEQDSK(filenameInputGEQDSK,filenameXMLoutput,HeaderType)
%
% Ex:
%    [GEQ,EQ_AOF,PLASMA_AOF]=AOFXmlFromGEQDSK('iter', 1000,'GEQDSK/examples/iter_Scen2_2007_equil_out_snap_20_65x136.eqdsk','psi_ne_Ti.dat', 'iter.xml',1);
%
% If you're happy with these defaults, you can just run
%
%   [GEQ,EQ,PLASMA] = AOFXmlFromGEQDSK()
%
addpath('GEQDSK/matlab');

% Default input
if nargin<1
  machine='iter';
%  machine='jet';
end
if nargin<2
  Nparticles=1000;
end
if nargin<5
  filenameXMLoutput = [machine '.xml'];
end
if nargin<6
  HeaderType=1;
end

if strcmp(machine,'iter')
  if nargin<3
      filenameInput='GEQDSK/examples/iter_Scen2_2007_equil_out_snap_20_65x136.eqdsk';
  end
  if nargin<4
    filenamePlasmaParams='psi_ne_Ti.dat';
  end

  % Equilibrium
  [GEQ,ier]=read_geqdsk( filenameInput, HeaderType );
  [EQ_AOF]=mapGEQDSKtoAOF( GEQ );
  EQ_AOF.inputReference=filenameInput;

  % Plasma
  if strcmp(filenamePlasmaParams,'hardcoded')
    [PLASMA_AOF]=HardcodedPlasmaParams( EQ_AOF.grid1d.Npsi, EQ_AOF );
    PLASMA_AOF.inputReference='HardcodedPlasmaParams.m';
  else
    [PLASMA_AOF]=ReadIterPlasmaParams( filenamePlasmaParams, EQ_AOF );
    PLASMA_AOF.inputReference=filenamePlasmaParams;
  end
elseif strcmp(machine,'jet')
  if nargin<3
    filenameInput='JETequilibria/JETequilibrium_pulse42976_at_53.2s.mat';
  end
  if nargin<4
    dn0='42976_at_53.2s_Profiles';
    fn_Ti='cxfs_ti_53.2s.txt';
    fn_ne='lidr_ne_52.3s.txt';
    fn_Te='lidr_te_52.3s.txt';
  else
    dn0 = filenamePlasmaParams(1);
    fn_Ti = filenamePlasmaParams(2);
    fn_ne = filenamePlasmaParams(3);
    fn_Te = filenamePlasmaParams(4);
  end

  EQ_AOF = mapJetEQtoAOF(filenameInput,100);
  EQ_AOF.inputReference = filenameInput;
  PLASMA_AOF = ReadJetPlasmaParams(EQ_AOF, dn0, fn_Ti, fn_ne);
  PLASMA_AOF.inputReference = filenameInput;
  GEQ=0;
end

% Numerical data
[NUMERICS_AOF]=HardcodedNumericalParams(Nparticles);
NUMERICS_AOF.inputReference='HardcodedNumericalParams.m';

% Output data
[OUTPUT_AOF]=OutputParams(EQ_AOF,true);
OUTPUT_AOF.inputReference='OutputParams.m';

% Write output xml
write_xml( EQ_AOF, PLASMA_AOF, NUMERICS_AOF, OUTPUT_AOF, filenameXMLoutput );

end
