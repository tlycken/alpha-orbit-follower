#include "aof/aof-config.h"
#include <iostream>
#include <chrono>

#include "aof/xml/xml-factory.hpp"
#include "aof/numerics/ode-steppers.hpp"
#include "aof/implementation-choices.hpp"

using namespace aof;
using namespace aof::xml::factories;
using namespace aof::physics;
using namespace aof::ode;

using namespace std::chrono;

void usage();

int main(int argc, char* argv[]) {

    if (argc < 2) {
        usage();
        return 0;
    }

    // try {

        high_resolution_clock::time_point start = high_resolution_clock::now();

        std::string fname(argv[1]);

        XmlFactory fac(fname);

        auto driver = fac.create<driver_t>();

        std::cout << "Initiation complete.\nStarting integration runs...\n";
        std::cout.flush();

        timings time = driver.integrateAll();
        std::cout << '\r' << "Done!\n";
        std::cout << "\tIC time:      " << time.icTime << " seconds.\n";
        std::cout << "\tTracing time: " << time.traceTime << " seconds." << std::endl;

        high_resolution_clock::time_point finish = high_resolution_clock::now();
        duration<double> total_time = duration_cast<duration<double>>(finish-start);

        std::cout << "\tTotal time: " << total_time.count() << " seconds." << std::endl;

        return 0;

    // } catch (std::exception& e) {
    //     std::cerr << "Uncaught exception during execution: " << e.what() << std::endl;
    //     return -1;
    // }
}

void usage() {
    std::cout << aof_NAME;
    std::cout << " version " << aof_MAJOR_VERSION << "." << aof_MAJOR_VERSION << std::endl;

    std::cout << std::endl;

    std::cout << "Usage:" << std::endl;
    std::cout << "\t./" << aof_NAME << " ";
    std::cout << "<path-to-xml-input>";

    std::cout << std::endl;
}
