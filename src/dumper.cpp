#include "aof/xml/xml-factory.hpp"
#include "aof/implementation-choices.hpp"

#include <iostream>

void usage();

using namespace std;
using namespace aof;
using namespace xml::factories;

int main(int argc, char* argv[]) {

    if (argc < 2) {
        std::cout << "dumper\n\nUsage:\n\tdumper <path-to-xml-input>" << std::endl;
        return -1;
    }

    std::string fname(argv[1]);
    XmlFactory fac(fname);

    std::cout << "Dumping magnetic field...";
    std::cout.flush();
    auto bfield = fac.create<globalfield_t>(true);
    std::cout << " done!" << std::endl;

    std::cout << "Dumping thermal plasma...";
    std::cout.flush();
    auto plasma = fac.create<plasma_t>(bfield);
    std::cout << " done!" << std::endl;

    std::cout << "Dumping initial conditions...";
    std::cout.flush();
    auto icfactory = fac.create<initialcondition_t>(bfield);

    std::ostream& icf = (fac.create<std::unique_ptr<IOutputBuilder>>(std::string("diagnostic"), "IC"))->get();
    icf << "R" << SEPARATOR;
    icf << "Z" << SEPARATOR;
    icf << "Weight" << SEPARATOR;
    icf << "Psi" << std::endl;

    while (icfactory.hasNext()) {
        auto ic = icfactory.next();
        icf << ic.s(0) << SEPARATOR;
        icf << ic.s(2) << SEPARATOR;
        icf << ic.w << SEPARATOR;
        icf << bfield.psi(ic.s(0), 0.0, ic.s(2)) << std::endl;
    }
    if (icf != std::cout) {
        delete &icf;
    }
    std::cout << " done!" << std::endl;
}
