#include "aof/physics/plasma.hpp"


using namespace aof::physics;

ParametrizedRateCoefficient::ParametrizedRateCoefficient(double BG, double mc2, double C1, double C2, double C3, double C4, double C5, double C6, double C7, double Tmin, double Tmax) : BG2(BG* BG), mc2(mc2), C1(C1), C2(C2), C3(C3), C4(C4), C5(C5), C6(C6), C7(C7), Tmin(Tmin), Tmax(Tmax) { }

double ParametrizedRateCoefficient::rateCoeff(double Ti) const {
    if (Ti < Tmin || Ti > Tmax) {
        throw std::range_error("The temperature " + std::to_string(Ti) + " is outside the allowed interval [" + std::to_string(Tmin) + "," + std::to_string(Tmax) + "]");
    }

    Ti = .001 * Ti; // Convert to keV

    double theta = Ti / (1 - Ti * (C2 + Ti * (C4 + Ti * C6)) / (1 + Ti * (C3 + Ti * (C5 + Ti * C7))));
    double ksi = std::cbrt(BG2 / (4 * theta));
    return C1 * theta * std::sqrt(ksi / (mc2 * std::pow(Ti, 3.))) * std::exp(-3. * ksi) * 1e-6;
}
