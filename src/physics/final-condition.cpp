#include "aof/physics/final-condition.hpp"

using namespace aof::physics;

std::ostream &aof::physics::operator<< (std::ostream &s, reason r) {
    switch (r) {
    case reason::time: s << "time"; break;
    case reason::loop: s << "loop"; break;
    case reason::wall: s << "wall"; break;
    }
    return s;
}
