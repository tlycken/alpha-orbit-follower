#include "aof/physics/chamber.hpp"
#include "aof/physics/bounded-volume.hpp"

#include "aof/algebra/vector-algebra.hpp"

#include <cmath>
#include <algorithm>
#include <iostream>
#include <fstream>

using namespace aof::physics;

bool AxisymmetricPolygon::isInside(double R, double __attribute__((__unused__)) phi, double Z) const {
    int wn = 0;
    uint N = vxs.size();

    double Redge;

    for (uint i = 0; i < N; ++i) {
        auto cur = vxs[i];
        auto next = vxs[(i + 1) % N];

        if (next(2) > Z && cur(2) < Z) {
            Redge = cur(0) + (Z - cur(2)) * (next(0) - cur(0)) / (next(2) - cur(2));
            if (Redge > R) {
                ++wn;
            }
        } else if (next(2) < Z && cur(2) > Z) {
            Redge = cur(0) + (Z - cur(2)) * (next(0) - cur(0)) / (next(2) - cur(2));
            if (Redge > R) {
                --wn;
            }
        }
    }
    return wn != 0;
}

void AxisymmetricPolygon::add_vertex(double R, double __attribute__((__unused__)) phi, double Z) {
    vector<3> v; v << R, 0.0, Z;
    add_vertex(v);
}

void AxisymmetricPolygon::add_vertex(vertex vx) {
    vxs.push_back(vx);
    minR = std::min(minR, vx(0));
    maxR = std::max(maxR, vx(0));
    minZ = std::min(minZ, vx(2));
    maxZ = std::max(maxZ, vx(2));
}

double AxisymmetricPolygon::surfaceArea() const {
    double area = 0.0;

    unsigned int N = vxs.size();
    for (unsigned int i = 0; i < N; ++i) {
        area += vxs[i](0) * vxs[(i + 1) % N](2)
                - vxs[i](2) * vxs[(i + 1) % N](0);
    }

    // If the nodes are given in clock-wise order, this algorithm obtains
    // a negative (but otherwise correct) area. Counter with abs()
    return abs(area) / 2.;
}

double AxisymmetricPolygon::volume() const {
    double volume = 0.0, h, R, r;

    unsigned int N = vxs.size();
    for (unsigned int i = 0; i < N; ++i) {
        h = vxs[(i + 1) % N](2) - vxs[i](2);
        r = vxs[i](0);
        R = vxs[(i + 1) % N](0);
        volume += h * (R * R + R * r + r * r);
    }

    // If the nodes are given in clock-wise order, this algorithm obtains
    // a negative (but otherwise correct) volume. Counter with abs()
    return abs(volume) * 3.1415926 / 3.;
}

double AxisymmetricPolygon::Rmin() const {
    return minR;
}
double AxisymmetricPolygon::Rmax() const {
    return maxR;
}
double AxisymmetricPolygon::phimin() const {
    return minphi;
}
double AxisymmetricPolygon::phimax() const {
    return maxphi;
}
double AxisymmetricPolygon::Zmin() const {
    return minZ;
}
double AxisymmetricPolygon::Zmax() const {
    return maxZ;
}
