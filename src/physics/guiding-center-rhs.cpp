#include "aof/physics/guiding-center-rhs.hpp"
#include "aof/io/output.hpp"

#include "aof/algebra/vector-algebra.hpp"

using namespace aof;

template<>
std::ostream &header<aof::vector<6>>(std::ostream &os) {
    os << "Time" << aof::SEPARATOR;
    os << "R" << aof::SEPARATOR;
    // os << "Phi" << aof::SEPARATOR;
    os << "Z" << aof::SEPARATOR;
    os << "VParallel" << aof::SEPARATOR;
    // os << "Mu" << aof::SEPARATOR;
    // os << "Phase" << aof::SEPARATOR;
    // os << "RDot" << aof::SEPARATOR;
    // os << "PhiDot" << aof::SEPARATOR;
    // os << "ZDot" << aof::SEPARATOR;
    os << "VParallelDot";
    // os << "MuDot" << aof::SEPARATOR;
    // os << "PhaseDot";
    return os;
}


std::ostream &operator<<(std::ostream &os, aof::vector<6> s) {
    os << s.transpose();
    return os;
}

std::ostream &operator<<(std::ostream &os, std::vector<aof::physics::solpoint<aof::vector<6>>> trace) {
    header<vector<6>>(os) << std::endl;
    for (unsigned int i = 0; i < trace.size(); ++i) {
        os << trace[i].t << aof::SEPARATOR;
        os << trace[i].X; // << aof::SEPARATOR;
        os << "\n";
        // os << trace[i].rhs << std::endl;
    }
    os.flush();
    return os;
}
