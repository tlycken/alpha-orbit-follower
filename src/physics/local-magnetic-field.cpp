#include "aof/algebra/vector-algebra.hpp"
#include "aof/physics/local-magnetic-field.hpp"

using namespace aof;
using namespace aof::physics;

LocalMagneticField::LocalMagneticField(double R, double phi, double Z,
                                       vector<3> B, matrix<3, 3> nabla_outerprod_B) : R(R), phi(phi), Z(Z), Bvec(B), nabla_outerprod_B(nabla_outerprod_B) {
    if (B.norm() == 0) {
        throw zero_magnetic_field_exception();
    }

    lenB = B.norm();
    lenB2 = B.squaredNorm();
}

vector<3> LocalMagneticField::B() const {
    return Bvec;
}

vector<3> LocalMagneticField::b() const {
    return Bvec / lenB;
}

vector<3> LocalMagneticField::gradB() const {
    return nabla_outerprod_B * b();
}

vector<3> LocalMagneticField::rotb() const {
    return vector<3>(nabla_outerprod_B(1, 2) - nabla_outerprod_B(2, 1),
                     nabla_outerprod_B(2, 0) - nabla_outerprod_B(0, 2),
                     nabla_outerprod_B(0, 1) - nabla_outerprod_B(1, 0)) / lenB - gradB().cross(Bvec) / lenB2;
}
