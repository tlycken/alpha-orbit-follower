#include "aof/xml/xml-factory.hpp"

#include "aof/numerics/interpolation.hpp"
#include "aof/physics/local-magnetic-field.hpp"
#include "aof/physics/global-magnetic-field.hpp"
#include "aof/physics/grad-shafranov-model.hpp"
#include "aof/numerics/ode-steppers.hpp"
#include "aof/numerics/ode-integrator.hpp"
#include "aof/io/output.hpp"

#include <unistd.h>

using namespace aof::interpolation;
using namespace aof::physics;
using namespace aof::ode;
using namespace aof::xml;
using namespace aof::xml::factories;
using namespace aof::io;

invalid_input_error::invalid_input_error(std::string problem)
    : std::runtime_error("Invalid AOFinput: " + problem) { }

const std::vector<double> Util::parse_grid(grid g) {
    std::vector<double> gridpoints;
    if (g.uniforms.size() == 1 && g.nonUniforms.size() == 0) {
        uniform u = g.uniforms[0];
        double dx = (u.maxValue - u.minValue) / (u.gridPoints - 1);
        double x = u.minValue;
        for (int i = 0; i < u.gridPoints; ++i) {
            gridpoints.push_back(x);
            x += dx;
        }
    } else if (g.uniforms.size() == 0 && g.nonUniforms.size() == 1) {
        parse_values(g.nonUniforms[0]).swap(gridpoints);
    } else {
        throw xml::invalid_input_error("Grid " + g.name + " is not well defined. Specify exactly one child element of type either uniform or nonUniform.");
    }
    return gridpoints;
}

AOFinput Util::parse(std::string fname)  {
    XmlParser parser;
    parser.parse(fname);
    return parser.getInput();
}

void Verify::correct_field_dimensions(const field &f, uint expected_dimensions)  {
    if (f.dimensions != expected_dimensions) {
        throw xml::invalid_input_error("Cannot create " + std::to_string(expected_dimensions) + "D interpolation of field " + f.name + " which has " + std::to_string(f.dimensions) + " dimensions.");
    }
    if (f.coordinates.size() != expected_dimensions) {
        throw xml::invalid_input_error("Cannot create " + std::to_string(expected_dimensions) + "D interpolation of field " + f.name + " which has " + std::to_string(f.coordinates.size()) + " coordinates.");
    }
}

XmlFactory::XmlFactory(std::string fname) {
    input = Util::parse(fname);
}

template<>
std::unique_ptr<IOutputBuilder> factory_impl<std::unique_ptr<IOutputBuilder>>::create(XmlFactory &f,
                                                                                      const AOFinput &input,
        const std::string &type,
const std::string &name) {
    if (type == "diagnostic") {
        Verify::exactly_one_specified(input.outputs, "output");
        diagnostic d = Util::find_named(input.outputs[0].diagnostics, name);
        return f.create<std::unique_ptr<IOutputBuilder>>(d, name);
    } else if (type == "main") {
        if (input.outputs[0].mains.size() > 0) {
            Verify::exactly_one_specified(input.outputs[0].mains, "main");
            main m = input.outputs[0].mains[0];
            return f.create<std::unique_ptr<IOutputBuilder>>(m, "main");
        } else {
            // no main output speciifed - return null output builder
            return std::unique_ptr<IOutputBuilder>(new NullOutputBuilder());
        }
    } else if (type == "summary") {
        if (input.outputs[0].summaries.size() > 0) {
            Verify::exactly_one_specified(input.outputs[0].summaries, "summary");
            summary s = input.outputs[0].summaries[0];
            return f.create<std::unique_ptr<IOutputBuilder>>(s, "summary");
        } else {
            // no summary output speciifed - return null output builder
            return std::unique_ptr<IOutputBuilder>(new NullOutputBuilder());
        }
    }
    throw std::invalid_argument("This overload only defined for types diagnostic, main and summary. You supplied type " + type);
}

AxisymmetricPolygonChamber factory_impl<AxisymmetricPolygonChamber>::create(XmlFactory __attribute__((__unused__)) &f, const AOFinput &input) {
    Verify::exactly_one_specified(input.physicalDatas, "physicalData");
    chamber c = Util::find_named(input.physicalDatas[0].chambers, "polygon");

    AxisymmetricPolygonChamber chamber;

    for (uint i = 0; i < c.vertices.size(); ++i) {
        chamber.add_vertex(c.vertices[i].R, 0.0, c.vertices[i].Z);
    }

    return chamber;
}

PlasmaVolume factory_impl<PlasmaVolume>::create(XmlFactory __attribute__((__unused__)) &f, const AOFinput &input) {
    Verify::exactly_one_specified(input.physicalDatas, "physicalDatas");
    Verify::exactly_one_specified(input.physicalDatas[0].magneticFields, "magneticField");
    Verify::exactly_one_specified(input.physicalDatas[0].magneticFields[0].topologies, "topology");

    std::vector<separatrix> seps = input.physicalDatas[0].magneticFields[0].topologies[0].separatrices;

    auto sepit = std::find_if(seps.begin(), seps.end(), [](separatrix s) -> bool {
        return s.lastClosed;
    });

    if (sepit == seps.end()) {
        throw invalid_input_error("No separatrix defined!");
    }

    separatrix sep = *sepit;

    if (sep.type == "polygon") {
        PlasmaVolume V;

        for (auto it = sep.vertices.begin(); it != sep.vertices.end(); ++it) {
            V.add_vertex(it->R, 0.0, it->Z);
        }

        return V;
    }

    throw not_implemented("There's no implementation for this type of separatrix (" + sep.type + ") yet.");
}

ParametrizedRateCoefficient factory_impl<ParametrizedRateCoefficient>::create(XmlFactory __attribute__((__unused__)) &f, const AOFinput __attribute__((__unused__)) &input, const rateCoefficient &rc) {
    Verify::exactly_one_specified(rc.parametrizations, "parametrization");
    parametrization p = rc.parametrizations[0];
    return ParametrizedRateCoefficient(p.BG, p.mc2, p.C1, p.C2, p.C3, p.C4, p.C5, p.C6, p.C7, p.Tmin, p.Tmax);
}
