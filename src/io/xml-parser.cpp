#include "aof/xml/xml-parser.hpp"

#include "tinyxml/tinyxml.h"
#include "tinybind/tinybind.h"
#include "tinybind/tinybind_xml.h"

#include "aof/xml/configure-tinybind-mappings.hpp"

#include <fstream>
#include <sstream>
#include <iostream>

using namespace aof::xml;

/**
 * @brief PIMPL wrapper around the TinyBind library
 * @details See the [TinyBind project site](http://sourceforge.net/projects/tinybind/) or [this Github version](https://github.com/tyt2y3/tinybind) for library documentation (the former is the original source, the latter has better documentation).
 */
class aof::xml::XmlParser::Impl {
private:
    TiXmlDocument doc;
    AOFinput input;

public:
    void parse(std::stringstream &xmlstream) {
        std::string contents = xmlstream.str();
        doc.Parse(contents.c_str());

        TXB_fromxmldoc(&input, &doc);
    }

    void parse(std::string fpath) {
        std::ifstream file(fpath);

        if (!file) {
            throw no_file_error(fpath);
        }

        std::stringstream ss;
        ss << file.rdbuf();

        this->parse(ss);
    }

    AOFinput getInput() {
        return input;
    }
};

/**
 * @details Instantiates the parser and the wrapped implementation.
 */
XmlParser::XmlParser() : impl(new Impl()) { }

void XmlParser::parse(std::string fpath) {
    try {
        impl->parse(fpath);
        hasdoc = true;
    } catch (no_file_error e) {
        hasdoc = false;
        throw e;
    }
}

void XmlParser::parse(std::stringstream &contents) {
    impl->parse(contents);
    hasdoc = true;
}

bool XmlParser::hasDocument() const {
    return hasdoc;
}

AOFinput XmlParser::getInput() const {
    if (!hasdoc) {
        throw no_document_error();
    }
    return impl->getInput();
}

/**
 * @brief Destructor
 * @details Cleans up the resources used by TinyBind
 */
XmlParser::~XmlParser() {
    if (impl != 0) {
        delete impl;
    }
}
