#include "aof/io/output.hpp"

#include <fstream>
#include <iostream>
#include "aof/physics/global-magnetic-field.hpp"
#include "aof/physics/local-magnetic-field.hpp"
#include "aof/numerics/interpolation.hpp"
#include <string>

using namespace aof;
using namespace io;
using namespace physics;
using namespace interpolation;

SingleFileOutputBuilder::SingleFileOutputBuilder(const std::string &name) : name(name) {
}

std::ostream &SingleFileOutputBuilder::get() {
    if (name != "stdout") {
        std::ofstream* f = new std::ofstream(name, std::ios::trunc);
        f->precision(std::numeric_limits<double>::digits10 + 2);
        if (!f->good()) {
            throw std::runtime_error("An error occurred when building stream for output file '" + name + "'");
        }
        return *f;
    }
    std::cout.precision(std::numeric_limits<double>::digits10 + 2);
    return std::cout;
}

ManyFilesOutputBuilder::ManyFilesOutputBuilder(std::string namepattern) : namepattern(namepattern), N(0) {
    if (namepattern != "stdout") {
        size_t start_pos = namepattern.find(".txt");
        if (start_pos == namepattern.length() || start_pos + 4 != namepattern.length()) {
            throw std::out_of_range("The file pattern did not end with .txt");
        }
    }
}

std::ostream &ManyFilesOutputBuilder::get() {
    if (namepattern != "stdout") {
        // String replace recipie from http://stackoverflow.com/questions/3418231/
        size_t start_pos = namepattern.find(".txt");
        std::string name = namepattern;
        name.insert(start_pos, "-" + std::to_string(N));

        ++N;
        auto f = new std::ofstream(name);
        f->precision(std::numeric_limits<double>::digits10 + 2);

        if (!f->good()) {
            throw std::runtime_error("Couldn't open " + name + " for writing. Make sure that all folders in the path exist.");
        }
        return *f;
    }
    return std::cout;
}

int NullBuffer::overflow(int c) {
    return c;
}

NullStream::NullStream() : std::ostream(&m_sb) {
}

std::ostream &NullOutputBuilder::get() {
    return stream;
}

MagneticFieldDump::MagneticFieldDump(std::ostream &out,
                                     double Rmin, double Rmax, double dR,
                                     double Zmin, double Zmax, double dZ,
                                     const IGlobalMagneticField &field)
    :   f(out),
        Rmin(Rmin), Rmax(Rmax), dR(dR),
        Zmin(Zmin), Zmax(Zmax), dZ(dZ),
        field(field) {
}

void MagneticFieldDump::print() const {
    if (!f.good()) {
        throw std::runtime_error("An error occurred when opening output file.");
    }
    f << "R" << SEPARATOR;
    f << "Z" << SEPARATOR;
    f << "psi" << SEPARATOR;
    f << "gradBdotb" << SEPARATOR;
    f << "F" << SEPARATOR;
    f << "BR" << SEPARATOR;
    f << "Bphi" << SEPARATOR;
    f << "BZ" << SEPARATOR;
    f << "B" << SEPARATOR;
    f << "gradBR" << SEPARATOR;
    f << "gradBphi" << SEPARATOR;
    f << "gradBZ";
    f << std::endl;

    f.precision(std::numeric_limits<double>::digits10 + 2);

    double phi = 0.0;

    for (double R = Rmin; R <= Rmax; R += dR) {
        for (double Z = Zmin; Z <= Zmax; Z += dZ) {
            LocalMagneticField lf(R, phi, Z, field.B(R, phi, Z), field.nabla_outerprod_B(R, phi, Z));
            f << R << SEPARATOR << Z << SEPARATOR;
            f << field.psi(R, phi, Z) << SEPARATOR;
            f << lf.b().dot(lf.gradB()) << SEPARATOR;
            f << field.F(field.psi(R, phi, Z)) << SEPARATOR;
            f << lf.B().transpose() << SEPARATOR << lf.B().length() << SEPARATOR << lf.gradB().transpose();
            f << std::endl;
        }
    }
}

Interp2DDump::Interp2DDump(std::ostream &out, double xmin, double xmax, double dx, double ymin, double ymax, double dy, const IInterp2D &itp)  : f(out), xmin(xmin), xmax(xmax), dx(dx), ymin(ymin), ymax(ymax), dy(dy), itp(itp) { }

void Interp2DDump::print() const {
    if (!f.good()) {
        throw std::runtime_error("An error occurred when opening output file for Interp2DDump.");
    }

    f << "x" << SEPARATOR;
    f << "y" << SEPARATOR;
    f << "f" << SEPARATOR;
    f << "fx" << SEPARATOR;
    f << "fy" << SEPARATOR;
    f << "fxx" << SEPARATOR;
    f << "fxy" << SEPARATOR;
    f << "fyy" << std::endl;

    f.precision(std::numeric_limits<double>::digits10 + 2);

    for (double x = xmin; x <= xmax; x += dx) {
        for (double y = ymin; y <= ymax; y += dy) {
            f << x << SEPARATOR << y << SEPARATOR;
            f << itp.evaluateAt(x, y) << SEPARATOR;
            f << itp.evaluateAt(x, y, 1, 0) << SEPARATOR;
            f << itp.evaluateAt(x, y, 0, 1) << SEPARATOR;
            f << itp.evaluateAt(x, y, 2, 0) << SEPARATOR;
            f << itp.evaluateAt(x, y, 1, 1) << SEPARATOR;
            f << itp.evaluateAt(x, y, 0, 2) << std::endl;
        }
    }
}

Interp1DDump::Interp1DDump(std::ostream &out, double xmin, double xmax, double dx, const IInterp1D &itp)
    : f(out), xmin(xmin), xmax(xmax), dx(dx), itp(itp) {
}

void Interp1DDump::print() const {
    if (!f.good()) {
        throw std::runtime_error("An error occurred when opening output file for Interp2DDump.");
    }

    f << "x" << SEPARATOR;
    f << "f" << SEPARATOR;
    f << "fx" << SEPARATOR;
    f << "fxx" << std::endl;

    f.precision(std::numeric_limits<double>::digits10 + 2);

    for (double x = xmin; x <= xmax; x += dx) {
        f << x << SEPARATOR;
        f << itp.evaluateAt(x) << SEPARATOR;
        f << itp.evaluateAt(x, 1) << SEPARATOR;
        f << itp.evaluateAt(x, 2) << std::endl;
    }
}