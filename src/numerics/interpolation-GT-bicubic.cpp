#include "aof/numerics/interpolation.hpp"
#include "Wm5IntpBicubic2.h"
#include <iostream>
#include <cstring>

namespace aof {
    namespace interpolation {

        /**
         @brief Implementation wrapper around GeometricTools' bicubic spline interpolation library.
         @details See [GeometricTools docs](http://www.geometrictools.com/LibMathematics/Interpolation/Interpolation.html) for details.
        */
        class Interp2D::Impl {
        private:
            int M, N;
            double** fdata;
            Wm5::IntpBicubic2d _spline;

            double** copydata(double** otherdata) {
                double** newdata = new double*[N];
                for (int yi = 0; yi < N; ++yi) {
                    newdata[yi] = new double[M];
                    memcpy(newdata[yi], otherdata[yi], M * sizeof(double));
                }
                return newdata;
            }
        public:
            Impl(int M, int N, double xmin, double dx, double ymin, double dy, double** f) :
                M(M),
                N(N),
                fdata(copydata(f)),
                _spline(M, N, xmin, dx, ymin, dy, fdata, true /* exact */) { }

            Impl(const Impl &o) :
                M(o.M),
                N(o.N),
                fdata(copydata(o.fdata)),
                _spline(o.M, o.N, o._spline.GetXMin(), o._spline.GetXSpacing(), o._spline.GetYMin(), o._spline.GetYSpacing(), fdata, true /* exact */) { }

            ~Impl() {
                for (int ii = 0; ii < N; ++ii) {
                    delete[] fdata[ii];
                }
                delete[] fdata;
            }

            double evaluate(double x, double y) const {
                return _spline(x, y);
            }

            double evaluate(double x, double y, int dxOrder, int dyOrder) const {
                return _spline(dxOrder, dyOrder, x, y);
            }
        };

        Interp2D::Interp2D(
            int M, int N, double xmin, double dx, double ymin, double dy,
            double** f)
            : _impl(new Impl(M, N, xmin, dx, ymin, dy, f)) { }

        Interp2D::Interp2D(int M, int N, double* x, double* y, double** f)
            : _impl(new Impl(M, N, x[0], x[1] - x[0], y[0], y[1] - y[0], f)) { }

        Interp2D::Interp2D(const Interp2D &other) : _impl(new Impl(*(other._impl))) { }

        Interp2D::~Interp2D() { }

        double Interp2D::evaluateAt(double x, double y) const {
            return _impl->evaluate(x, y);
        }

        double Interp2D::evaluateAt(double x, double y, int dxOrder, int dyOrder) const {
            return _impl->evaluate(x, y, dxOrder, dyOrder);
        }
    }
}
