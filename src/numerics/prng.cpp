
#include "aof/numerics/prng.hpp"
//#include <cstdint>
#include <fstream>
//#include <limits>
#include <stdexcept>

using namespace aof::rng;

unsigned int JKISS::x = 0;
unsigned int JKISS::y = 0;
unsigned int JKISS::z = 0;
unsigned int JKISS::c = 0;
bool JKISS::seeded = false;

unsigned int JKISS::randui32() {
    if (!seeded) {
        throw std::runtime_error("Must call JKISS::seed() before extracting random numbers!");
    }

    unsigned long long t;

    JKISS::x = 314527869 * JKISS::x + 1234567;
    JKISS::y ^= JKISS::y << 5; JKISS::y ^= JKISS::y >> 7; JKISS::y ^= JKISS::y << 22;
    t = 4294584393ULL * JKISS::z + JKISS::c; JKISS::c = t >> 32; JKISS::z = t;

    return JKISS::x + JKISS::y + JKISS::z;
}

double JKISS::randdouble() {
    return randui32() / 4294967296.0;
}

unsigned int JKISS::devrand() {
    std::ifstream urandom("/dev/urandom", std::ios::binary);

    if (urandom.good()) {
        unsigned int* r = new unsigned int;
        urandom.read((char*)r, sizeof(unsigned int));
        unsigned int i = *r;
        delete r;
        return i;
    } else {
        throw std::runtime_error("Could not open /dev/urandom to seed JKISS");
    }
}

void JKISS::randseed() {
    JKISS::x = devrand();
    while (!(JKISS::y = devrand())); // y != 0
    JKISS::z = devrand();

    JKISS::c = devrand() % 698769068 + 1; // !(z==c==0) && c < 698769069
}

void JKISS::defaultseed() {
    x = 123456789;
    y = 987654321;
    z = 43219876;
    c = 6543217;
}

void JKISS::seed() {
    randseed();
    seeded = true;
}

void MT::seed() {
    seed(false);
}

bool MT::seeded = false;
std::mt19937_64 MT::mt = std::mt19937_64(URandom::randint());

void MT::seed(bool reseed) {
    if (!seeded || reseed) {
        mt.seed(URandom::randint());
        seeded = true;
    }
}

double MT::randdouble() {
    if (!seeded) {
        throw std::runtime_error("Must call MT::seed() before extracting random numbers!");
    }
    return (mt() - mt.min()) / ((double)(mt.max() - mt.min()));
}

int URandom::randint() {
    std::ifstream urandom("/dev/urandom", std::ios::binary);

    if (urandom.good()) {
        unsigned int* r = new unsigned int;
        urandom.read((char*)r, sizeof(unsigned int));
        unsigned int i = *r;
        delete r;
        return i;
    } else {
        throw std::runtime_error("Could not open /dev/urandom to seed MT");
    }
}