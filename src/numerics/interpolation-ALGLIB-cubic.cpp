#include "aof/numerics/interpolation.hpp"
#include <interpolation.h>
#include <iostream>
#include <string>
#include <sstream>

using namespace aof::interpolation;

/**
 @brief Implementation wrapper around ALGLIB's 1-dimensional cubic spline routines
 @details See [ALGLIB docs](http://www.alglib.net/interpolation/leastsquares.php#splinefit) for details.
*/
class aof::interpolation::Interp1D::Impl {
private:
    alglib::spline1dinterpolant s;

public:
    Impl(int N, double* x, double* f) {
        // all the zeros are for parabolically terminated spline BC
        // see ALGLIB doc for details
        alglib::real_1d_array xdata;
        xdata.setcontent(N, x);
        alglib::real_1d_array fdata;
        fdata.setcontent(N, f);

        alglib::spline1dbuildcubic(xdata, fdata, N, 0, 0.0, 0, 0.0, s);
    }

    Impl(const Impl &other) : s(other.s) { }

    double evaluate(double x) {
        return alglib::spline1dcalc(s, x);
    }

    double evaluate(double x, int dOrder) {
        double result[3];
        alglib::spline1ddiff(s, x, result[0], result[1], result[2]);
        return result[dOrder];
    }
};

Interp1D::Interp1D(int N, double xmin, double dx, double* f) {
    double* x = new double[N];
    for (int ii = 0; ii < N; ++ii) {
        x[ii] = xmin + ii * dx;
    }

    _impl = std::shared_ptr<Impl>(new Impl(N, x, f));
    delete[] x;
}

Interp1D::Interp1D(int N, double* x, double* f)
    : _impl(new Impl(N, x, f)) { }

Interp1D::Interp1D(const Interp1D &other) : _impl(new Impl(*(other._impl))) { }

double Interp1D::evaluateAt(double x) const {
    return _impl->evaluate(x);
}

double Interp1D::evaluateAt(double x, int dOrder) const {
    if (dOrder > 2 || dOrder < 1) {
        if (dOrder == 0) {
            std::cerr << "WARNING: You asked for a derivative of order 0. It's more effective to just evaluate the function, using the evaluate(double x) function." << std::endl;
        }
        std::stringstream msg;
        msg << "Derivative order must be of order 0-2! (You asked for order " << dOrder << ".)";
        throw std::out_of_range(msg.str());
    }

    return _impl->evaluate(x, dOrder);
}

Interp1D::~Interp1D() { }

