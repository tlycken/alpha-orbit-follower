# - Try to find Wm5Mathematics
# Once done, this will define
# WM5MATHEMATICS_FOUND - System has libWm5Mathematics
# WM5MATHEMATICS_INCLUDE_DIRS - The Wm5Mathematics include directories
# WM5MATHEMATICS_LIBRARIES - The libraries needed to use Wm5Mathematics
# WM5MATHEMATICS_DEFINITIONS - Compiler switches required for using Wm5Mathematics

find_package(PkgConfig)
pkg_check_modules(Wm5Mathematics QUIET Wm5Mathematics)
set(Wm5Mathematics_DEFINITIONS ${WM5MATHEMATICS_CFLAGS_OTHER})

set(WM5MATHEMATICS_LIBRARY_DIR "${PROJECT_SOURCE_DIR}/deps/GeometricTools/WildMagic5/SDK/Library/Release/")

find_path(WM5MATHEMATICS_INCLUDE_DIR "Wm5IntpAkimaUniform2.h"
	HINTS "${PROJECT_SOURCE_DIR}/deps/GeometricTools/WildMagic5/SDK/Include"
)

find_library(WM5MATHEMATICS_LIBRARY NAMES Wm5Mathematics libWm5Mathematics
	HINTS ${WM5MATHEMATICS_LIBRARY_DIR})

set(WM5MATHEMATICS_LIBRARIES ${WM5MATHEMATICS_LIBRARY})
set(WM5MATHEMATICS_INCLUDES ${WM5MATHEMATICS_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Wm5Mathematics DEFAULT_MSG
	WM5MATHEMATICS_LIBRARY WM5MATHEMATICS_INCLUDE_DIR)

mark_as_advanced(WM5MATHEMATICS_INCLUDE_DIR WM5MATHEMATICS_LIBRARY)
