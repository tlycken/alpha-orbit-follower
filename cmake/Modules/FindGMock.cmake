
find_package(PkgConfig)

pkg_check_modules(PC_GMOCK QUIET GMock)

set(GMOCK_DEFINITIONS ${PC_GMOCK_CFLAGS_OTHER})

set(GMOCK_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/deps/gmock-1.7.0/include")
set(GMOCK_LIBRARY_DIR "${PROJECT_SOURCE_DIR}/deps/gmock-1.7.0/build")

find_library(GMOCK_LIBRARY NAMES gmock libgmock HINTS ${GMOCK_LIBRARY_DIR})
find_library(GMOCK_MAIN_LIBRARY NAMES gmock_main HINTS ${GMOCK_LIBRARY_DIR})

set(GMOCK_LIBRARIES ${GMOCK_LIBRARY})
set(GMOCK_INCLUDES ${GMOCK_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GMOCK DEFAULT_MSG GMOCK_LIBRARY GMOCK_INCLUDE_DIR)

mark_as_advanced(GMOCK_INCLUDE_DIR GMOCK_LIBRARY)
