# - Try to find TinyXML2
# Once done this will define
#  TINYXML2_FOUND - System has TinyXml2
#  TINYXML2_INCLUDE_DIRS - The TinyXml2 include directories
#  TINYXML2_LIBRARIES - The libraries needed to use TinyXml2
#  TINYXML2_DEFINITIONS - Compiler switches required for using TinyXml2

find_package(PkgConfig)
pkg_check_modules(PC_TINYBIND QUIET tinybind)
set(LIBTINYBIND_DEFINITIONS ${PC_LIBTINYBIND_CFLAGS_OTHER})

find_path(TINYBIND_INCLUDE_DIR "tinybind/tinybind.h"
          HINTS ${PC_TINYBIND_INCLUDEDIR} ${PC_TINYBIND_INCLUDE_DIRS} "${PROJECT_SOURCE_DIR}/deps/tinybind/include")

find_library(TINYBIND_LIBRARY libtinybind.a
            HINTS ${PC_TINYBIND_LIBDIR} ${PC_TINYXML_LIBRARY_DIRS}
                "${PROJECT_SOURCE_DIR}/deps/tinybind/lib")

find_package(TinyXML REQUIRED)

set(TINYBIND_LIBRARIES ${TINYBIND_LIBRARY} ${TINYXML_LIBRARIES})
set(TINYBIND_INCLUDES ${TINYBIND_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBTINYBIND_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(TinyBind  DEFAULT_MSG
                                  TINYBIND_LIBRARY TINYBIND_INCLUDE_DIR)

mark_as_advanced(TINYBIND_INCLUDE_DIR TINYBIND_LIBRARY)
