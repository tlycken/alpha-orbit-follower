
if (${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
	message(FATAL_ERROR "This project does not support in-source builds. Please build from a separate build-tree, e.g. by running mkdir build && cd build from the source root, and then running cmake [options] .. from there.")
endif()