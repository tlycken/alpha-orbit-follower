macro(add_ext_library lib) 
    string(REGEX REPLACE "^ +| +$" "" program_EXTERNAL_LIBS "${program_EXTERNAL_LIBS} ${lib}")
    set(program_EXTERNAL_LIBS ${program_EXTERNAL_LIBS} PARENT_SCOPE)
endmacro(add_ext_library)
