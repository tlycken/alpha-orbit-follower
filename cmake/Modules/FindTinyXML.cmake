# - Try to find TinyXML2
# Once done this will define
#  TINYXML2_FOUND - System has TinyXml2
#  TINYXML2_INCLUDE_DIRS - The TinyXml2 include directories
#  TINYXML2_LIBRARIES - The libraries needed to use TinyXml2
#  TINYXML2_DEFINITIONS - Compiler switches required for using TinyXml2

find_package(PkgConfig)
pkg_check_modules(PC_TINYXML QUIET tinyxml)
set(LIBXML_DEFINITIONS ${PC_LIBXML_CFLAGS_OTHER})

find_path(TINYXML_INCLUDE_DIR "tinyxml/tinyxml.h"
          HINTS ${PC_TINYXML_INCLUDEDIR} ${PC_TINYXML_INCLUDE_DIRS} "${PROJECT_SOURCE_DIR}/deps/tinybind/tinyxml/include")

find_library(TINYXML_LIBRARY NAMES tinyxml libtinyxml
             HINTS ${PC_TINYXML_LIBDIR} ${PC_TINYXML_LIBRARY_DIRS} "${PROJECT_SOURCE_DIR}/deps/tinybind/tinyxml/lib")

set(TINYXML_LIBRARIES ${TINYXML_LIBRARY})
set(TINYXML_INCLUDES ${TINYXML_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBXML_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(TinyXml  DEFAULT_MSG
                                  TINYXML_LIBRARY TINYXML_INCLUDE_DIR)

mark_as_advanced(TINYXML_INCLUDE_DIR TINYXML_LIBRARY)
