
find_package(PkgConfig)

pkg_check_modules(PC_GTEST QUIET GTest)
set(GTEST_DEFINITIONS ${PC_GTEST_CFLAGS_OTHER})

set(GTEST_LIBRARY_DIR "${PROJECT_SOURCE_DIR}/deps/gmock-1.7.0/gtest/build")
set(GTEST_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/deps/gmock-1.7.0/gtest/include")

find_library(GTEST_LIBRARY NAMES gtest libgtest HINTS ${GTEST_LIBRARY_DIR})
find_library(GTEST_MAIN_LIBRARY NAMES gtest_main HINTS ${GTEST_LIBRARY_DIR})

set(GTEST_LIBRARIES ${GTEST_LIBRARY} pthread)
set(GTEST_INCLUDES ${GTEST_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GTest DEFAULT_MSG 
                                  GTEST_LIBRARY GTEST_INCLUDE_DIR)

mark_as_advanced(GTEST_INCLUDE_DIR GTEST_LIBRARY)
