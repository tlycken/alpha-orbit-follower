#ifndef INTERPOLATION_HPP
#define INTERPOLATION_HPP

#include <memory>

namespace aof {
    namespace interpolation {

        /**
         @brief Interface for 1D interpolation classes
         @details All 1D interpolation implementations should inherit
         from this class, and implement all its members. By doing so, they
         can be used by other parts of the program using dependency injection.

         @author Tomas Lycken
        */
        class IInterp1D {
        public:
            /**
             @brief Evaluate the interpolated function value

             @param x The coordinate \f$x\f$ at which to evaluate
             @return The interpolated function value \f$f(x)\f$
            */
            virtual double evaluateAt(double x) const = 0;

            /**
             @brief Evaluate interpolated function derivative value
             @details All implementations should support at least derivatives of the first and second orders.

             @param x The coordinate at which to evaluate
             @param dOrder The order of differentiation. For `dOrder == 0` this should return the same value as `evaluateAt(x)`, while for higher values of `dOrder` the corresponding derivative should be returned.

             @return The interpolated value of the derivative of appropriate order
            */
            virtual double evaluateAt(double x, int dOrder) const = 0;

            /**
             @brief Virtual destructor
             @details Only defined here so that inheriting classes can clean up after themselves.
            */
            virtual ~IInterp1D() { };
        };

        /**
         @brief Implementation wrapper for 1D interpolation libraries
         @details This class declaration allows for wrapping an external interpolation library to provide 1D interpolation functionality using the [PIMPL pattern](http://en.wikipedia.org/wiki/Pimpl).

         @author Tomas Lycken
        */
        class Interp1D : public IInterp1D {
        public:
            /**
             @brief Construct 1D interpolation object from data
             @details The constructor should handle all necessary initialization of the interpolant, so that after constructing an instance of `Interp1D` one can directly call `evaluateAt()` to obtain interpolated values.

             The constructor takes two data sets `x` and `f` of equal size `N`, that specify the points through which the interpolated curve should pass. `x` is uniform, and \f$f_i\f$ should correspond to \f$f(x_i)\f$

             @param N The number of data points in the discrete data.
             @param xmin The first point in `x`
             @param dx The distance between poins in `x`
             @param f The function values on the data grid
            */
            Interp1D(int N, double xmin, double dx, double* f);
            /**
             @brief Construct 1D interpolation object from data
             @details The constructor should handle all necessary initialization of the interpolant, so that after constructing an instance of `Interp1D` one can directly call `evaluateAt()` to obtain interpolated values.

             The constructor takes two data sets `x` and `f` of equal size `N`, that specify the points through which the interpolated curve should pass. `x` is uniform, and \f$f_i\f$ should correspond to \f$f(x_i)\f$

             @param N The number of data points in the discrete data.
             @param x The values of \f$x\f$ on the data grid
             @param f The function values on the data grid
            */
            Interp1D(int N, double* x, double* f);
            /**
             @brief Copy constructor
             @param other interpolation object to copy
            */
            Interp1D(const Interp1D &other);
            /**
             @brief Destructor
            */
            ~Interp1D();

            double evaluateAt(double) const;
            double evaluateAt(double, int) const;
        private:
            /**
             @brief Implementation wrapper class
             @details The implementation wrapper that this class uses in order to hide the external library references from the header file. Each wrapper implementation is responsible for defining the actual contents of the `Impl` class.
             */
            class Impl;
            std::shared_ptr<Impl> _impl;
        };

        /**
         @brief Interface for 2D interpolation classes
         @details All 2D interpolation implementations should inherit from this class, and implement all its members. By doing so, they can be used by other parts of the program using dependency injection.

         @author Tomas Lycken
        */
        class IInterp2D {
        public:
            /**
             @brief Evaluate the interpolated function value

             @param x The \f$x\f$-coordinate at which to evaluate
             @param y The \f$y\f$-coordinate at which to evaluate
             @return The interpolated function value \f$f(x,y)\f$
            */
            virtual double evaluateAt(double x, double y) const = 0;
            /**
             @brief Evaluate interpolated function derivative value
             @details All implementations should support at least derivatives of the first and second orders in all directions, including cross terms.

             @param x The \f$x\f$-coordinate at which to evaluate
             @param y The \f$y\f$-coordinate at which to evaluate
             @param dxOrder The order of differentiation in the \f$x\f$-direction.
             @param dyOrder The order of differentiation in the \f$y\f$-direction.

             @return The interpolated value of the derivative of appropriate order. If `dxOrder == dyOrder == 0`, the value should be the same as the function value.
            */
            virtual double evaluateAt(double x, double y, int dxOrder, int dyOrder) const = 0;

            /**
             @brief Virtual destructor
             @details Provided to allow implementations to clean up after themselves
            */
            virtual ~IInterp2D() { };
        };

        /**
         @brief Implementation wrapper for 2D interpolation libraries
         @details This class declaration allows for wrapping an external interpolation library to provide 1D interpolation functionality using the [PIMPL pattern](http://en.wikipedia.org/wiki/Pimpl).

         @author Tomas Lycken
        */
        class Interp2D : public IInterp2D {
        private:
            class Impl;
            std::shared_ptr<Impl> _impl;

        public:
            /**
             @brief Initializes interpolation object, and calculates interpolation coefficients
             @details The constructor should handle all necessary initialization of the interpolant, so that after constructing an instance of `Interp2D` one can directly call `evaluateAt()` to obtain interpolated values.

             The constructor takes three data sets \f$x_i\f$, \f$y_j\f$ and \f$f_{ij}\f$ such that \f$f_{ij} = f(x_i, y_j)\f$, that defines the points in three-dimensional space that the interpolated surface should intersect.

             Note that the data grid does not have to be square, but it must be uniform in both directions independently.

             @param M The numer of points in the \f$x\f$-direction
             @param N The numer of points in the \f$y\f$-direction
             @param xmin The location of the first point in the \f$x\f$-direction
             @param dx The distance between points in the \f$x\f$-direction
             @param ymin The location of the first point in the \f$y\f$-direction
             @param dy The distance between points in the \f$y\f$-direction
             @param f The function values at the points \f$(x_i,y_j)\f$. The data is assumed to be organized such that \f$f(x_i,y_j)\f$ can be obtained as `f[j][i]`.
            */
            Interp2D(int M, int N, double xmin, double dx, double ymin, double dy, double** f);
            /**
             @brief Initializes interpolation object, and calculates interpolation coefficients
             @details The constructor should handle all necessary initialization of the interpolant, so that after constructing an instance of `Interp2D` one can directly call `evaluateAt()` to obtain interpolated values.

             The constructor takes three data sets \f$x_i\f$, \f$y_j\f$ and \f$f_{ij}\f$ such that \f$f_{ij} = f(x_i, y_j)\f$, that defines the points in three-dimensional space that the interpolated surface should intersect.

             Note that the data grid does not have to be square, but it must be uniform in both directions independently.

             @param M The numer of points in the \f$x\f$-direction
             @param N The numer of points in the \f$y\f$-direction
             @param x The data grid in the \f$x\f$-direction
             @param y The data grid in the \f$y\f$-direction
             @param f The function values at the points \f$(x_i,y_j)\f$. The data is assumed to be organized such that \f$f(x_i,y_j)\f$ can be obtained as `f[j][i]`.
            */
            Interp2D(int M, int N, double* x, double* y, double** f);
            /**
             @brief Copy constructor
            */
            Interp2D(const Interp2D &other);
            /**
             @brief Destructor
            */
            ~Interp2D();

            double evaluateAt(double, double) const;
            double evaluateAt(double, double, int, int) const;
        };

    }
}

#endif
