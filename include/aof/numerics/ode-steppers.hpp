#ifndef ODE_STEPPERS_HPP
#define ODE_STEPPERS_HPP

#include "aof/algebra/vector-algebra.hpp"
#include "aof/physics/rhs-evaluator.hpp"
#include "aof/io/output.hpp"
#include <boost/numeric/odeint/stepper/runge_kutta_dopri5.hpp>
#include <boost/numeric/odeint/algebra/vector_space_algebra.hpp>

namespace aof {
    namespace ode {
        using namespace physics;

        template<typename TRHSEvaluator>
        class IStepper {
        protected:
            double h;

            IStepper(TRHSEvaluator rhs_evaluator, double step_size) : h(step_size), rhs(rhs_evaluator) { }

        public:
            typedef TRHSEvaluator rhs_t;
            typedef typename TRHSEvaluator::state_t state_t;
            const TRHSEvaluator rhs;

            virtual solpoint<state_t> next(double t, const state_t &X) = 0;
            virtual solpoint<state_t> first(const state_t X0) const {
                auto rhs0 = rhs(0.0, X0);
                return solpoint<state_t> {0.0, X0, rhs0};
            }
            virtual ~IStepper() { };
        };

        template<typename TRHSEvaluator>
        class RK4Stepper : public IStepper<TRHSEvaluator> {
        public:
            typedef TRHSEvaluator rhs_t;
            typedef typename TRHSEvaluator::state_t state_t;

            RK4Stepper(const TRHSEvaluator rhs_evaluator, double step_size) : IStepper<TRHSEvaluator>(rhs_evaluator, step_size) { }

            solpoint<state_t> next(double t, const state_t &X) {
                auto k1 = this->rhs(t, X);
                auto k2 = this->rhs(t + .5 * this->h, X + .5 * this->h * k1);
                auto k3 = this->rhs(t + .5 * this->h, X + .5 * this->h * k2);
                auto k4 = this->rhs(t + this->h, X + this->h * k3);

                auto xout = X + this->h * ((k1 + k4) / 6. + (k2 + k3) / 3.);

                return solpoint<state_t> {t + this->h, xout, this->rhs(t + this->h, xout)};
            };
        };

        template<typename TRHSEvaluator>
        class Dopri5Stepper : public IStepper<TRHSEvaluator> {
        public:
            typedef TRHSEvaluator rhs_t;
            typedef typename TRHSEvaluator::state_t state_t;
        private:
            boost::numeric::odeint::runge_kutta_dopri5<state_t, double, state_t, double, boost::numeric::odeint::vector_space_algebra> rk_stepper;

        public:
            Dopri5Stepper(const TRHSEvaluator rhs_evaluator, double step_size) : IStepper<TRHSEvaluator>(rhs_evaluator, step_size) { }

            solpoint<state_t> next(double t, const state_t &X) {
                state_t xout;

                rk_stepper.do_step([this](const state_t &x, state_t &dxdt, const double t) -> void {
                    dxdt = this->rhs(t, x);
                }, X, t, xout, this->h);

                return solpoint<state_t> {t + this->h, xout, this->rhs(t + this->h, xout)};
            }
        };
    }
}

#endif
