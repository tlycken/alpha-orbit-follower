#ifndef ODE_INTEGRATOR_HPP
#define ODE_INTEGRATOR_HPP

#include "aof/physics/rhs-evaluator.hpp"
#include "aof/physics/final-condition.hpp"
#include <chrono>

#include "aof/io/output.hpp"

namespace aof {
    namespace ode {

        template<typename TStepper, typename TFinalConditionBuilder>
        class Integrator {
        public:
            typedef typename TStepper::state_t state_t;
            typedef TStepper stepper_t;
            typedef TFinalConditionBuilder fcbuilder_t;

            Integrator(TStepper odein, TFinalConditionBuilder fcbin) : ode(odein), fcb(fcbin) { }

            double integrate(state_t ic) {
                solpoints.clear();
                solpoint<state_t> current = ode.first(ic);
                solpoint<state_t> prev = current;
                auto fc = fcb.build(ic, current.rhs);

                std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();

                // Save first point if applicable
                // trace_state s = fc.current_state(prev, current, end);
                // if (s.save) {
                solpoints.push_back(current);
                // }
                trace_state s;
                do {
                    prev = current;
                    current = ode.next(current.t, current.X);
                    s = fc.current_state(prev, current, end);
                    // if (s.save) {
                    solpoints.push_back(current);
                    // }
                } while (!s.end);

                std::chrono::high_resolution_clock::time_point finish = std::chrono::high_resolution_clock::now();

                std::chrono::duration<double> calc_time = std::chrono::duration_cast<std::chrono::duration<double>>(finish - start);

                return calc_time.count();
            }

            std::vector<physics::solpoint<state_t>> trace() {
                return solpoints;
            }

            aof::physics::final_state<state_t> endState() {
                return end;
            };


        private:
            TStepper ode;
            TFinalConditionBuilder fcb;
            aof::physics::final_state<state_t> end;
            std::vector<solpoint<state_t>> solpoints;
        };

    }
}

#endif
