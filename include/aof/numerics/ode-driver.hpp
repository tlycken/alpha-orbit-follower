#ifndef ODE_DRIVER_HPP
#define ODE_DRIVER_HPP

#include <chrono>
#include <iostream>
#include <memory>

#include "aof/io/output.hpp"
#include "aof/physics/guiding-center-rhs.hpp"

namespace aof {
    namespace ode {

        using namespace io;

        struct timings {
            double traceTime;
            double icTime;
        };

        template<typename TIntegrator, typename TInitialCondition>
        class Driver {
        private:
            TIntegrator integrator;
            TInitialCondition ic;
            std::unique_ptr<IOutputBuilder> mainbuilder;
            std::unique_ptr<IOutputBuilder> summarybuilder;

            void writeHeader(std::ostream &summary) {
                summary << "Index" << SEPARATOR;
                summary << "Weight" << SEPARATOR;

                summary << "InitialR" << SEPARATOR;
                // summary << "InitialPhi" << SEPARATOR;
                summary << "InitialZ" << SEPARATOR;
                // summary << "InitialVParallel" << SEPARATOR;
                // summary << "InitialMu" << SEPARATOR;
                // summary << "InitialPhase" << SEPARATOR;

                summary << "SecondToFinalR" << SEPARATOR;
                // summary << "SecondToFinalPhi" << SEPARATOR;
                summary << "SecondToFinalZ" << SEPARATOR;
                // summary << "SecondToFinalVParallel" << SEPARATOR;
                // summary << "SecondToFinalMu" << SEPARATOR;
                // summary << "SecondToFinalPhase" << SEPARATOR;

                summary << "FinalR" << SEPARATOR;
                // summary << "FinalPhi" << SEPARATOR;
                summary << "FinalZ" << SEPARATOR;
                // summary << "FinalVParallel" << SEPARATOR;
                // summary << "FinalMu" << SEPARATOR;
                // summary << "FinalPhase" << SEPARATOR;

                summary << "Reason" << std::endl;
            }

        public:
            typedef TIntegrator integrator_t;
            typedef TInitialCondition ic_t;

            Driver(TIntegrator integratorin,
                   TInitialCondition icin,
                   std::unique_ptr<IOutputBuilder> mainbuilderin,
                   std::unique_ptr<IOutputBuilder> summarybuilderin)
                : integrator(integratorin),
                  ic(icin),
                  mainbuilder(std::move(mainbuilderin)),
                  summarybuilder(std::move(summarybuilderin)) { }

            timings integrateAll() {
                timings timing {.0, 0.0};

                std::ostream &summary = summarybuilder->get();

                writeHeader(summary);

                for (int i = 0; i < ic.totalCount(); ++i) {

                    std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();

                    auto s = ic.next();

                    std::chrono::high_resolution_clock::time_point finish = std::chrono::high_resolution_clock::now();

                    timing.icTime += std::chrono::duration_cast<std::chrono::duration<double>>(finish - start).count();

                    timing.traceTime += integrator.integrate(s.s);

                    auto trace = integrator.trace();
                    std::ostream &main = mainbuilder->get();

                    header<typename TIntegrator::state_t>(main) << std::endl;
                    for (unsigned int itrace = 0; itrace < trace.size(); ++itrace) {
                        main << trace[itrace].t << SEPARATOR;
                        main << trace[itrace].X(0) << SEPARATOR;
                        main << trace[itrace].X(2) << SEPARATOR;
                        main << trace[itrace].X(3) << SEPARATOR;
                        // main << trace[itrace].X(4) << SEPARATOR;
                        main << trace[itrace].rhs(3) << std::endl;

                        // main << trace[itrace].X.transpose() << aof::SEPARATOR;
                        // main << trace[itrace].rhs.transpose() << std::endl;
                    }

                    if (main != std::cout && &main != 0) {
                        delete &main;
                    }

                    summary << i << SEPARATOR
                            << s.w << SEPARATOR
                            << trace[0].X(0) << SEPARATOR
                            << trace[0].X(2) << SEPARATOR
                            << trace[trace.size() - 2].X(0) << SEPARATOR
                            << trace[trace.size() - 2].X(2) << SEPARATOR
                            << trace[trace.size() - 1].X(0) << SEPARATOR
                            << trace[trace.size() - 1].X(2) << SEPARATOR
                            << integrator.endState().how << std::endl;

                    if (i % 10 == 0) {
                        std::cout << '\r' << (100 * i) / ic.totalCount() << "%";
                        std::cout.flush();
                    }

                }


                return timing;
            }
        };
    }
}

#endif
