#ifndef PRNG_HPP
#define PRNG_HPP

#include <cstdint>
#include <random>

namespace aof {
    namespace rng {

        class JKISS {
        private:
            // Seed variables
            static unsigned int x;
            static unsigned int y;
            static unsigned int z;
            static unsigned int c;

            static bool seeded;

            static void randseed();
            static void defaultseed();

        public:
            static unsigned int randui32();
            static double randdouble();
            static unsigned int devrand();
            static void seed();
        };

        class MT {
        private:
            static std::mt19937_64 mt;
            static bool seeded;
        public:
            static double randdouble();
            static void seed();
            static void seed(bool reseed);
        };

        class URandom {
        public:
            static int randint();
        };
    }
}

#endif
