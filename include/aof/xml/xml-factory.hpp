#ifndef XML_FACTORY_HPP
#define XML_FACTORY_HPP

#include "aof/aof-commons.hpp"
#include "aof/xml/xml-structs.hpp"
#include "aof/xml/xml-parser.hpp"

#include "aof/physics/grad-shafranov-model.hpp"
#include "aof/numerics/interpolation.hpp"
#include "aof/physics/flux-function-interpolation.hpp"
#include "aof/physics/guiding-center-rhs.hpp"
#include "aof/physics/initial-condition.hpp"
#include "aof/physics/guiding-center-initial-condition.hpp"
#include "aof/physics/guiding-center-final-condition.hpp"
#include "aof/numerics/ode-steppers.hpp"
#include "aof/numerics/ode-integrator.hpp"
#include "aof/numerics/ode-driver.hpp"
#include "aof/physics/plasma.hpp"
#include "aof/physics/chamber.hpp"

#include "aof/io/output.hpp"

#include <iterator>
#include <algorithm>

namespace aof {
    namespace xml {

        using namespace aof::xml::structs;
        using namespace aof::io;

        /**
         * @brief Exception thrown when an xml element contains invalid data
         *
         * @author Tomas Lycken
         */
        class invalid_input_error : public std::runtime_error {
        public:
            invalid_input_error(std::string);
        };


        /**
         * @brief XML parsing utilities
         * @details Contains a few utility methods used when parsing the xml data and building domain representations.
         *
         * @author Tomas Lycken
         */
        class Util {
        public:
            /**
             * @brief Parse a `<values>` element
             * @details Finds a `<values>` element on the provided struct, and parses its contents to a `std::vector<double>`.
             *
             * @param v The struct which contains the `<values>` element
             * @return A `std::vector<double>` containing the parsed values
             *
             * @throws invalid_input_error if the struct does not have exactly one `<values>` element
             */
            template<typename T>
            static std::vector<double> parse_values(const T &v) {
                if (v.valueList.size() != 1) {
                    throw xml::invalid_input_error("Invalid values specification");
                }
                std::string raw_data = v.valueList[0].data;
                std::stringstream iss(raw_data);

                std::vector<double> data;

                std::transform(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), std::back_inserter(data), [](std::string s) -> double { return atof(s.c_str()); });

                return data;
            }

            /**
             * @brief Finds a named element in a collection
             * @details Uses standard library functionality to find, in a given collection, an element with a property `name` that has a given value.
             *
             * @param coll The collection to search in
             * @param name The name to search for
             * @tparam T Type of the elements in the colletion
             * @return The matching element
             * @throws invalid_input_error if no element with the given name was found
             */
            template<typename T>
            static const T &find_named(const std::vector<T> &coll, const std::string &name) {
                auto t_it = std::find_if(coll.begin(), coll.end(), [name](T t) -> bool { return t.name == name; });
                if (t_it == coll.end()) {
                    throw xml::invalid_input_error(std::string(typeid(T).name()) + " with name " + name + " could not be found.");
                }
                return *t_it;
            }

            template<typename T>
            static bool has_named(const std::vector<T> &coll, const std::string &name) {
                auto t_it = std::find_if(coll.begin(), coll.end(), [name](T t) -> bool { return t.name == name; });
                return t_it != coll.end();
            }

            /**
             * @brief Utility function to parse a `<grid>` element
             * @details The function verifies that exactly one element of type either `<uniform>` or `<nonUniform>` was provided, and constructs a `std::vector<double>` containing all the grid points.
             *
             * @param g The grid to parse
             * @return A `std::vector<double>` with the grid points
             * @throws invalid_input_error if the grid is not well-defined
             */
            static const std::vector<double> parse_grid(grid g);

            /**
             * @brief Static wrapper function around the XML Parser functionalities
             * @details Instantiates a parser, parses the file and returns an AOFinput representing the file contents
             *
             * @param fname relative path to the XML data file
             * @return an AOFinput with the file contents
             */
            static AOFinput parse(std::string fname);
        };


        class Verify {
        public:
            template<typename T>
            static void exactly_one_specified(std::vector<T> v, std::string name) {
                if (v.size() > 1) {
                    throw xml::invalid_input_error("More than one <" + name + "> node present.");
                } else if (v.size() == 0) {
                    throw xml::invalid_input_error("No <" + name + "> node present.");
                }
            }

            template<typename T>
            static void has_named(std::vector<T> &coll, std::string name) {
                if (!Util::has_named(coll, name)) {
                    throw xml::invalid_input_error("No element named " + name + " specified.");
                }
            }

            static void correct_field_dimensions(const field &f, unsigned int expected_dimensions);
        };

        namespace factories {

            using namespace physics;
            using namespace interpolation;
            using namespace ode;

            template<typename T>
            struct factory_impl;

            class XmlFactory {
            private:
                AOFinput input;

            public:
                XmlFactory(std::string fname);

                /**
                 * @brief Template factory method for all types that will be instantiated
                 * based on XML input.
                 * @details This method calls a specialized template for `factory_impl<T>::create()` and forwards all arguments passed to it. This requires a template specialization of `factory_impl<T>` with the appropriate `create()` method specified somewhere before the call.
                 *
                 * For example, in order to be able to do
                 *
                 *     XmlFactory f = getFactory();
                 *     auto itp1d = f.create<Interp1D>("F");
                 *
                 * you must first create a template specialization
                 *
                 *     template<>
                 *     struct factory_impl<Interp1D> {
                 *         Interp1D create(AOPInput input, std::string name) {
                 *             // class specific code here, creates instance named itp
                 *             return itp;
                 *         }
                 *     }
                 *
                 * For a detailed explanation of this pattern, see http://stackoverflow.com/q/22966239/38055 and the accepted answer
                 *
                 * @tparam T The type to build
                 * @tparam Args Types of arguments that will be passed to the implementation method.
                 * @return An instance of `T`
                 */
                template<typename T, typename... Args>
                T create(Args &&... args) {
                    return factory_impl<T>::create(*this, input, std::forward<Args>(args)...);
                }
            };

            /**
             * @brief Factory for GradShafranovModel
             * @details  Looks for a magnetic field representation with its `name` property set to `"GradShafranov"`, fetches the interpolation objects required from fields named `"F"` and `"psi"`, and instantiates the global field model.
             *
             * @throws invalid_input_error if the builder was initialized with an \ref xml::structs::AOFinput structure which does not have the required data.
             * @return an instantiated `GradShafranovModel<Interp2D, Interp1D>`
             */
            template<typename TInterp2D, typename TInterp1D, int COCOS>
            struct factory_impl<GradShafranovModel<TInterp2D, TInterp1D, COCOS>> {
                static GradShafranovModel<TInterp2D, TInterp1D, COCOS> create(XmlFactory &f, const AOFinput &input, bool dumpDiagnostics) {
                    Verify::exactly_one_specified(input.physicalDatas, "physicalData");
                    Verify::exactly_one_specified(input.physicalDatas[0].magneticFields, "magneticField");

                    representation gs = xml::Util::find_named<representation>(input.physicalDatas[0].magneticFields[0].representations, "GradShafranov");

                    auto F = f.create<TInterp1D>(gs, "F");
                    auto psi = f.create<TInterp2D>(gs, "psi");

                    auto bfield = GradShafranovModel<TInterp2D, TInterp1D, COCOS>(psi, F);

                    if (dumpDiagnostics && input.outputs.size() > 0) {
                        Verify::exactly_one_specified(input.outputs, "output");
                        if (Util::has_named(input.outputs[0].diagnostics, "grad-shafranov-fields")) {
                            diagnostic fd = Util::find_named(input.outputs[0].diagnostics, "grad-shafranov-fields");
                            auto outbuilder = f.create<std::unique_ptr<IOutputBuilder>>(fd, fd.name);

                            // <GradShafranovModel<TInterp2D, TInterp1D, COCOS>, LocalMagneticField>
                            MagneticFieldDump fieldDump(outbuilder->get(),
                                                        fd.Rmin, fd.Rmax, fd.dR,
                                                        fd.Zmin, fd.Zmax, fd.dZ,
                                                        bfield);
                            std::cout << "Dumping magnetic fields...";
                            fieldDump.print();
                        }
                        if (Util::has_named(input.outputs[0].diagnostics, "psi")) {
                            diagnostic psid = Util::find_named(input.outputs[0].diagnostics, "psi");
                            auto outbuilder = f.create<std::unique_ptr<IOutputBuilder>>(psid, psid.name);

                            // <TInterp2D>
                            Interp2DDump psidump(outbuilder->get(), psid.Rmin, psid.Rmax, psid.dR, psid.Zmin, psid.Zmax, psid.dZ, psi);

                            std::cout << "Dumping psi...";
                            psidump.print();
                        }
                        if (Util::has_named(input.outputs[0].diagnostics, "F")) {
                            diagnostic Fd = Util::find_named(input.outputs[0].diagnostics, "F");
                            auto outbuilder = f.create<std::unique_ptr<IOutputBuilder>>(Fd, Fd.name);

                            Interp1DDump Fdump(outbuilder->get(), Fd.psimin, Fd.psimax, Fd.dpsi, F);
                            std::cout << "Dumping F...";
                            Fdump.print();
                        }
                    }

                    return bfield;
                }
            };

            template<>
            struct factory_impl<Interp1D> {
                /**
                 * @brief Build 1D interpolation object
                 * @details Builds a 1-dimensional interpolation object of type TInterp1D for the specified field.
                 *
                 * @param q The struct that contains the field raw data
                 * @param fieldname The name of the field
                 * @tparam TInterp1D The type of the interpolation object. Must inherit from IInterp1D
                 * @return a TInterp1D instance corresponding to the provided field
                 * @throws invalid_input_error if the number of grid points and the number of data points don't mach
                 */
                template<typename T>
                static Interp1D create(XmlFactory __attribute__((__unused__)) &f, const AOFinput __attribute__((__unused__)) &input, const T &model, std::string name) {
                    // Find the named field in the provided model
                    field fld = Util::find_named(model.fields, name);

                    // Assert that the given field has 1 dimension
                    Verify::correct_field_dimensions(fld, 1);

                    // Find grid corresponding to field coordinate
                    coordinate c = fld.coordinates[0]; // safe - asserting size() == 1 above
                    grid g = Util::find_named(model.grids, c.name);

                    // Parse gridpoints and data values
                    std::vector<double> gridpoints = Util::parse_grid(g);
                    std::vector<double> datapoints = Util::parse_values(fld);

                    // Sanity check: require the same number of grid points as data points
                    if (gridpoints.size() != datapoints.size()) {
                        throw xml::invalid_input_error("The number of grid points (" + std::to_string(gridpoints.size()) + ") and the number of data points (" + std::to_string(datapoints.size()) + ") for field " + name + " don't match.");
                    }

                    // Input seems good - instantiate and return interpolation object
                    return Interp1D(gridpoints.size(), gridpoints.data(), datapoints.data());
                }
            };

            template<>
            struct factory_impl<FluxFunctionInterpolation<Interp1D, 13>> {
                template<typename T>
                static FluxFunctionInterpolation<Interp1D, 13> create(XmlFactory &f, const AOFinput __attribute__((__unused__)) &input, const T &model, std::string name) {
                    auto itp = f.create<Interp1D>(model, name);

                    // Find the named field in the provided model
                    field fld = Util::find_named(model.fields, name);

                    // Assert that the given field has 1 dimension
                    Verify::correct_field_dimensions(fld, 1);

                    // Find grid corresponding to field coordinate
                    coordinate c = fld.coordinates[0]; // safe - asserting size() == 1 above
                    grid g = Util::find_named(model.grids, c.name);

                    std::vector<double> gridpoints = Util::parse_grid(g);
                    std::vector<double> datapoints = Util::parse_values(fld);

                    // Sanity check: require the same number of grid points as data points
                    if (gridpoints.size() != datapoints.size()) {
                        throw xml::invalid_input_error("The number of grid points (" + std::to_string(gridpoints.size()) + ") and the number of data points (" + std::to_string(datapoints.size()) + ") for field " + name + " don't match.");
                    }

                    return FluxFunctionInterpolation<Interp1D, 13>(gridpoints.size(), gridpoints.data(), datapoints.data());
                }
            };

            template<>
            struct factory_impl<Interp2D> {
                /**
                 * @brief Build 2D interpolation object
                 * @details Builds a 2-dimensional interpolation object of type TInterp2D for the specified field.
                 *
                 * @param q The struct that contains the field raw data
                 * @param fieldname The name of the field
                 * @tparam TInterp2D The type of the interpolation object. Must inherit from IInterp2D
                 * @return a TInterp2D instance corresponding to the provided field
                 * @throws invalid_input_error if the number of grid points and the number of data points don't mach
                 */
                template<typename TModel>
                static Interp2D create(XmlFactory __attribute__((__unused__)) &f, const AOFinput __attribute__((__unused__)) &input, const TModel &model, std::string name) {
                    // Find the correct field in the provided model
                    field fld = Util::find_named(model.fields, name);
                    // Assert that the given field has 2 dimensions
                    Verify::correct_field_dimensions(fld, 2);

                    // Get grids corresponding to field coordinates
                    // These access statements are safe, since we assert size() == 2 above
                    coordinate c1 = fld.coordinates[0];
                    coordinate c2 = fld.coordinates[1];

                    grid g1 = Util::find_named(model.grids, c1.name);
                    grid g2 = Util::find_named(model.grids, c2.name);

                    // Parse grid points and data
                    std::vector<double> gridpoints1 = Util::parse_grid(g1);
                    std::vector<double> gridpoints2 = Util::parse_grid(g2);
                    std::vector<double> datapoints = Util::parse_values(fld);

                    // Sanity check: number of data points = number of grid points
                    const int M = gridpoints1.size();
                    const int N = gridpoints2.size();
                    const int MN = datapoints.size();

                    if (M * N != MN) {
                        throw xml::invalid_input_error("The number of grid points (" + std::to_string(M) + "x" + std::to_string(N) + ") does not match the number of data points (" + std::to_string(MN) + ") for field " + name);
                    }

                    // Reinterpret the 1D-vector datapoints as a 2D array of data
                    // See http://stackoverflow.com/a/22352556/38055 for an excellent
                    // explanation of how this works.
                    std::vector<double*> fps;
                    fps.reserve(N);
                    for (int i = 0; i < N; ++i) {
                        fps.push_back(&datapoints[i * M]);
                    }

                    double** f2d = fps.data();

                    // Instantiate and return interpolation object
                    return Interp2D(M, N, gridpoints1.data(), gridpoints2.data(), f2d);
                }
            };

            template<typename TRHS>
            struct factory_impl<RK4Stepper<TRHS>> {
                static RK4Stepper<TRHS> create(XmlFactory __attribute__((__unused__)) &f, const AOFinput &input, particle &p, std::string ictype, typename TRHS::globalfield_t bfield) {
                    Verify::exactly_one_specified(input.physicalDatas, "physicalData");
                    Verify::exactly_one_specified(input.physicalDatas[0].magneticFields, "magneticField");
                    Verify::exactly_one_specified(input.physicalDatas[0].magneticFields[0].topologies, "topology");

                    Opoint magneticAxis = input.physicalDatas[0].magneticFields[0].topologies[0].mainAxis();

                    Verify::exactly_one_specified(input.numericalDatas, "numericalData");
                    Verify::exactly_one_specified(input.numericalDatas[0].integratorParams, "integratorParameters");

                    auto ic = Util::find_named(input.numericalDatas[0].initialConditions, ictype);
                    double dl = input.numericalDatas[0].integratorParams[0].stepLength;

                    double v0 = sqrt(ic.E0 * particle::e /* convert E in eV to Joules */ * 2. / p.m);

                    double dt = magneticAxis.R * dl / v0;
                    auto rhs = f.create<TRHS>(p, bfield);

                    return RK4Stepper<TRHS>(rhs, dt);
                }
            };

            template<typename TRHS>
            struct factory_impl<Dopri5Stepper<TRHS>> {
                static Dopri5Stepper<TRHS> create(XmlFactory __attribute__((__unused__)) &f, const AOFinput &input, particle &p, std::string ictype) {
                    Verify::exactly_one_specified(input.physicalDatas, "physicalData");
                    Verify::exactly_one_specified(input.physicalDatas[0].magneticFields, "magneticField");
                    Verify::exactly_one_specified(input.physicalDatas[0].magneticFields[0].topologies, "topology");

                    Opoint magneticAxis = input.physicalDatas[0].magneticFields[0].topologies[0].mainAxis();

                    Verify::exactly_one_specified(input.numericalDatas, "numericalData");
                    Verify::exactly_one_specified(input.numericalDatas[0].integratorParams, "integratorParameters");

                    auto ic = Util::find_named(input.numericalDatas[0].initialConditions, ictype);
                    double dl = input.numericalDatas[0].integratorParams[0].stepLength;

                    double v0 = sqrt(ic.E0 * particle::e /* convert E in eV to Joules */ * 2. / p.m);

                    double dt = magneticAxis.R * dl / v0;
                    auto rhs = f.create<TRHS>(p);

                    return Dopri5Stepper<TRHS>(rhs, dt);
                }
            };

            template<typename TStepper, typename TFinalCondition>
            struct factory_impl<Integrator<TStepper, TFinalCondition>> {
                static Integrator<TStepper, TFinalCondition> create(XmlFactory &f, const AOFinput __attribute__((__unused__)) &input, particle p, std::string ictype, typename TStepper::rhs_t::globalfield_t bfield) {
                    auto ode = f.create<TStepper>(p, ictype, bfield);
                    auto fc = f.create<TFinalCondition>(p, ictype);

                    return Integrator<TStepper, TFinalCondition>(ode, fc);
                }

            };

            template<typename TGlobalField>
            struct factory_impl <GuidingCenterRHSEvaluator<TGlobalField>> {
                static GuidingCenterRHSEvaluator<TGlobalField> create(XmlFactory __attribute__((__unused__)) &f, const AOFinput __attribute__((__unused__)) &input, particle p, TGlobalField &field) {
                    return GuidingCenterRHSEvaluator<TGlobalField>(field, p);
                }
            };

            template<typename state, typename TChamber, typename TPlasmaVolume>
            struct factory_impl<GuidingCenterFinalConditionBuilder<state, TChamber, TPlasmaVolume>> {
                static GuidingCenterFinalConditionBuilder<state, TChamber, TPlasmaVolume> create(XmlFactory __attribute__((__unused__)) &f, const AOFinput &input, particle p, std::string ictype)  {
                    Verify::exactly_one_specified(input.numericalDatas, "numericalData");
                    Verify::exactly_one_specified(input.numericalDatas[0].finalConditions, "finalCondition");

                    auto ic = Util::find_named(input.numericalDatas[0].initialConditions, ictype);

                    finalCondition fc = input.numericalDatas[0].finalConditions[0];
                    double E0 = ic.E0 * particle::e;

                    auto chamber = f.create<TChamber>();
                    auto lcfs = f.create<TPlasmaVolume>();

                    return GuidingCenterFinalConditionBuilder<state, TChamber, TPlasmaVolume>(fc.time, fc.laps, chamber, lcfs, E0, p);
                }
            };

            template<typename state>
            struct factory_impl<SingleParticleGCInitialCondition<state>> {
                template<typename TFieldModel>
                static SingleParticleGCInitialCondition<state> create(XmlFactory __attribute__((__unused__)) &f, const AOFinput &input, TFieldModel &bfield) {
                    Verify::exactly_one_specified(input.numericalDatas, "numericalData");

                    initialCondition ic = Util::find_named(input.numericalDatas[0].initialConditions, "single");

                    particle p = particle::named(ic.particleType);

                    double v0 = sqrt(ic.E0 * particle::e /* to convert E to Joules */ * 2. / p.m);

                    double vpar0 = v0 * (1 - ic.pitch);
                    double vperp0 = v0 * ic.pitch;

                    double mu0 = p.m * vperp0 * vperp0 / (2 * (bfield.B(ic.R0, ic.phi0, ic.Z0).length()));

                    state X0;
                    X0 << ic.R0, ic.phi0, ic.Z0, vpar0, mu0, ic.phase0;

                    return SingleParticleGCInitialCondition<state>(X0, p);
                }
            };

            template<typename TIntegrator, typename TIC>
            struct factory_impl<Driver<TIntegrator, TIC>> {
                static Driver<TIntegrator, TIC> create(XmlFactory &f, const AOFinput __attribute__((__unused__)) &input) {
                    auto b = f.create<typename TIntegrator::stepper_t::rhs_t::globalfield_t>(true);

                    auto ic = f.create<TIC>(b);
                    auto integrator = f.create<TIntegrator>(ic.particleType(), ic.name(), b);

                    auto mainbuilder = f.create<std::unique_ptr<IOutputBuilder>>(std::string("main"), "");
                    auto summaryin = f.create<std::unique_ptr<IOutputBuilder>>(std::string("summary"), "");

                    return Driver<TIntegrator, TIC>(integrator, ic, std::move(mainbuilder), std::move(summaryin));
                }
            };

            template<>
            struct factory_impl<std::unique_ptr<IOutputBuilder>> {
                template<typename T>
                static std::unique_ptr<IOutputBuilder> create(XmlFactory __attribute__((__unused__)) &f, const AOFinput __attribute__((__unused__)) &input, const T &t, const std::string &name) {
                    if (t.fileName != "") {
                        return std::unique_ptr<IOutputBuilder>(new SingleFileOutputBuilder(t.fileName));
                    } else if (t.filePattern != "") {
                        return std::unique_ptr<IOutputBuilder>(new ManyFilesOutputBuilder(t.filePattern));
                    }
                    throw invalid_input_error("The output " + name + " must have either fileName or filePattern specified.");
                }
            };
            template<>
            std::unique_ptr<IOutputBuilder> factory_impl<std::unique_ptr<IOutputBuilder>>::create(XmlFactory &f, const AOFinput &input, const std::string &type, const std::string &name);


            template<typename TItp, typename TB>
            struct factory_impl<ParticleSpecies<TItp, TB>> {
                template<typename TSpecies>
                static ParticleSpecies<TItp, TB> create(XmlFactory &f, const __attribute__((__unused__)) AOFinput &input, TSpecies s, TB bfield) {
                    particle type(s.amn * particle::u, s.zion, s.zn, s.name, s.symbol);
                    auto density = f.create<TItp>(s, "density");
                    auto temperature = f.create<TItp>(s, "temperature");

                    return ParticleSpecies<TItp, TB>(density, temperature, type, bfield);
                }
            };

            template<typename TParticleSpecies, typename TRateCoeff, typename TPlasmaVolume>
            struct factory_impl <ThermalPlasma<TParticleSpecies, TRateCoeff, TPlasmaVolume>> {
                static ThermalPlasma<TParticleSpecies, TRateCoeff, TPlasmaVolume> create(XmlFactory &f, const AOFinput &input, typename TParticleSpecies::bfield_t bfield) {
                    Verify::exactly_one_specified(input.physicalDatas, "physicalData");
                    Verify::exactly_one_specified(input.physicalDatas[0].plasmas, "plasma");

                    auto react = f.create<Reaction<2, TParticleSpecies, TRateCoeff>>(bfield);
                    auto v = f.create<TPlasmaVolume>();

                    ThermalPlasma<TParticleSpecies, TRateCoeff, TPlasmaVolume> tp(react, v);

                    auto plasma = input.physicalDatas[0].plasmas[0];
                    std::vector<TParticleSpecies> spcs;
                    for (uint s = 0; s < plasma.particles.size(); ++s) {
                        tp.species().push_back(f.create<TParticleSpecies>(plasma.particles[s], bfield));
                    }

                    if (input.outputs.size() > 0) {
                        Verify::exactly_one_specified(input.outputs, "output");

                        if (Util::has_named(input.outputs[0].diagnostics, "thermal-plasma")) {
                            diagnostic d = Util::find_named(input.outputs[0].diagnostics, "thermal-plasma");
                            auto outbuilder = f.create<std::unique_ptr<IOutputBuilder>>(std::string("diagnostic"), "thermal-plasma");

                            ThermalPlasmaDump<ThermalPlasma<TParticleSpecies, TRateCoeff, TPlasmaVolume>> dump(outbuilder->get(), d.Rmin, d.Rmax, d.dR, d.Zmin, d.Zmax, d.dZ, tp);
                            std::cout << "Dumping thermal plasma...";
                            dump.print();
                        }
                    }

                    return tp;
                }
            };

            template<>
            struct factory_impl<AxisymmetricPolygonChamber> {
                static AxisymmetricPolygonChamber create(XmlFactory &f, const AOFinput &input);
            };

            template<>
            struct factory_impl<PlasmaVolume> {
                static PlasmaVolume create(XmlFactory &f, const AOFinput &input);
            };

            template<>
            struct factory_impl<ParametrizedRateCoefficient> {
                static ParametrizedRateCoefficient create(XmlFactory &f, const AOFinput &input, const rateCoefficient &rc);
            };

            template<typename state, typename TThermalPlasma, typename TGlobalField, typename TPRNG>
            struct factory_impl<ParticleCloudGCInitialCondition<state, TThermalPlasma, TGlobalField, TPRNG>> {
                static ParticleCloudGCInitialCondition<state, TThermalPlasma, TGlobalField, TPRNG> create(XmlFactory &f, const AOFinput &input, TGlobalField b) {
                    Verify::exactly_one_specified(input.numericalDatas, "numericalData");
                    initialCondition ic = Util::find_named(input.numericalDatas[0].initialConditions, "cloud");

                    int N = ic.Nparticles;

                    auto pl = f.create<TThermalPlasma>(b);

                    auto prtcl = particle::named(ic.particleType);

                    return ParticleCloudGCInitialCondition<state, TThermalPlasma, TGlobalField, TPRNG>(N, prtcl, pl, b, ic.E0 * particle::e, ic.time);
                }
            };

            template<typename TParticleSpecies, typename TRateCoeff>
            struct factory_impl <Reaction<2, TParticleSpecies, TRateCoeff>> {
                static Reaction<2, TParticleSpecies, TRateCoeff> create(XmlFactory &f, const AOFinput &input, typename TParticleSpecies::bfield_t bfield) {
                    Verify::exactly_one_specified(input.physicalDatas, "physicalData");
                    Verify::exactly_one_specified(input.physicalDatas[0].plasmas, "plasma");

                    auto plsm = input.physicalDatas[0].plasmas[0];

                    Verify::exactly_one_specified(plsm.reactions, "reaction");

                    auto rct = plsm.reactions[0];

                    if (rct.reactants.size() != 2) {
                        throw invalid_input_error("Invalid number of reactants: " + std::to_string(rct.reactants.size()));
                    }

                    auto r1 = f.create<TParticleSpecies>(Util::find_named(plsm.particles, rct.reactants[0].name), bfield);
                    auto r2 = f.create<TParticleSpecies>(Util::find_named(plsm.particles, rct.reactants[1].name), bfield);

                    auto rc = f.create<TRateCoeff>(rct.rateCoefficients[0]);

                    return Reaction<2, TParticleSpecies, TRateCoeff>(r1, r2, rc);
                }
            };
        }
    }
}

#endif
