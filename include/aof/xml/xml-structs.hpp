#ifndef XML_STRUCTS_HPP
#define XML_STRUCTS_HPP

#include <vector>
#include <string>
#include <algorithm>

namespace aof {
    namespace xml {
        namespace structs {

            struct point_rz_t {
                double R;
                double Z;
            };

            struct vertex : point_rz_t {
            };

            struct origo : point_rz_t {
            };

            struct values {
                std::string data;
            };

            struct uniform {
                double minValue;
                double maxValue;
                int gridPoints;
            };

            struct nonUniform {
                std::vector<values> valueList;
            };

            struct grid {
                std::string name;
                std::vector<uniform> uniforms;
                std::vector<nonUniform> nonUniforms;
            };

            struct coordinate {
                std::string name;
                unsigned int dimension;
            };

            struct field {
                std::string name;
                unsigned int dimensions;
                std::vector<coordinate> coordinates;
                std::vector<values> valueList;
            };

            struct representation {

                std::string name;


                double R0;
                double Z0;
                double Rx;
                double psi0;
                double E;
                double tau;


                std::vector<grid> grids;
                std::vector<field> fields;
            };

            struct Xpoint : point_rz_t {
                bool lastClosed;
            };

            struct Opoint : point_rz_t {
                bool mainAxis;
            };

            struct separatrix {
                std::string type;
                bool lastClosed;

                // Used for circular separatrices
                double r;
                double Rcentre;
                double Zcentre;

                // Used for polygon separatrices
                std::vector<vertex> vertices;
                std::vector<origo> origos;
            };

            struct topology {
                std::vector<Xpoint> xpoints;
                std::vector<Opoint> opoints;
                std::vector<separatrix> separatrices;

                Opoint mainAxis() const {
                    auto axisitr = std::find_if(opoints.begin(), opoints.end(), [](Opoint op) -> bool { return op.mainAxis; });

                    if (axisitr == opoints.end()) {
                        throw "No magnetic axis defined.";
                    }

                    return *axisitr;
                }
            };

            struct magneticField {
                std::vector<representation> representations;
                std::vector<topology> topologies;
            };

            struct species {
                double amn;
                int zn;
                int zion;
                std::string name;
                std::string symbol;
                std::vector<field> fields;
                std::vector<grid> grids;
            };

            struct reactant {
                std::string name;
            };

            struct product {
                std::string name;
            };

            struct parametrization {
                double BG, mc2, C1, C2, C3, C4, C5, C6, C7, Tmin, Tmax;
            };

            struct rateCoefficient {
                std::vector<parametrization> parametrizations;
            };

            struct reaction {
                std::vector<reactant> reactants;
                std::vector<product> products;
                std::vector<rateCoefficient> rateCoefficients;
            };

            struct plasma {
                std::vector<species> particles;
                std::vector<reaction> reactions;
            };

            struct chamber {
                std::string name;
                std::vector<vertex> vertices;
                std::vector<origo> origos;
            };

            struct physicalData {
                std::vector<magneticField> magneticFields;
                std::vector<plasma> plasmas;
                std::vector<chamber> chambers;
            };

            struct initialCondition {
                std::string name;
                // Single particle initial conditions
                double R0;
                double phi0;
                double Z0;
                double phase0;

                double pitch;
                double E0;

                double time;

                // Particle cloud initial conditions
                int Nparticles;

                // Both kinds
                std::string particleType;
            };

            struct finalCondition {
                double time;
                int laps;
            };

            struct integratorParameters {
                double stepLength;
            };

            struct numericalData {
                std::vector<initialCondition> initialConditions;
                std::vector<finalCondition> finalConditions;
                std::vector<integratorParameters> integratorParams;
            };

            struct outputItem {
                std::string fileName;
                std::string filePattern;
            };

            struct diagnostic : public outputItem {
                // All diagnostics
                std::string name;

                // 2D dumps
                double Rmin, Rmax, dR, Zmin, Zmax, dZ;

                // 1D dumps
                double psimin, psimax, dpsi;
            };

            struct main : public outputItem {
            };

            struct summary : public outputItem {
            };

            struct output {
                std::vector<diagnostic> diagnostics;
                std::vector<main> mains;
                std::vector<summary> summaries;
            };

            struct AOFinput {
                std::vector<physicalData> physicalDatas;
                std::vector<numericalData> numericalDatas;
                std::vector<output> outputs;
            };

        }
    }
}

#endif
