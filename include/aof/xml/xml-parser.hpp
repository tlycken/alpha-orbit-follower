#ifndef XML_PARSER_HPP
#define XML_PARSER_HPP

#include <string>
#include <sstream>
#include <stdexcept>

#include "aof/xml/xml-structs.hpp"

namespace aof {
    namespace xml {

        using namespace structs;

        /**
         * @brief Interface for xml parsing capacities
         * @details Classes that wish to provide XML parsing capabilities to this program should implement this interface.
         *
         * @author Tomas Lycken
         */
        class IXmlParser {
        public:
            /**
             * @brief Parse the provided XML file
             * @details Tries to open the file specified by fpath, interpreted as a relative path for reading, and on success parses the file contents.
             *
             * @note After this method is successfully called, hasDocument() should return `true`.
             *
             * @throws no_file_error if the file could not be opened
             *
             * @param fpath The (relative) path to the xml data file
             */
            virtual void parse(std::string fpath) = 0;
            /**
             * @brief Parse the provided XML stream
             * @details Reads the provided stream as an xml document, and parses it
             *
             * @note After this method is successfully called, hasDocument() should return `true`.
             *
             * @param xmlstream A stream with XML contents to parse
             */
            virtual void parse(std::stringstream &xmlstream) = 0;
            /**
             * @brief Indicates wether a document has been parsed.
             * @details This function reports the status of the parser, since instantiating it does not imply parsing a document.
             *
             * @return `true` if an XML document has successfully been parsed, `false` otherwise
             */
            virtual bool hasDocument() const = 0;

            /**
             * @brief Get the parsed data
             * @details After parsing an XML file, an AOFinput struct is, or can be, instantiated and filled with the data. This method returns this struct.
             *
             * @throws no_document_error if called when `hasDocument() == false`
             *
             * @return An AOFinput with the data from the parsed XML file
             */
            virtual AOFinput getInput() const = 0;
            virtual ~IXmlParser() { }
        };

        /**
         * @brief Implementation wrapper for XML parser classes
         * @details This class declaration allows for wrapping an external xml parsing library using the [PIMPL pattern](http://en.wikipedia.org/wiki/Pimpl)
         *
         * @author Tomas Lycken
         */
        class XmlParser : IXmlParser {
        private:
            class Impl;
            Impl* impl;

            bool hasdoc = false;

        public:
            /**
             * @brief Constructor
             */
            XmlParser();
            void parse(std::string);
            void parse(std::stringstream &);
            bool hasDocument() const;

            AOFinput getInput() const;

            ~XmlParser();
        };

        /**
         * @brief Exception thrown when trying to read from a parser without first parsing a document.
         *
         * @author Tomas Lycken
         */
        class no_document_error : public std::runtime_error {
        public:
            no_document_error() : no_document_error("") { }
            no_document_error(const std::string &msg) : std::runtime_error("Trying to get input without first parsing a document: " + msg) {

            }
        };

        /**
         * @brief Exception thrown when a file cannot be opened for parsing
         *
         * @author Tomas Lycken
         */
        class no_file_error : public std::runtime_error {
        public:
            no_file_error() : no_file_error("") { }
            no_file_error(const std::string &msg) : std::runtime_error("Could not find the requested file: " + msg) { }
        };
    }
}

#endif
