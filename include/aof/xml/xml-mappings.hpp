#include <vector>
#include <string>

// needed because CHILD outputs vector<T> without the std:: qualifier
using std::vector;
using namespace aof::xml;

STRUCT(point_rz_t) {
    ATTR(double, R);
    ATTR(double, Z);
}

STRUCT_INHERIT(vertex, point_rz_t) {
    ATTR(double, R);
    ATTR(double, Z);
}

STRUCT_INHERIT(origo, point_rz_t) {
    ATTR(double, R);
    ATTR(double, Z);
}

STRUCT(values) {
    TEXT(std::string, data);
}

STRUCT (uniform) {
    ATTR(double, minValue);
    ATTR(double, maxValue);
    ATTR(int, gridPoints);
}

STRUCT(nonUniform) {
    CHILD(values, valueList);
}

STRUCT(grid) {
    ATTR(std::string, name);
    CHILD(uniform, uniforms);
    CHILD(nonUniform, nonUniforms);
}

STRUCT(coordinate) {
    ATTR(std::string, name);
    ATTR(int, dimension);
}

STRUCT(field) {
    ATTR(std::string, name);
    ATTR(int, dimensions);
    CHILD(coordinate, coordinates);
    CHILD(values, valueList);
}

STRUCT(representation) {
    // Can be used for searching the parsed struct
    ATTR(std::string, name);

    // These appear on Solovev representations
    ATTR(double, R0);
    ATTR(double, Z0);
    ATTR(double, Rx);
    ATTR(double, psi0);
    ATTR(double, E);
    ATTR(double, tau);

    // These appear on more general representations, such as Grad-Shafranov
    CHILD(grid, grids);
    CHILD(field, fields);
}

STRUCT_INHERIT(Xpoint, point_rz_t) {
    ATTR(double, R);
    ATTR(double, Z);
    ATTR(bool, lastClosed);
}

STRUCT_INHERIT(Opoint, point_rz_t) {
    ATTR(double, R);
    ATTR(double, Z);
    ATTR(bool, mainAxis);
}

STRUCT(separatrix) {
    ATTR(std::string, type);
    ATTR(bool, lastClosed);
    ATTR(double, r);
    ATTR(double, Rcentre);
    ATTR(double, Zcentre);
    CHILD(vertex, vertices);
    CHILD(origo, origos);
}

STRUCT(topology) {
    CHILD(Xpoint, xpoints);
    CHILD(Opoint, opoints);
    CHILD(separatrix, separatrices);
}

STRUCT (magneticField) {
    CHILD(representation, representations);
    CHILD(topology, topologies);
}

STRUCT(species) {
    ATTR(double, amn);
    ATTR(int, zn);
    ATTR(int, zion);
    ATTR(std::string, name);
    ATTR(std::string, symbol);
    CHILD(field, fields);
    CHILD(grid, grids);
}

STRUCT(reactant) {
    ATTR(std::string, name);
}

STRUCT(product) {
    ATTR(std::string, name);
}

STRUCT(parametrization) {
    ATTR(double, BG);
    ATTR(double, mc2);
    ATTR(double, C1);
    ATTR(double, C2);
    ATTR(double, C3);
    ATTR(double, C4);
    ATTR(double, C5);
    ATTR(double, C6);
    ATTR(double, C7);
    ATTR(double, Tmin);
    ATTR(double, Tmax);
}

STRUCT(rateCoefficient) {
    CHILD(parametrization, parametrizations);
}

STRUCT(reaction) {
    CHILD(reactant, reactants);
    CHILD(product, products);
    CHILD(rateCoefficient, rateCoefficients);
}

STRUCT(plasma) {
    CHILD(species, particles);
    CHILD(reaction, reactions);
}

STRUCT (chamber) {
    ATTR(std::string, name);
    CHILD(vertex, vertices);
    CHILD(origo, origos);
}

STRUCT (physicalData)  {
    CHILD(magneticField, magneticFields);
    CHILD(plasma, plasmas);
    CHILD(chamber, chambers);
}

STRUCT(initialCondition) {
    ATTR(std::string, name);
    ATTR(double, R0);
    ATTR(double, phi0);
    ATTR(double, Z0);
    ATTR(double, phase0);
    ATTR(double, pitch);
    ATTR(double, E0);
    ATTR(double, time);

    ATTR(int, Nparticles);

    ATTR(std::string, particleType);
}

STRUCT(finalCondition) {
    ATTR(double, time);
    ATTR(int, laps);
}

STRUCT(integratorParameters) {
    ATTR(double, stepLength);
}

STRUCT(numericalData)  {
    CHILD(initialCondition, initialConditions);
    CHILD(finalCondition, finalConditions);
    CHILD(integratorParameters, integratorParams);
}

STRUCT(diagnostic) {
    ATTR(std::string, name);
    ATTR(std::string, fileName);

    ATTR(double, Rmin);
    ATTR(double, Rmax);
    ATTR(double, dR);
    ATTR(double, Zmin);
    ATTR(double, Zmax);
    ATTR(double, dZ);
    ATTR(double, psimin);
    ATTR(double, psimax);
    ATTR(double, dpsi);
}

STRUCT(main) {
    ATTR(std::string, fileName);
    ATTR(std::string, filePattern);
}

STRUCT(summary) {
    ATTR(std::string, fileName);
}

STRUCT(output) {
    CHILD(diagnostic, diagnostics);
    CHILD(main, mains);
    CHILD(summary, summaries);
}

STRUCT (AOFinput) {
    CHILD(physicalData, physicalDatas);
    CHILD(numericalData, numericalDatas);
    CHILD(output, outputs);
}
