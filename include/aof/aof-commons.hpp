#ifndef AOF_COMMONS_HPP
#define AOF_COMMONS_HPP

#include <stdexcept>

namespace aof {
    const char SEPARATOR = ',';
}

template <typename T> inline constexpr
short int signum(T x, __attribute__((__unused__)) std::false_type is_signed) {
    return T(0) < x;
}

template <typename T> inline constexpr
short int signum(T x, __attribute__((__unused__)) std::true_type is_signed) {
    return (T(0) < x) - (x < T(0));
}

template <typename T> inline constexpr
short int signum(T x) {
    return signum(x, std::is_signed<T>());
}

struct not_implemented : public std::logic_error {
    not_implemented() : std::logic_error("Not implemented.") { }
    not_implemented(std::string msg) : std::logic_error("Not implemented: " + msg) { }
};

struct not_supported : public std::logic_error {
    not_supported() : std::logic_error("Not supported.") { }
    not_supported(std::string msg) : std::logic_error("Not supported: " + msg) { }
};

#endif
