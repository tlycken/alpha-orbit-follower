#ifndef VECTOR_ALGEBRA_HPP
#define VECTOR_ALGEBRA_HPP

#include "aof/aof-commons.hpp"
#include <string>

#define EIGEN_MATRIXBASE_PLUGIN "aof/algebra/eigen/matrixbase-extensions.hpp"
#define EIGEN_DEFAULT_IO_FORMAT IOFormat(FullPrecision, DontAlignCols, std::string(1, aof::SEPARATOR))


#include "Eigen/Core"
#include "Eigen/Geometry"

    namespace aof {
        template<size_t N>
        using vector = Eigen::Matrix<double, N, 1>;
        template<size_t M, size_t N>
        using matrix = Eigen::Matrix<double, M, N>;

    }

#endif
