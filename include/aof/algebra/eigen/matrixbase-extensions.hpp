/**
 * @file
 * Extends `Eigen::MatrixBase` with operations for scalars
 * 
 * This file will be included inside the definition of `MatrixBase`, inside Eigen, and 
 * compiled with the templates. The mechanism is described 
 * [in the Eigen documentation](http://eigen.tuxfamily.org/dox/TopicCustomizingEigen.html), 
 * and several of the implementations are taken directly from there, or adapted from the examples.
 */

/**
 * @brief Right-addition with scalar
 * @details Taken directly from example at http://eigen.tuxfamily.org/dox/TopicCustomizingEigen.html
 * 
 * With a scalar `k` and matrix `A`, `A + k` works element-wise, adding `k` to each element of `A`.
 * 
 * @param scalar The scalar to add
 * 
 * @return The resulting matrix
 */
inline const CwiseUnaryOp<internal::scalar_add_op<Scalar>, Derived>
operator+(const Scalar& scalar) const
{ return CwiseUnaryOp<internal::scalar_add_op<Scalar>, Derived>(derived(), internal::scalar_add_op<Scalar>(scalar)); }
/**
 * @brief Left-addition with scalar
 * @details Taken directly from example at http://eigen.tuxfamily.org/dox/TopicCustomizingEigen.html
 * 
 * With a scalar `k` and matrix `A`, `k + A` works element-wise, adding `k` to each element of `A`.
 * 
 * @param scalar The scalar to add
 * 
 * @return The resulting matrix
 */
inline friend const CwiseUnaryOp<internal::scalar_add_op<Scalar>, Derived>
operator+(const Scalar& scalar, const MatrixBase<Derived>& mat)
{ return CwiseUnaryOp<internal::scalar_add_op<Scalar>, Derived>(mat.derived(), internal::scalar_add_op<Scalar>(scalar)); }

/**
 * @brief Right-subtraction with scalar
 * @details Implemented as a copy of right-addition, but passing `-scalar`.
 * 
 * With a scalar `k` and matrix `A`, `k - A` works element-wise, and the resulting matrix will have 
 * each element equal to `k` minus the corresponding element of `A`.
 * 
 * @param scalar The scalar to subtract from.
 * 
 * @return The resulting matrix
 */
inline const CwiseUnaryOp<internal::scalar_add_op<Scalar>, Derived>
operator-(const Scalar& scalar) const
{ return CwiseUnaryOp<internal::scalar_add_op<Scalar>, Derived>(derived(), internal::scalar_add_op<Scalar>(-scalar)); }
/**
 * @brief Right-subtraction with scalar
 * @details Implemented as a copy of right-addition, but passing `-scalar`.
 * 
 * With a scalar `k` and matrix `A`, `A - k` works element-wise, and the resulting matrix will have 
 * each element equal to the corresponding element of `A` minus `k`.
 * 
 * @param scalar The scalar to subtract
 * 
 * @return The resulting matrix
 */
inline friend const CwiseUnaryOp<internal::scalar_add_op<Scalar>, Derived>
operator-(const Scalar& scalar, const MatrixBase<Derived>& mat)
{ return CwiseUnaryOp<internal::scalar_add_op<Scalar>, Derived>(mat.derived(), internal::scalar_add_op<Scalar>(-scalar)); }

inline RealScalar squaredLength() const { return squaredNorm(); }
inline RealScalar length() const { return norm(); }
inline RealScalar invLength(void) const { return fast_inv_sqrt(squaredNorm()); }
