#ifndef OUTPUT_HPP
#define OUTPUT_HPP

#include <string>
#include <limits>
#include <fstream>
#include <stdexcept>
#include <algorithm>

#include "aof/aof-commons.hpp"
#include "aof/physics/global-magnetic-field.hpp"
#include "aof/numerics/interpolation.hpp"
#include "aof/physics/plasma.hpp"

namespace aof {
    namespace io {

        class IOutputBuilder {
        public:
            virtual std::ostream &get() = 0;
            virtual ~IOutputBuilder() { }
        };

        class IMainOutputBuilder : public IOutputBuilder {
        public:
            virtual ~IMainOutputBuilder() { }
        };

        class SingleFileOutputBuilder : public IOutputBuilder {
        private:
            std::string name;
        public:
            SingleFileOutputBuilder(const std::string &name);
            std::ostream &get();
        };

        class ManyFilesOutputBuilder : public IOutputBuilder {
        private:
            std::string namepattern;
            unsigned int N;

        public:
            ManyFilesOutputBuilder(std::string namepattern);
            std::ostream &get();
        };

        class NullBuffer : public std::streambuf {
        public:
            int overflow(int c);
        };

        class NullStream : public std::ostream {
        public:
            NullStream();
        private:
            NullBuffer m_sb;
        };

        class NullOutputBuilder : public IOutputBuilder {
        private:
            NullStream stream;
        public:
            std::ostream &get();
        };

        class MagneticFieldDump {
        private:
            std::ostream &f;
            double Rmin, Rmax, dR, Zmin, Zmax, dZ;
            const aof::physics::IGlobalMagneticField &field;

        public:
            MagneticFieldDump(std::ostream &out, double Rmin, double Rmax, double dR, double Zmin, double Zmax, double dZ, const aof::physics::IGlobalMagneticField &field);

            void print() const;
        };

        class Interp1DDump {
        private:
            std::ostream &f;
            double xmin, xmax, dx;
            const aof::interpolation::IInterp1D &itp;

        public:
            Interp1DDump(std::ostream &out, double xmin, double xmax, double dx, const aof::interpolation::IInterp1D &itp);

            void print() const;
        };

        class Interp2DDump {
        private:
            std::ostream &f;
            double xmin, xmax, dx, ymin, ymax, dy;
            const aof::interpolation::IInterp2D &itp;

        public:
            Interp2DDump(std::ostream &out, double xmin, double xmax, double dx, double ymin, double ymax, double dy, const aof::interpolation::IInterp2D &itp);

            void print() const;
        };

        template<typename TPlasma>
        class ThermalPlasmaDump {
        private:
            std::ostream &f;
            double Rmin, Rmax, dR, Zmin, Zmax, dZ;
            TPlasma &plasma;

        public:
            ThermalPlasmaDump(std::ostream &out,
                              double Rmin, double Rmax, double dR,
                              double Zmin, double Zmax, double dZ,
                              TPlasma &plasma)
                : f(out),
                  Rmin(Rmin), Rmax(Rmax), dR(dR),
                  Zmin(Zmin), Zmax(Zmax), dZ(dZ),
                  plasma(plasma) {
            }

            void print() const {
                if (!f.good()) {
                    throw std::runtime_error("An error occurred when opening output file.");
                }

                f << "R" << SEPARATOR;
                f << "Z" << SEPARATOR;

                auto spcs = plasma.species();

                std::for_each(spcs.begin(), spcs.end(), [this](typename TPlasma::species_t s) -> void {
                    f << s.properties().name << "Density" << SEPARATOR;
                    f << s.properties().name << "Temperature" << SEPARATOR;
                });

                f << "RateCoeff" << SEPARATOR;
                f << "Rate" << SEPARATOR;
                f << "psi" << std::endl;

                f.precision(std::numeric_limits<double>::digits10 + 2);

                for (double R = Rmin; R <= Rmax; R += dR) {
                    for (double Z = Zmin; Z <= Zmax; Z += dZ) {
                        f << R << SEPARATOR << Z << SEPARATOR;
                        std::for_each(spcs.begin(), spcs.end(), [this, R, Z](typename TPlasma::species_t s) -> void {
                            f << s.n(R, 0.0, Z) << SEPARATOR;
                            f << s.T(R, 0.0, Z) << SEPARATOR;
                        });
                        try {
                            f << plasma.reaction().rateCoefficient.rateCoeff(spcs[0].T(R, 0.0, Z));
                        } catch (std::range_error e) {
                            f << 0;
                        }
                        f << SEPARATOR;
                        try {
                            f << plasma.reaction().intensity(R, 0.0, Z);
                        } catch (std::range_error e) {
                            f << 0;
                        }
                        f << SEPARATOR;
                        f << spcs[0].bfield.psi(R, 0.0, Z);
                        f << std::endl;
                    }
                }
            }
        };
    }
}
template<typename state>
std::ostream &header(std::ostream &os);

template<template <typename state> class solpoint, typename state>
std::ostream &operator<<(std::ostream &os, std::vector<solpoint<state>> trace);

#endif
