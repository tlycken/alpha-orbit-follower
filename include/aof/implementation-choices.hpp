#ifndef IMPLEMENTATION_CHOICES_HPP
#define IMPLEMENTATION_CHOICES_HPP

#include "aof/numerics/interpolation.hpp"
#include "aof/physics/flux-function-interpolation.hpp"
#include "aof/physics/grad-shafranov-model.hpp"
#include "aof/physics/guiding-center-rhs.hpp"
#include "aof/numerics/prng.hpp"

typedef aof::interpolation::Interp1D itp1_t;
typedef aof::physics::FluxFunctionInterpolation<itp1_t, 13> itpF_t;
typedef aof::interpolation::Interp2D itp2_t;
typedef aof::physics::GradShafranovModel<itp2_t, itpF_t, 13> globalfield_t;
typedef aof::physics::GuidingCenterRHSEvaluator<globalfield_t> rhs_t;
typedef rhs_t::state_t state_t;

typedef aof::physics::ParticleSpecies<itp1_t, globalfield_t> particle_t;
typedef aof::physics::ParametrizedRateCoefficient ratecoeff_t;
typedef aof::physics::PlasmaVolume plasmavol_t;
typedef aof::physics::ThermalPlasma<particle_t, ratecoeff_t, plasmavol_t> plasma_t;

typedef aof::physics::AxisymmetricPolygonChamber chamber_t;

typedef aof::rng::MT rng_t;

typedef aof::ode::RK4Stepper<rhs_t> stepper_t;
// typedef aof::ode::Dopri5Stepper<rhs_t> stepper_t;
typedef aof::physics::GuidingCenterFinalConditionBuilder<state_t, chamber_t, plasmavol_t> finalcondbuilder_t;
typedef aof::ode::Integrator<stepper_t, finalcondbuilder_t> integrator_t;
//typedef aof::physics::SingleParticleGCInitialCondition<state_t> initialcondition_t;
typedef aof::physics::ParticleCloudGCInitialCondition<state_t, plasma_t, globalfield_t, rng_t> initialcondition_t;
typedef aof::ode::Driver<integrator_t, initialcondition_t> driver_t;


#endif
