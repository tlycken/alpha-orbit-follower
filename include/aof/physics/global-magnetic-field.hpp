#ifndef GLOBAL_MAGNETIC_FIELD_MODELS_HPP
#define GLOBAL_MAGNETIC_FIELD_MODELS_HPP

#include "aof/aof-commons.hpp"
#include "aof/physics/magnetic-field.hpp"
#include "aof/physics/local-magnetic-field.hpp"
#include "aof/numerics/interpolation.hpp"
#include <string>
#include <fstream>

namespace aof {
    namespace physics {

        /**
         * @brief Interface for global magnetic field descriptions
         * @details Given some representation of the magnetic field, implementations of this interface should be able to calculate \f$\vec B\f$ and \f$\nabla\otimes\vec B\f$
         *
         * @author Tomas Lycken
        */
        class IGlobalMagneticField {
        public:
            /**
             * @brief Calculates \f$\nabla\otimes\vec B\f$ at a point \f$(R,\varphi,Z)\f$
             * @details The interface does not make any assumptions about symmetry etc, but implementations may e.g. assume axisymmetry, and ignore the \f$\varphi\f$ coordinate (the second argument, `phi`)
             *
             * @param R The radial coordinate
             * @param phi The azimuthal angle coordinate
             * @param Z The \f$Z\f$ coordinate
             *
             * @return \f$\nabla\otimes\vec B\f$ in the given point
            */
            virtual matrix<3, 3> nabla_outerprod_B(double R, double phi, double Z) const = 0;

            /**
             * @brief Calculates \f$\vec B\f$ at a point \f$(R,\varphi,Z)\f$
             * @details The interface does not make any assumptions about symmetry etc, but implementations may e.g. assume axisymmetry, and ignore the \f$\varphi\f$ coordinate (the second argument, `phi`)
             *
             * @param R The radial coordinate
             * @param phi The azimuthal angle coordinate
             * @param Z The \f$Z\f$ coordinate
             *
             * @return \f$\vec B\f$ in the given point
            */
            virtual vector<3> B(double R, double phi, double Z) const = 0;

            /**
             * @brief Calculates the poloidal flux \f$\psi\f$ at a point \f$(R,\varphi,Z)\f$
             * @details The interface does not make any assumptions about symmetry etc, but implementations may e.g. assume axisymmetry, and ignore the \f$\varphi\f$ coordinate (the second argument, `phi`)
             *
             * @param R The radial coordinate
             * @param phi The azimuthal angle coordinate
             * @param Z The \f$Z\f$ coordinate
             *
             * @return The poloidal flux \f$\psi\f$ in the given point
            */
            virtual double psi(double R, double phi, double Z) const = 0;

            virtual double F(double psi) const = 0;

            /**
             * @brief Virtual destructor
             * @details Defined so that implementing classes can clean up after themselves
             */
            virtual ~IGlobalMagneticField() { };
        };

        /**
         * @brief Implementation of a global magnetic field in a parametrized Grad-Shafranov equilibrium
         * @details Uses the assumption that \f$\vec B = F(\psi)\nabla\varphi - \nabla\varphi\times\frac{\nabla\psi}{2\pi}\f$ that follows from axisymmetry and force balance, to calculate magnetic field properties \f$\vec B\f$ and \f$\nabla\otimes\vec B\f$ in a point \f$(R,\varphi,Z)\f$. Furthermore, assumes a solution to the Grad-Shafranov equation on the form
         * \f[
         * \psi(R,Z) = \frac{\psi_0}{R_0^4}\left[\left(R^2-R_0^2\right)^2 + \frac{Z^2}{E^2}\left(R^2-R_x^2\right) - \tau R_0^2\left(R^2\ln \frac{R^2}{R_0^2} - \left(R^2-R_0^2\right)-\frac{\left(R^2-R_0^2\right)^2}{2 R_0^2}\right)\right]
         * \f]
         * and calculates all quantities analytically based on the above expression for \f$\psi\f$.
         *
         * @note This class is currently only *declared*, but not *defined* - it is not currently possible to use it.
         */
        class SolovevModel {
        protected:
            double R0, Z0, Rx, psi0, E, tau;

        public:
            SolovevModel(double, double, double, double, double, double);
            ~SolovevModel();

            double psi(double, double);
            vector<3> B(double, double);
            matrix<3, 3> nabla_outerprod_B(double, double);
        };
    }
}

#endif
