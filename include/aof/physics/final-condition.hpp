#ifndef FINAL_CONDITION_HPP
#define FINAL_CONDITION_HPP

#include "aof/algebra/vector-algebra.hpp"
#include "aof/physics/rhs-evaluator.hpp"

namespace aof {
    namespace physics {

        enum class reason {
            time,
            loop,
            wall
        };

        struct trace_state {
            bool save;
            bool end;
        };

        std::ostream &operator<<(std::ostream &s, reason r);

        template<typename TFinalCondition>
        class IFinalConditionBuilder;

        template<typename state>
        struct final_state {
            state where;
            reason how;
            double when;
        };

        template<typename state>
        class IFinalCondition {
        public:
            typedef state state_t;

            virtual trace_state current_state(const solpoint<state> &prev, const solpoint<state> &cur, final_state<state> &fstate) = 0;
        };

        template<typename TFinalCondition>
        class IFinalConditionBuilder {
        public:
            virtual TFinalCondition build(const typename TFinalCondition::state_t& X0, const typename TFinalCondition::state_t& rhs0) = 0;
            virtual ~IFinalConditionBuilder() { }
        };
    }
}

#endif
