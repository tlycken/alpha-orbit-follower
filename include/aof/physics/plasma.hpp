#ifndef PLASMA_HPP
#define PLASMA_HPP

#include <string>
#include "aof/physics/particle.hpp"
#include "aof/physics/bounded-volume.hpp"

#include <cmath>

namespace aof {
    namespace physics {

        class IParticleSpecies {
        public:
            virtual particle properties() const = 0;
            virtual double n(double R, double phi, double Z) const = 0;
            virtual double T(double R, double phi, double Z) const = 0;
            virtual ~IParticleSpecies() { };
        };

        template<typename TInterp1D, typename TFieldModel>
        class ParticleSpecies : public IParticleSpecies {
        private:
            TInterp1D density;
            TInterp1D temperature;
            particle type;

        public:
            TFieldModel bfield;

            typedef TFieldModel bfield_t;

            ParticleSpecies(TInterp1D density, TInterp1D temperature, particle type, TFieldModel bfield) : density(density), temperature(temperature), type(type), bfield(bfield) { }

            particle properties() const {
                return type;
            }
            double n(double R, double phi, double Z) const {
                return density.evaluateAt(bfield.psi(R, phi, Z));
            }
            double T(double R, double phi, double Z) const {
                return temperature.evaluateAt(bfield.psi(R, phi, Z));
            }
        };

        class IRateCoefficient {
            virtual double rateCoeff(double Ti) const = 0;
        };

        class ParametrizedRateCoefficient {
        private:
            double BG2;
            double mc2;
            double C1, C2, C3, C4, C5, C6, C7;
            double Tmin, Tmax;

        public:
            ParametrizedRateCoefficient(double BG, double mc2, double C1, double C2, double C3, double C4, double C5, double C6, double C7, double Tmin, double Tmax);

            double rateCoeff(double Ti) const;
        };

        template<int NReactants, typename TParticleSpecies, typename TRateCoeff>
        struct Reaction;

        template<typename TParticleSpecies, typename TRateCoeff>
        class Reaction<2, TParticleSpecies, TRateCoeff> {
        public:
            TParticleSpecies reactant1;
            TParticleSpecies reactant2;

            TRateCoeff rateCoefficient;

            Reaction(TParticleSpecies r1, TParticleSpecies r2, TRateCoeff rc) : reactant1(r1), reactant2(r2), rateCoefficient(rc) { }

            double intensity(double R, double phi, double Z) {
                double T = reactant1.T(R, phi, Z);
                if (T != reactant2.T(R, phi, Z)) {
                    throw not_supported("Only plasmas with isothermal ion configurations are supported.");
                }

                double sigmav = rateCoefficient.rateCoeff(T);
                return sigmav * reactant1.n(R, phi, Z) * reactant2.n(R, phi, Z);
            }
        };


        template<typename TParticleSpecies, typename TRateCoeff, typename TPlasmaVolume>
        class ThermalPlasma {
        private:
            std::vector<TParticleSpecies> ss;
            Reaction<2, TParticleSpecies, TRateCoeff> r;
            TPlasmaVolume vol;

        public:
            typedef TParticleSpecies species_t;

            ThermalPlasma(Reaction<2, TParticleSpecies, TRateCoeff> rin, TPlasmaVolume v) : r(rin), vol(v) { }

            std::vector<TParticleSpecies> &species() {
                return ss;
            }

            Reaction<2, TParticleSpecies, TRateCoeff> &reaction() {
                return r;
            }

            TPlasmaVolume &boundingVolume() {
                return vol;
            }
        };

        class PlasmaVolume : public AxisymmetricPolygon {
        };
    }
}

#endif
