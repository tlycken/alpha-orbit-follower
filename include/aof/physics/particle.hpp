#ifndef PARTICLE_HPP
#define PARTICLE_HPP

#include "aof/aof-commons.hpp"

namespace aof {
    namespace physics {

        /**
         * @brief Small struct to hold particle properties
         * @details Holds properties of individual particles, such as mass, charge index and nuclear charge.
         *
         * @note Electrons are indicated by a nuclear charge index of `-1`.
         *
         * @param m Mass in kilograms
         * @param Zi Ion charge count (-1 for electrons)
         * @param Zn Nucleus charge count (-1 for electrons)
         *
         */
        struct particle {
            //! Mass in kilograms
            double m;
            //! Ion charge count (-1 for electrons)
            int Zi;
            //! Nucleus charge count (-1 for electrons)
            int Zn;
            //! Human-readable name, e.g. "Hydrogen" or "Tritium"
            std::string name;
            //! Symbol, e.g. "H" or "T"
            std::string symbol;

            //! Elementary charge in Coulombs
            static constexpr double e = 1.602176565e-19;
            //! Atomic mass unit in kilograms
            static constexpr double u = 1.66053892e-27;

            /**
             * @brief Construct a particle instance
             * @details This should only have to be done once per particle type - none of these properties refer to a specific particle, just to the particle species
             *
             * @param m Particle mass in kilograms
             * @param Zi Particle ion charge count (-1 for electrons)
             * @param Zn Particle nucleus charge count (-1 for electrons)
             */
            particle(double m, double Zi, double Zn, std::string name, std::string symbol) : m(m), Zi(Zi), Zn(Zn), name(name), symbol(symbol) { }

            double charge() const {
                return Zi * e;
            }

            static const particle alpha() {
                return particle(6.644657e-27, 2, 2, "alpha", "α");
            }

            static const particle hydrogen() {
                return particle(1.673533e-27, 1, 1, "Hydrogen", "H");
            }

            static const particle deuterium() {
                return particle(3.343583e-27, 1, 1, "Deuterium", "D");
            }

            static const particle tritium() {
                return particle(5.008267e27, 1, 1, "Tritium", "T");
            }

            static const particle named(std::string name) {
                if (name == "alpha") {
                    return particle::alpha();
                } else if (name == "Hydrogen" || name == "H") {
                    return particle::hydrogen();
                } else if (name == "Deuterium" || name == "D") {
                    return particle::deuterium();
                } else if (name == "Tritium" || name == "T") {
                    return particle::tritium();
                } else {
                    throw not_implemented("There is no particle type named \"" + name + "\"");
                }
            }
        };

    }
}

#endif
