#ifndef CHAMBER_HPP
#define CHAMBER_HPP

#include "bounded-volume.hpp"

namespace aof {
    namespace physics {

        class AxisymmetricPolygonChamber : public AxisymmetricPolygon { };

    }
}

#endif
