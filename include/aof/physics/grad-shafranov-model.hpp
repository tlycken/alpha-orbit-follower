#ifndef GRAD_SHAFRANOV_MODEL_HPP
#define GRAD_SHAFRANOV_MODEL_HPP

#include "aof/algebra/vector-algebra.hpp"
#include "aof/physics/global-magnetic-field.hpp"
#include <iostream>


namespace aof {
    namespace physics {
        /**
         * @brief Implementation of a global magnetic field in an interpolated Grad-Shafranov equilibrium.
         * @details Uses the assumption that \f$\vec B = F(\psi)\nabla\varphi - \nabla\varphi\times\frac{\nabla\psi}{2\pi}\f$ that follows from axisymmetry and force balance, to calculate magnetic field properties \f$\vec B\f$ and \f$\nabla\otimes\vec B\f$ in a point \f$(R,\varphi,Z)\f$.
         *
         * Internally, it uses two interpolation objects for representing \f$F(\psi)\f$ and \f$\psi(R,Z)\f$, and uses analytic expressions to evaluate \f$\vec B\f$ and \f$\nabla\otimes\vec B\f$ based on \f$F\f$, \f$\psi\f$ and their derivatives.
         *
         * @tparam TInterp2D The type of the interpolation object for \f$\psi(R,Z)\f$
         * @tparam TInterp1D The type of the interpolation object for \f$F(psi)\f$
         *
         * @author Tomas Lycken
         */
        template<typename TInterp2D, typename TInterp1D, int COCOS>
        class GradShafranovModel;

        template<typename TInterp2D, typename TInterp1D>
        class GradShafranovModel<TInterp2D, TInterp1D, 11> : public IGlobalMagneticField {
        private:
            static constexpr double tau = 2 * 3.141592653589793238463;
        protected:
            //! Reference to \f$\psi\f$ interpolation
            TInterp2D psi_itp;
            //! Reference to \f$F\f$ interpolation
            TInterp1D F_itp;

        public:
            static constexpr int COCOS = 11;
            /**
             * @brief Constructs a field model
             * @details Saves references to the provided interpolation objects, for use when evaluating the field properties.
             *
             * @param psi The interpolation object describing \f$\psi\f$
             * @param F The interpolation object describing \f$F\f$
             */
            GradShafranovModel(const TInterp2D &psi, const TInterp1D &F) : psi_itp(psi), F_itp(F) {
            };

            GradShafranovModel(const GradShafranovModel &other) : psi_itp(other.psi_itp), F_itp(other.F_itp) { }

            GradShafranovModel &operator=(const GradShafranovModel &other) {
                psi_itp = other.psi_itp;
                F_itp = other.F_itp;
                return *this;
            }

            matrix<3, 3> nabla_outerprod_B(double R, double __attribute__((__unused__)) phi, double Z) const {
                double psi = psi_itp.evaluateAt(R, Z);
                double psi_R = psi_itp.evaluateAt(R, Z, 1, 0);
                double psi_Z = psi_itp.evaluateAt(R, Z, 0, 1);

                double F = F_itp.evaluateAt(psi);
                double Fp = F_itp.evaluateAt(psi, 1);

                matrix<3, 3> result;
                result << -R* psi_itp.evaluateAt(R, Z, 1, 1) + psi_R,
                       tau * (R * Fp * psi_R - F),
                       R* psi_itp.evaluateAt(R, Z, 2, 0) - psi_Z,
                       -tau* F, -psi_Z, 0.,
                       -R* psi_itp.evaluateAt(R, Z, 0, 2),
                       tau* R* Fp* psi_Z,
                       R* psi_itp.evaluateAt(R, Z, 1, 1);
                return result / (tau * R * R);
            };
            vector<3> B(double R, double __attribute__((__unused__)) phi, double Z) const {
                vector<3> theB(-psi_itp.evaluateAt(R, Z, 0, 1) / tau,
                               F_itp.evaluateAt(psi_itp.evaluateAt(R, Z)),
                               psi_itp.evaluateAt(R, Z, 1, 0) / tau);

                if (!(theB.norm() > 0)) {
                    throw zero_magnetic_field_exception();
                } else if (R == 0) {
                    throw zero_magnetic_field_exception("R == 0");
                }
                return theB / R;
            };
            double psi(double R, double __attribute__((__unused__)) phi, double Z) const {
                return psi_itp.evaluateAt(R, Z);
            };
            double F(double psi) const {
                return F_itp.evaluateAt(psi);
            };

            /**
             * @brief Destructor
             * @details The destructor does NOT free the memory for the interpolation objects, as they were passed by reference. The client code is responsible for that.
             */
            ~GradShafranovModel() { };
        };

        template<typename TInterp2D, typename TInterp1D>
        class GradShafranovModel<TInterp2D, TInterp1D, 13> : public IGlobalMagneticField {
        private:
            static constexpr double tau = 2 * 3.141592653589793238463;
        protected:
            //! \f$\psi\f$ interpolation
            TInterp2D psi_itp;
            //! \f$F\f$ interpolation
            TInterp1D F_itp;

        public:
            static constexpr int COCOS = 13;

            /**
             * @brief Constructs a field model
             * @details Saves references to the provided interpolation objects, for use when evaluating the field properties.
             *
             * @param psi The interpolation object describing \f$\psi\f$
             * @param F The interpolation object describing \f$F\f$
             */
            GradShafranovModel(const TInterp2D &psi, const TInterp1D &F) : psi_itp(psi), F_itp(F) {
            };

            GradShafranovModel(const GradShafranovModel &other) : psi_itp(other.psi_itp), F_itp(other.F_itp) { }

            GradShafranovModel &operator=(const GradShafranovModel &other) {
                psi_itp = other.psi_itp;
                F_itp = other.F_itp;
                return *this;
            }

            matrix<3, 3> nabla_outerprod_B(double R, double __attribute__((__unused__)) phi, double Z) const {
                double psi = psi_itp.evaluateAt(R, Z);
                double psi_R = psi_itp.evaluateAt(R, Z, 1, 0);
                double psi_Z = psi_itp.evaluateAt(R, Z, 0, 1);

                double F = F_itp.evaluateAt(psi);
                double Fp = F_itp.evaluateAt(psi, 1);

                matrix<3, 3> result;
                result << R* psi_itp.evaluateAt(R, Z, 1, 1) - psi_R,
                       tau * (R * Fp * psi_R - F),
                       -R* psi_itp.evaluateAt(R, Z, 2, 0) + psi_Z,
                       -tau* F, psi_Z, 0.,
                       R* psi_itp.evaluateAt(R, Z, 0, 2),
                       tau* R* Fp* psi_Z,
                       -R* psi_itp.evaluateAt(R, Z, 1, 1);
                return result / (tau * R * R);
            };
            vector<3> B(double R, double __attribute__((__unused__)) phi, double Z) const {
                vector<3> theB(psi_itp.evaluateAt(R, Z, 0, 1) / tau,
                               F_itp.evaluateAt(psi_itp.evaluateAt(R, Z)),
                               -psi_itp.evaluateAt(R, Z, 1, 0) / tau);

                if (!(theB.norm() > 0)) {
                    throw zero_magnetic_field_exception();
                } else if (R == 0) {
                    throw zero_magnetic_field_exception("R == 0");
                }
                return theB / R;
            };
            double psi(double R, double __attribute__((__unused__)) phi, double Z) const {
                return psi_itp.evaluateAt(R, Z);
            };
            double F(double psi) const {
                return F_itp.evaluateAt(psi);
            };
        };
    }
}

#endif
