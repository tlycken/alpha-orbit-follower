#ifndef FLUX_FUNCTION_INTERPOLATION
#define FLUX_FUNCTION_INTERPOLATION

#include "aof/numerics/interpolation.hpp"

namespace aof {
    namespace physics {

        template<typename TInterp1D, int COCOS>
        class FluxFunctionInterpolation;

        template<typename TInterp1D>
        class FluxFunctionInterpolation<TInterp1D, 13> : public interpolation::IInterp1D {
        private:
            TInterp1D itp;
            double psiaxis, psiboundary;
            double Faxis, Fboundary, dFaxis;

        public:
            FluxFunctionInterpolation(int N, double* psigrid, double* Fvalues)
                : itp(N, psigrid, Fvalues),
                  psiaxis(psigrid[0]),
                  psiboundary(psigrid[N-1]),
                  Faxis(itp.evaluateAt(psiaxis)),
                  Fboundary(itp.evaluateAt(psiboundary)),
                  dFaxis(itp.evaluateAt(psiaxis, 1)) {
            }

            double evaluateAt(double psi) const {
                if (psi < psiaxis) {
                    return (psi - psiaxis) * dFaxis + Faxis;
                } else if (psi < psiboundary) {
                    return itp.evaluateAt(psi);
                } else {
                    return Fboundary;
                }
            }

            double evaluateAt(double psi, int dOrder) const {
                if (psi < psiaxis) {
                    return dFaxis;
                } else if (psi < psiboundary) {
                    return itp.evaluateAt(psi, dOrder);
                } else {
                    return 0;
                }
            }
        };
    }
}

#endif
