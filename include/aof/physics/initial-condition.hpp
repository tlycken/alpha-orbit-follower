#ifndef INITIAL_CONDITION_HPP
#define INITIAL_CONDITION_HPP

namespace aof {
    namespace physics {

        template<typename state>
        class IInitialCondition {
        public:
            typedef state state_t;

            struct initial_condition {
                state_t s;
                double w;
            };

            virtual bool hasNext() = 0;
            virtual initial_condition next() = 0;
            virtual particle particleType() const = 0;
            virtual std::string name() const = 0;
        };
    }
}

#endif
