#ifndef GUIDING_CENTER_FINAL_CONDITION_HPP
#define GUIDING_CENTER_FINAL_CONDITION_HPP

#include "aof/physics/final-condition.hpp"
#include "aof/physics/guiding-center-rhs.hpp"
#include "aof/physics/particle.hpp"

#include <cmath>

namespace aof {
    namespace physics {

        template<typename state, typename TChamber, typename TPlasmaVolume>
        class GuidingCenterFinalConditionBuilder;

        template<typename state, typename TChamber, typename TPlasmaVolume>
        class GuidingCenterFinalCondition : public IFinalCondition<state> {
        public:
            typedef state state_t;
            typedef GuidingCenterFinalConditionBuilder<state_t, TChamber, TPlasmaVolume> builder_t;

        private:
            const double tfinal;
            const int lapsfinal;
            const solpoint<state_t> solpoint0;

            int laps = 0;
            int signchanges = 0;

            const TChamber chamber;
            const TPlasmaVolume &lcfs;
            const double E0;
            const particle p;

        public:
            GuidingCenterFinalCondition(double tfinalin, int lapsin, solpoint<state_t> solpoint0, TChamber chamber, const TPlasmaVolume &lcfs, double E0, particle p)
                : tfinal(tfinalin),
                  lapsfinal(lapsin),
                  solpoint0(solpoint0),
                  chamber(chamber),
                  lcfs(lcfs),
                  E0(E0),
                  p(p)
            {
            }

            trace_state current_state(const solpoint<state_t> &prev, const solpoint<state_t> &cur, final_state<state_t> &fstate) {
                if (cur.t > tfinal) {
                    fstate = final_state<state_t> {
                        cur.X,
                        reason::time,
                        cur.t
                    };
                    return trace_state {true, true};
                } else if (loopClosed(prev, cur)) {
                    if (++laps == lapsfinal) {
                        fstate = final_state<state_t> {
                            cur.X,
                            reason::loop,
                            cur.t
                        };
                        return trace_state {true, true};
                    }
                } else if (hitWall(prev, cur)) {
                    fstate = final_state<state_t> {
                        cur.X,
                        reason::wall,
                        cur.t
                    };
                    return trace_state {true, true};
                }
                return trace_state {true, false};
            }

        private:

            template<int dimension>
            bool signchange(const state_t &Xprev, const state_t &Xcur) {
                return (Xprev(dimension) * Xcur(dimension) < 0);
            }

            bool insideLcfs(const solpoint<state_t> &cur) {
                return lcfs.isInside(cur.X(0), cur.X(1), cur.X(2));
            }

            bool loopClosed(const solpoint<state_t> &prev, const solpoint<state_t> &cur) {
                // Check if v-parallel has changed sign
                if (signchange<3>(prev.rhs, cur.rhs) && insideLcfs(cur)) {
                    signchanges += 1;
                }
                return signchanges >= 2 * lapsfinal + 1;
            }

            bool hitWall(const __attribute__((__unused__)) solpoint<state_t> &prev, const solpoint<state_t> &cur) {
                return !chamber.isInside(cur.X(0), cur.X(1), cur.X(2));
            }
        };

        template<typename state, typename TChamber, typename TPlasmaVolume>
        class GuidingCenterFinalConditionBuilder : public IFinalConditionBuilder<GuidingCenterFinalCondition<state, TChamber, TPlasmaVolume>> {
        private:
            double tfinal;
            int lapsfinal;

            TChamber chamber;
            TPlasmaVolume lcfs;
            double E0;
            particle p;

        public:
            GuidingCenterFinalConditionBuilder(double tfinalin, int lapsfinalin, TChamber chamber, TPlasmaVolume lcfs, double E0, particle p) : tfinal(tfinalin), lapsfinal(lapsfinalin), chamber(chamber), lcfs(lcfs), E0(E0), p(p) { }

            GuidingCenterFinalCondition<state, TChamber, TPlasmaVolume> build(const state &X0, const state &rhs0) {
                return GuidingCenterFinalCondition<state, TChamber, TPlasmaVolume>(tfinal, lapsfinal, solpoint<state> {0.0, X0, rhs0}, chamber, lcfs, E0, p);
            }
        };
    }
}

#endif
