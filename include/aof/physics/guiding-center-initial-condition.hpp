#ifndef GUIDING_CENTER_INITIAL_CONDITION_HPP
#define GUIDING_CENTER_INITIAL_CONDITION_HPP

#include "aof/physics/initial-condition.hpp"
#include "aof/physics/guiding-center-rhs.hpp"
#include "aof/physics/plasma.hpp"

namespace aof {
    namespace physics {

        template<typename state>
        class SingleParticleGCInitialCondition : public IInitialCondition<state> {
        public:
            typedef state state_t;

        private:
            state_t ic;
            bool returned;
            particle p;

        public:
            SingleParticleGCInitialCondition(state_t icin, particle pin) : ic(icin), returned(false), p(pin) { }

            bool hasNext() {
                return !returned;
            }
            typename IInitialCondition<state>::initial_condition next() {
                returned = true;
                return typename IInitialCondition<state>::initial_condition {ic, 1.};
            }

            particle particleType() const {
                return p;
            }

            std::string name() const {
                return "single";
            }
        };

        template<typename state, typename TThermalPlasma, typename TGlobalField, typename TPRNG>
        class ParticleCloudGCInitialCondition : public IInitialCondition<state> {
        public:
            typedef state state_t;

        private:
            const double twopi = 6.2831853071795864769252;
            const double state_space_vol;

            const unsigned int N;
            unsigned int returned;
            particle p;
            TThermalPlasma plasma;
            TGlobalField bfield;
            double E0;

            const double weight_factor;

            double weight(state X) {
                return weight_factor
                       * X(0)
                       * plasma.reaction().intensity(X(0), X(1), X(2));
            }

        public:
            ParticleCloudGCInitialCondition(unsigned int N, particle p, TThermalPlasma plasma, TGlobalField bfield, double E0, double dt)
                : state_space_vol(
                    // 2 /* cos alpha */
                    // * twopi /* gyro-phase */
                    // * twopi /* toroidal angle */
                    twopi /* toroidal angle */
                    * plasma.boundingVolume().surfaceArea() /* RZ */),
                N(N),
                returned(0),
                p(p),
                plasma(plasma),
                bfield(bfield),
                E0(E0),
                weight_factor(dt* state_space_vol / N) {
                assert(state_space_vol > 0);
                TPRNG::seed();
            }

            bool hasNext() {
                return returned < N;
            }

            int totalCount() const {
                return N;
            }

            typename IInitialCondition<state>::initial_condition next() {
                double R;
                double phi;
                double Z;

                IBoundedVolume &vol = plasma.boundingVolume();

                do {
                    R = TPRNG::randdouble() * (vol.Rmax() - vol.Rmin()) + vol.Rmin();
                    phi = TPRNG::randdouble() * (vol.phimax() - vol.phimin()) + vol.phimin();
                    Z = TPRNG::randdouble() * (vol.Zmax() - vol.Zmin()) + vol.Zmin();
                } while (!vol.isInside(R, phi, Z));

                double v0 = sqrt(2 * E0 / p.m);

                double cos_alpha = 2 * TPRNG::randdouble() - 1;

                double vpar = v0 * cos_alpha;
                double mu = (E0 - p.m * vpar * vpar / 2.) / bfield.B(R, phi, Z).norm();

                double phase = twopi * TPRNG::randdouble();

                state_t s; s << R, phi, Z, vpar, mu, phase;
                double w = weight(s);
                ++returned;

                return typename IInitialCondition<state>::initial_condition {s, w};
            }

            particle particleType() const {
                return p;
            }

            std::string name() const {
                return "cloud";
            }
        };

    }
}

#endif
