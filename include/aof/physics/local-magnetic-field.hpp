#ifndef LOCAL_MAGNETIC_FIELD_MODELS_HPP
#define LOCAL_MAGNETIC_FIELD_MODELS_HPP

#include "aof/algebra/vector-algebra.hpp"
#include "aof/physics/magnetic-field.hpp"

namespace aof {
    namespace physics {

        /**
         * @brief A representation of the magnetic field in a specific point
         * @details Uses knowledge of the global magnetic field, \f$\vec B\f$ and \f$\nabla\otimes\vec B\f$, be able to calculate the magnetic field properties required in the drift equations: \f$\vec B\f$, \f$\vec b\f$, \f$\nabla B\f$ and \f$\nabla\times\vec b\f$.
         */
        class LocalMagneticField {
        private:
            double R, phi, Z;

            vector<3> Bvec;
            matrix<3, 3> nabla_outerprod_B;

            double lenB, lenB2;

        public:
            /**
             * @brief Constructs a representation of the local magnetic field in the point \f$(R,\varphi,Z)\f$
             *
             * @param R The \f$R\f$ coordinate
             * @param phi The \f$\varphi\f$ coordinate
             * @param Z The \f$Z\f$ coordinate
             * @param globalField a representation of the global field
            */
            LocalMagneticField(double R, double phi, double Z,
                               vector<3> B, matrix<3, 3> nabla_outerprod_B);

            /**
             * @brief Calculates the magnetic field, \f$\vec B\f$
             * @return The magnetic field
            */
            vector<3> B() const;
            /**
             * @brief Calculate the unit vector in the direction of the magnetic field, \f$\vec b\f$
             * @return The unit vector in the direction of the magnetic field
            */
            vector<3> b() const;
            /**
             * @brief Calculate the gradient of the magnitude of the magnetic field, \f$\nabla B\f$.
             * @return The gradient of the magnitude of the magnetic field
             */
            vector<3> gradB() const;
            /**
             * @brief Calculate the rotation of the unit vector in the direction of the magnetic field, \f$\nabla\times\vec b\f$
             * @return The rotation of the unit vector in the direction of the magnetic field
             */
            vector<3> rotb() const;
        };
    }
}

#endif
