#ifndef BOUNDED_VOLUME_HPP
#define BOUNDED_VOLUME_HPP

#include "aof/algebra/vector-algebra.hpp"
#include <vector>
#include <limits>

namespace aof {
    namespace physics {
        class IBoundedVolume {
        public:
            virtual bool isInside(double R, double phi, double Z) const = 0;
            virtual double volume() const = 0;
            virtual double Rmin() const = 0;
            virtual double Rmax() const = 0;
            virtual double phimin() const = 0;
            virtual double phimax() const = 0;
            virtual double Zmin() const = 0;
            virtual double Zmax() const = 0;
            virtual ~IBoundedVolume() { }
        };

        class IAxisymmetricVolume : virtual public IBoundedVolume {
        public:
            virtual double surfaceArea() const = 0;
        };

        class IPolygonVolume : virtual public IBoundedVolume {
        public:
            typedef vector<3> vertex;

            virtual void add_vertex(double R, double phi, double Z) = 0;
            virtual void add_vertex(vertex v) = 0;
        };

        class AxisymmetricPolygon : public IAxisymmetricVolume, public IPolygonVolume {
        private:
            std::vector<vertex> vxs;
            double area = 0.0;
            double minR = std::numeric_limits<double>::infinity();
            double maxR = -std::numeric_limits<double>::infinity();
            double minphi = 0;
            double maxphi = 6.28318530718;
            double minZ = std::numeric_limits<double>::infinity();
            double maxZ = -std::numeric_limits<double>::infinity();

        public:
            void add_vertex(double R, double phi, double Z);
            void add_vertex(vertex v);
            bool isInside(double R, double phi, double Z) const;
            double surfaceArea() const;
            double volume() const;
            double Rmin() const;
            double Rmax() const;
            double phimin() const;
            double phimax() const;
            double Zmin() const;
            double Zmax() const;
        };
    }
}

#endif
