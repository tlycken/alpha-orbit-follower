#ifndef RHS_EVALUATOR_HPP
#define RHS_EVALUATOR_HPP

#include "aof/algebra/vector-algebra.hpp"
#include "aof/physics/local-magnetic-field.hpp"
#include "aof/physics/particle.hpp"
#include "aof/io/output.hpp"

namespace aof {
    namespace physics {

        template<typename state>
        struct solpoint {
            double t;
            state X;
            state rhs;
        };


        /**
         * @brief Template class for RHS evaluators
         * @details The RHS evaluators evaluate the right-hand side of an ODE system \f$\frac{d\vec X}{dt} = \vec F(t, \vec X)\f$, for use with ODE integrators.
         *
         * @tparam dimension The number of equations in the ODE system, i.e. the dimension of the vectors \f$\vec X\f$ and \f$\vec F\f$.
         */
        template<typename state, typename TGlobalMagneticField>
        class IRHSEvaluator {
        public:
            typedef state state_t;
            typedef TGlobalMagneticField globalfield_t;

            /**
             * @brief Evaluate the right-hand side of the system
             * @details The system function \f$F(t,\vec X)\f$ is evaluated at the point \f$(t,\vec X)\f$
             *
             * @param t The time \f$t\f$
             * @param y The point \f$\vec X\f$
             *
             * @return \f$\vec F(t,\vec X)\f$ as defined by the ODE system
             */
            virtual state_t operator() (const double &t, const state_t &X) const = 0;
            /**
             * @brief Virtual destructor
             * @details defined to allow for inheriting classes to clean up after themselves
             */
            virtual ~IRHSEvaluator() { };
        };
    }
}

// template<typename state>
// std::ostream &operator<<(std::ostream &os, std::vector<aof::physics::solpoint<state>> trace);

#endif
