#ifndef MAGNETIC_FIELD_HPP
#define MAGNETIC_FIELD_HPP

#include <stdexcept>
#include <sstream>

namespace aof {
    namespace physics {

        /**
        * @brief Exception thrown if the magnetic field is zero in any point.
        * @details Because some of the expressions used in the program depend on division by \f$B\f$, or by scalar multiples of \f$\vec b\f$, zero fields are not supported. This shouldn't be a problem if realistic indata for magnetic confinement fusion reactors is given, but if a zero field is encountered, this exception is thrown.
        *
        * @author Tomas Lycken
        */
        class zero_magnetic_field_exception : public std::runtime_error {
        private:
        public:
            zero_magnetic_field_exception() : runtime_error("Zero magnetic field") {
            }

            zero_magnetic_field_exception(std::string msg) : runtime_error(msg) { }

            virtual const char* what() const throw() {
                std::ostringstream cnvt;
                cnvt << runtime_error::what() << ": " << "This should never happen. Or you're not in a Tokamak, in which case, good for you!";

                return cnvt.str().c_str();
            }
        };
    }
}

#endif