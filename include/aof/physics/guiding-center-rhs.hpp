#ifndef GUIDING_CENTER_RHS
#define GUIDING_CENTER_RHS

#include "aof/aof-commons.hpp"
#include "aof/physics/rhs-evaluator.hpp"

#include "aof/algebra/vector-algebra.hpp"
#include "aof/physics/magnetic-field.hpp"
#include "aof/physics/global-magnetic-field.hpp"
#include "aof/physics/particle.hpp"

#include "aof/io/output.hpp"

namespace aof {
    namespace physics {
        /**
        * @brief RHS evaluator for the guiding center equations
        * @details Evaluates the RHS of the ODE system described by \f$\vec X = (R, \varphi, Z, v_\parallel, \mu, \phi)\f$, using the model
        *
        * \f[
        * \begin{cases}
        * \dot R &= \vec{\hat R}\cdot \frac{v_\parallel \vec B^\times}{B_\parallel^\times} \\
        * \\
        * \dot \varphi &= \vec{\hat \varphi} \cdot \frac{v_\parallel \vec B^\times}{B_\parallel^\times} \frac{1}{R} \\
        * \\
        * \dot Z &= \vec{\hat Z} \cdot \frac{v_\parallel \vec B^\times}{B_\parallel^\times} \\
        * \\
        * \dot{v}_{\parallel} &= -\frac{\mu}{m}\vec b\cdot\nabla B \\
        * \\
        * \dot{\mu} &= 0 \\
        * \\
        * \dot{\phi} &= \omega_c = \frac{ZeB}{m}
        * \end{cases}
        * \f]
        *
        * (See the thesis report for a detailed description of this system.)
        */
        template<typename TGlobalMagneticField>
        class GuidingCenterRHSEvaluator : public IRHSEvaluator<vector<6>, TGlobalMagneticField> {
        private:
            const TGlobalMagneticField bmodel;
            const particle p;

        public:
            typedef TGlobalMagneticField globalfield_t;
            typedef vector<6> state_t;
            /**
             * @param bmodel The IGlobalMagneticField that describes the magnetic field model used in the equations.
             * @param p A \ref particle instance describing the type of particle that should be considered
             */
            GuidingCenterRHSEvaluator(const TGlobalMagneticField bmodel, const particle &p) : bmodel(bmodel), p(p) { }

            /**
             * @brief Evaluates the RHS of the system
             * @details \f[
             * \begin{cases}
             * \dot {\vec R} &= \frac{v_\parallel \vec B^\times}{B_\parallel^\times} \\
             * \\
             * \dot{v}_{\parallel} &= -\frac{\mu}{m}\vec b\cdot\nabla B \\
             * \\
             * \dot{\mu} &= 0 \\
             * \\
             * \dot{\phi} &= \omega_c
             * \end{cases}
             * \f]
             *
             * @param t The time \f$t\f$ (ignored in this system)
             * @param y The point \f$(R,\varphi,Z,v_\parallel,\mu,\phi)\f$ in the shape of a `std::array<double, 6>`
             * @return A `std::array<double, 6>` containing the quantities \f$(\dot R, \dot \varphi, \dot Z, \dot v_\parallel, \dot \mu,\phi)\f$, in that order
             */
            state_t operator() (const double __attribute__((__unused__)) &t, const state_t &X) const {
                double R = X(0);
                double phi = X(1);
                double Z = X(2);
                double vpar = X(3);
                double mu = X(4);
                // double phase = y(5);

                LocalMagneticField localField(R, phi, Z, bmodel.B(R, phi, Z), bmodel.nabla_outerprod_B(R, phi, Z));

                vector<3> B = localField.B();
                vector<3> b = localField.b();
                vector<3> gradB = localField.gradB();

                state_t rhs;

                // position
                vector<3> Bstar = B + p.m * vpar / (p.charge()) * localField.rotb();
                vector<3> dR = (vpar * Bstar + mu / p.charge() * b.cross(gradB)) / (b.dot(Bstar));

                rhs.head<3>() << dR(0), dR(1) / R, dR(2);

                // v-parallel
                rhs(3) = - mu / p.m * b.dot(gradB);

                // mu
                rhs(4) = 0;

                // phase
                rhs(5) = p.charge() * B.length() / p.m;

                return rhs;
            }

        };
    }
}

template<>
std::ostream &header<aof::vector<6>>(std::ostream &os);

std::ostream &operator<<(std::ostream &os, aof::vector<6> s);

#endif
