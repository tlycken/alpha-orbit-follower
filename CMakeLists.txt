
cmake_minimum_required(VERSION 2.8)

option(BUILD_TESTS "Should the tests be compiled as well?" ON)
option(BUILD_DOCUMENTATION "Create and install the HTML based API documentation (requires Doxygen)" ON)

project(alpha-orbit-follower)

set(CMAKE_MODULE_PATH "${alpha-orbit-follower_SOURCE_DIR}/cmake/Modules/")
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/deps/eigen/cmake")
include(OutOfSourceBuild)
include(CPP11)

set(aof_VERSION_MAJOR 1)
set(aof_VERSION_MINOR 0)
set(aof_NAME alpha-orbit-follower)

configure_file (
    "${PROJECT_SOURCE_DIR}/include/aof/aof-config.h.in"
    "${PROJECT_BINARY_DIR}/include/aof/aof-config.h"
)
include_directories("${PROJECT_BINARY_DIR}/include")

configure_file("${PROJECT_SOURCE_DIR}/input/iter.xml"
    "${CMAKE_BINARY_DIR}/iter.xml" COPYONLY)
configure_file("${PROJECT_SOURCE_DIR}/input/jet.xml"
    "${CMAKE_BINARY_DIR}/jet.xml" COPYONLY)
configure_file("${PROJECT_SOURCE_DIR}/test/data/grad-shafranov.xml"
    "${CMAKE_BINARY_DIR}/data/grad-shafranov.xml" COPYONLY)

configure_file("${PROJECT_SOURCE_DIR}/scripts/prep.sh"
    "${CMAKE_BINARY_DIR}" COPYONLY)
configure_file("${PROJECT_SOURCE_DIR}/scripts/run.sh"
    "${CMAKE_BINARY_DIR}" COPYONLY)
configure_file("${PROJECT_SOURCE_DIR}/scripts/process.sh"
    "${CMAKE_BINARY_DIR}" COPYONLY)
configure_file("${PROJECT_SOURCE_DIR}/scripts/dump.sh"
    "${CMAKE_BINARY_DIR}" COPYONLY)
configure_file("${PROJECT_SOURCE_DIR}/scripts/one-script.sh"
    "${CMAKE_BINARY_DIR}" COPYONLY)
configure_file("${PROJECT_SOURCE_DIR}/scripts/generate-input.sh"
    "${CMAKE_BINARY_DIR}" COPYONLY)

set(CMAKE_INSTALL_PREFIX ${PROJECT_BINARY_DIR})

add_subdirectory(src)

if (BUILD_TESTS)
    add_subdirectory(test)
endif(BUILD_TESTS)

if(BUILD_DOCUMENTATION)

    find_package(Doxygen)
    if(NOT DOXYGEN_FOUND)
        message(FATAL_ERROR
        "Doxygen is needed to build the documentation.")
    endif()

    set(doxyfile_in ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in)
    set(doxyfile ${PROJECT_BINARY_DIR}/Doxyfile)
    set(doxy_html_index_file ${CMAKE_CURRENT_BINARY_DIR}/html/index.html)
    set(doxy_output_root ${CMAKE_CURRENT_BINARY_DIR})
    set(doxy_input "${PROJECT_SOURCE_DIR}/src ${PROJECT_SOURCE_DIR}/include")

    configure_file(${doxyfile_in} ${doxyfile} @ONLY)

    add_custom_command(OUTPUT ${doxy_html_index_file}
                       COMMAND ${DOXYGEN_EXECUTABLE} ${doxyfile}
                       MAIN_DEPENDENCY ${doxyfile} ${doxyfile_in}
                       DEPENDS xml numerics physics
                       COMMENT "Generating HTML documentation")

    add_custom_target( doc DEPENDS ${doxy_html_index_file} )
endif()
