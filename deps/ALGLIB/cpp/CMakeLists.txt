cmake_minimum_required(VERSION 2.8)

project(alglib)

file(GLOB ALGLIB_SOURCES "src/*.cpp")

set(ALGLIB_INCLUDES ${CMAKE_CURRENT_SOURCE_DIR})

add_library(ALGLIB ${ALGLIB_SOURCES})

install(TARGETS ALGLIB
    ARCHIVE DESTINATION ${PROJECT_SOURCE_DIR}/lib
)
