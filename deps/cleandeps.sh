#!/bin/bash

depsroot=`readlink -f ${0%/*}`

# Go to project root/deps
cd "$depsroot"

echo "Cleaning Google Test"
rm -rf "$depsroot/gmock-1.7.0/gtest/build"
rm -rf "$depsroot/gmock-1.7.0/gtest/lib"

echo "Cleaning Google Mock"
rm -rf "$depsroot/gmock-1.7.0/build"
rm -rf "$depsroot/gmock-1.7.0/lib"

echo "Cleaning TinyXML"
rm -rf "$depsroot/tinybind/tinyxml/build"
rm -rf "$depsroot/tinybind/tinyxml/lib"

echo "Cleaning TinyBind"
rm -rf "$depsroot/tinybind/build"
rm -rf "$depsroot/tinybind/lib"

echo "Cleaning ALGLIB"
rm -rf "$depsroot/ALGLIB/cpp/build"
rm -rf "$depsroot/ALGLIB/cpp/lib"

echo "Cleaning GeometricTools"
cd "$depsroot/GeometricTools/WildMagic5" && rm -rf "SDK" && make VERBOSE=1 CFG=Release clean -f makefile.wm5 > "$depsroot/clean.log" 2> "$depsroot/clean.log"
