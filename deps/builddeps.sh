#!/bin/sh

echo "Will now build all dependencies."
echo "Build logs will generally be located in files named build.log, either in the source root directory, or in a folder named 'build'"

depsroot=`readlink -f ${0%/*}`

# Go to project root/deps
cd "$depsroot"

echo "Building Google Test"
cd gmock-1.7.0/gtest
mkdir -p build && cd build && cmake .. > cmake.log && make > build.log
cd "$depsroot"

echo "Building Google Mock"
cd gmock-1.7.0
mkdir -p build && cd build && cmake .. > cmake.log && make > build.log
cd "$depsroot"

echo "Building ALGLIB..."
cd ALGLIB/cpp
mkdir -p build && cd build && cmake .. > cmake.log && make install > build.log
cd "$depsroot"

echo "Building TinyXML..."
cd "tinybind/tinyxml" && mkdir -p build && cd build && cmake .. > cmake.log && make install > build.log
cd "$depsroot"

echo "Building TinyBind..."
cd "tinybind" && mkdir -p build && cd build && cmake .. > cmake.log && make install > build.log
cd "$depsroot"

echo "Building GeometricTools..."
echo "This might take a while. However, there's no need to wait around,"
echo "since the mathematics library is built quite early in the process,"
echo "and no other parts of the library are used. If you don't want to"
echo "wait, just let this build script finish on its own, and open another"
echo "terminal in which you try to configure Alpha Orbit Follower."
echo "When it works, GeometricTools is 'ready enough'."

cd GeometricTools/WildMagic5
make CFG=Release -f makefile.wm5 > build.log

echo "All done!"
