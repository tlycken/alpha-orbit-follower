if [[ "$#" -ne 2 ]]; then
    echo "Usage: ./process.sh <run-name> <machine-name>"
    echo "Example: './process.sh run-01 iter' to process folder run-01 for the iter machine"
    exit -1
fi

if [[ -d "$1" ]]; then
	echo "Moving to postprocessing..." && \
	mv "$1" "../postprocessing"
elif [[ ! -d "../postprocessing/$1" ]]; then
    echo "Couln't find ./$1 or ../postprocessing/$1 - run ./run.sh first!"
    exit -1
fi

cd ../postprocessing && echo "Starting Julia..." && \
julia -e "println(\"Including script...\"); include(\"process-all.jl\"); processall(\"$1/output\",\"$2\")" && \
echo "Processing done!"
