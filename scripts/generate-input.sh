#!/bin/bash

if [[ "$#" -ne 1 ]]; then
	echo "Usage: ./generate-input.sh <number-of-particles>"
	exit 1
fi

cd ../input && matlab -nojvm -r "generate($1); exit" && cd ../build
cmake ..
