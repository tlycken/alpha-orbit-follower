#!/bin/bash

# ...to run them all...

if [[ "$#" -ne 1 ]]; then
	echo "Usage: ./one-script.sh <run-postfix>"
	echo "Example: './one-script.sh foo' will run the following sequence of commands:"
	echo "\t./prep.sh all foo"
	echo "\t./run.sh iter-foo iter 2>&1 >> iter-foo.log && ./process.sh iter-foo iter 2>&1 >> iter-foo.log"
	echo "\t./run.sh jet-foo jet 2>&1 >> iter-foo.log && ./process.sh jet-foo jet 2>&1 >> iter-foo.log"
	echo "The final two may be run in parallel."
	exit -1
fi

./prep.sh all "$1"
prepped=$?
if [[ prepped -eq 0 ]]; then
	echo "Forking ITER simulation - output in iter-$1.log"
	./run.sh "iter-$1" iter 2>&1 >> "iter-$1.log" && ./process.sh "iter-$1" iter 2>&1 >> "iter-$1.log" &
	echo "Forking JET simulation - output in jet-$1.log"
	./run.sh "jet-$1" jet 2>&1 >> "jet-$1.log" && ./process.sh "jet-$1" jet 2>&1 >> "jet-$1.log" &
fi
