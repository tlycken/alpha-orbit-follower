if [[ "$#" -ne 2 ]]; then
    echo "Usage: ./dump.sh <root-name> <machine-name>"
    echo "Example: './dump.sh iter-d3' to run in folder iter-d3 using input file iter.xml"
    exit -1
fi

if [[ ! -d "$1" ]]; then
    echo "Run ./prep.sh first!"
    exit -1
fi

echo "Starting dump..."
cd $1 && ../bin/dumper "$2.xml" && \
echo "Compressing results..." && \
gzip --recursive output && \
echo "Moving to postprocessing..." && \
cd .. && mv "$1" "../postprocessing" && \
echo "All done!"
