if [[ "$#" -ne 2 ]]; then
    echo "Usage: ./run.sh <run-name> <machine-name>"
    echo "Example: './run.sh run-01 iter' to run for the iter machine in folder run-01"
    exit -1
fi

if [[ ! -d "$1" ]]; then
    echo "Run ./prep.sh first!"
    exit -1
fi

echo "Starting simulation of ${2^^} in folder $1..."
cd $1 && ../bin/alpha-orbit-follower "$2.xml" && \
echo "Simulation done! Compressing results..." && \
gzip --recursive output && \
echo "Compressing done!"
