if [[ "$#" -ne 2 ]]; then
    echo "Usage: ./prep.sh <run-name> <machine-name>"
    echo "Example: './prep.sh iter-1e5 iter' to run with iter.xml in folder iter-1e5"
    echo ""
    echo "You can also use ./prep.sh all <suffix> to prep both iter and jet with folder names"
    echo "iter-<suffix> / jet-<suffix>"
    exit 1
fi

if [[ "$1" == "all" ]]; then
    ./prep.sh "iter-$2" iter && ./prep.sh "jet-$2" jet || exit 1
    exit 0
fi

if [[ ! -f "$2.xml" ]]; then
    echo "No file $2.xml - run cmake first!"
    exit 1
fi

if [[ -d "../postprocessing/$1" ]]; then
    echo "Folder postprocessing/$1 already exists - move or delete it before prepping."
    exit 1
fi

rm -rf "$1"

mkdir -p "$1/output/diagnostics"
mkdir -p "$1/output/traces"

mv "$2.xml" "$1"
